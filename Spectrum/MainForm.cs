﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

using Bramble.Core;
using Malison.Core;
using Malison.WinForms;

namespace Spectrum
{
    public enum ActiveMenuType { Title, Options };

    public partial class MainForm : TerminalForm
    {
        float FPS = 0;
        const int DEFAULT_RENDER_X = 160;
        const int DEFAULT_RENDER_Y = 58;
        static Vec getDefaultRenderSize { get { return new Vec(DEFAULT_RENDER_X, DEFAULT_RENDER_Y); } }
        const float VERSION = 1.0f;

        private System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
        private const int FPS_TARGET = 60;
        

        Renderer renderer;
        GameSimulation game;

        ActiveMenuType ActiveMenu { get; set; }
        

        IMenu titleMenu;
        IMenu optionsMenu;

        public IInputProcessor Input { get; private set; }


        public MainForm()
        {
            // malison form setup
            InitializeComponent(); // required method for malison, seems to configure form settings
            ConfigureFormSettings();
            
            // set up key capture events
            AddHandlers();
            
            // initialize all the program data
            CreateGameSimulation();
            CreateMenus();
            CreateTerminalAndRenderer();
            CreateInput();

            // start counting time for FPS purposes
            stopwatch.Reset();
            stopwatch.Start();
        }

        private void CreateMenus()
        {
            ActiveMenu = ActiveMenuType.Title;
            titleMenu = new TitleMenu(game, SetActiveMenu);
            optionsMenu = new OptionsMenu(game, SetActiveMenu);
        }

        // The main program loop
        private void HandleApplicationIdle(object sender, EventArgs e)
        {
            while (IsApplicationIdle())
            {
                if (game.Active)
                {
                    UpdateGame();
                    FileOperations.Save(FilePaths.Save, game.Scene);
                }

                else
                    UpdateMenu();
            }
        }

        public void SetActiveMenu(ActiveMenuType value)
        {
            ActiveMenu = value;
        }

        private void UpdateGame()
        {
            if (Input.HasInput)
            {
                game.AddCommand(Input.PullCommand());
            }
            game.Update();
            RenderGameSimulation();
        }

        private void UpdateMenu()
        {
            // determine the appropriate menu to work on
            IMenu activeMenu = null;
            switch (ActiveMenu)
            {
                case ActiveMenuType.Options:
                    activeMenu = optionsMenu;
                    break;

                default:
                    activeMenu = titleMenu;
                    break;
            }


            // pass commands to the active menu
            if (Input.HasInput)
            {
                activeMenu.AddCommand(Input.PullCommand());
            }

            // process
            activeMenu.Update();
            RenderMenu(activeMenu);
        }

        
        private void RenderMenu(IMenu menu)
        {
            renderer.RenderMenu(game, menu);
        }

        private void RenderGameSimulation()
        {

            // skip rendering if the framerate is above target
            if (stopwatch.ElapsedMilliseconds < (1000 / FPS_TARGET))
                return;
            else
            {
                FPS = 1000 / stopwatch.ElapsedMilliseconds;
                stopwatch.Reset();
                stopwatch.Start();
            }
             

            renderer.RenderGameSimulation(game, titleMenu);
        }

        private void ConfigureFormSettings()
        {
            this.Text = "Hard Target v" + VERSION.ToString();
        }


        private void AddHandlers()
        {
            Application.Idle += HandleApplicationIdle;
            this.KeyPreview = true;
            this.KeyDown += new KeyEventHandler(HandleKeyDown);
        }

        private void CreateTerminalAndRenderer()
        {
            Terminal = new Terminal(DEFAULT_RENDER_X, DEFAULT_RENDER_Y);
            renderer = new Renderer(Terminal, getDefaultRenderSize);
        }

        private void CreateGameSimulation()
        {
            game = new GameSimulation();
        }

        private void CreateInput()
        {
            Input = new InputProcessor();
        }

        private void HandleKeyDown(object sender, KeyEventArgs e)
        {
            Input.Register(e);
            //RenderGameSimulation();
        }


        // returns true if there are no events in the pump
        static bool IsApplicationIdle()
        {
            NativeMessage result;
            return PeekMessage(out result, IntPtr.Zero, (uint)0, (uint)0, (uint)0) == 0;
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct NativeMessage
        {
            public IntPtr Handle;
            public uint Message;
            public IntPtr WParameter;
            public IntPtr LParameter;
            public uint Time;
            public Point Location;
        }

        [DllImport("user32.dll")]
        private static extern int PeekMessage(out NativeMessage message, IntPtr window, uint filterMin, uint filterMax, uint remove);


        
    }
}
