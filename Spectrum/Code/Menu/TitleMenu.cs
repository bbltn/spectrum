﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace Spectrum
{
    public class TitleMenu : BaseMenu
    {
        MenuChangeMethod menuChangeDelegate;

        public TitleMenu(GameSimulation game, MenuChangeMethod menuChangeFunction)
            : base(game)
        {
            menuChangeDelegate = menuChangeFunction;

            AddItems();
        }

        override protected void AddItems()
        {
            IMenuItem newGameItem = new NewGameItem(Game, "New Game");
            Items.Add(newGameItem);

            IMenuItem loadGameItem = new LoadGameItem(Game, "Resume Game [Can take about a minute!]");
            Items.Add(loadGameItem);

            IMenuItem optionsItem = new MenuChangeItem(menuChangeDelegate, ActiveMenuType.Options, "Options");
            Items.Add(optionsItem);
        }
    }
}
