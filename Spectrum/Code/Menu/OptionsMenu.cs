﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace Spectrum
{
    public class OptionsMenu : BaseMenu
    {
        MenuChangeMethod menuChangeDelegate;

        public OptionsMenu(GameSimulation game, MenuChangeMethod menuChangeFunction)
            : base(game)
        {
            menuChangeDelegate = menuChangeFunction;

            AddItems();
        }

        
        override protected void AddItems()
        {
            IMenuItem returnItem = new MenuChangeItem(menuChangeDelegate, ActiveMenuType.Title, "Return");
            Items.Add(returnItem);
        }
    }
}
