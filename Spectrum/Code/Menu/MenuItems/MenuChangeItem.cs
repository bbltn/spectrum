﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public delegate void MenuChangeMethod(ActiveMenuType value);

    public class MenuChangeItem : BaseMenuItem
    {
        private MenuChangeMethod OnActivation;
        private ActiveMenuType targetMenu;


        public MenuChangeItem(MenuChangeMethod menuChangeFunction, ActiveMenuType changeTo, string name)
            : base(name)
        {
            OnActivation = menuChangeFunction;
            targetMenu = changeTo;
        }

        override public void Use(BaseCommand command)
        {
            OnActivation(targetMenu);
        }
    }
}
