﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public class NewGameItem : BaseMenuItem
    {
        GameSimulation Game;

        public NewGameItem(GameSimulation game, string name)
            : base(name)
        {
            Game = game;
        }

        override public void Use(BaseCommand command)
        {
            SceneGenerator.Generate(Game.Scene, Game.Resources);
            Game.Active = true;
        }
    }
}
