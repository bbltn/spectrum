﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public class LoadGameItem : BaseMenuItem
    {
        GameSimulation Game;

        public LoadGameItem(GameSimulation game, string name)
            : base(name)
        {
            Game = game;
        }

        override public void Use(BaseCommand command)
        {
            SceneOperations.LoadScene(Game.Scene, Game.Resources);
            Game.Active = true;
        }
    }
}
