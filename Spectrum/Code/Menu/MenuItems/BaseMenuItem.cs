﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public interface IMenuItem
    {
        CharacterString Name { get; }

        void SetDefaultColors();
        void SetColors(TermColor foreColor, TermColor backColor);
        void Use(BaseCommand command);
    }

    public class BaseMenuItem : IMenuItem
    {
        public CharacterString Name { get; private set; }

        private const TermColor Fore = TermColor.LightGray;
        private const TermColor Back = TermColor.Black;


        public BaseMenuItem(string name)
        {
            Name = new CharacterString(name, Fore, Back);
        }

        public void SetDefaultColors()
        {
            Name = new CharacterString(Name.ToString(), Fore, Back);
        }

        public void SetColors(TermColor foreColor, TermColor backColor)
        {
            Name = new CharacterString(Name.ToString(), foreColor, backColor);
        }

        virtual public void Use(BaseCommand command)
        {
        }
    }
}
