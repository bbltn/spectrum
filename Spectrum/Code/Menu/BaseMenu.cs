﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace Spectrum
{
    public interface IMenu
    {
        int Selector { get; }
        int ItemCount { get; }
        IMenuItem GetItem(int index);
        void Update();
        void AddCommand(BaseCommand command);
    }

    public class BaseMenu : IMenu
    {
        protected GameSimulation Game { get; set; }
        public int Selector { get; private set; }
        protected List<IMenuItem> Items { get; private set; }
        public int ItemCount { get { return Items.Count; } }
        protected List<BaseCommand> Commands { get; private set; }

        public BaseMenu(GameSimulation game)
        {
            Game = game;
            Selector = 0;
            Items = new List<IMenuItem>();
            Commands = new List<BaseCommand>();
        }
        
        public void AddCommand(BaseCommand command)
        {
            Commands.Add(command);
        }

        public void Update()
        {
            if (Commands.Count == 0) return;

            VectorCommand vc = Commands[0] as VectorCommand;
            if (vc != null)
            {
                if (vc.Vector == new Vec(0, 1))
                    IncrementSelector();
                if (vc.Vector == new Vec(0, -1))
                    DecrementSelector();

                Commands.RemoveAt(0);
                return;
            }

            KeyCommand kc = Commands[0] as KeyCommand;
            if (kc != null)
            {
                if (kc.Key == System.Windows.Forms.Keys.W || kc.Key == System.Windows.Forms.Keys.Up)
                    DecrementSelector();

                if (kc.Key == System.Windows.Forms.Keys.S || kc.Key == System.Windows.Forms.Keys.Down)
                    IncrementSelector();

                if (kc.Key == System.Windows.Forms.Keys.Enter || kc.Key == System.Windows.Forms.Keys.Space)
                    Items[Selector].Use(null);

                Commands.RemoveAt(0);
                return;
            }

            Commands.RemoveAt(0);
        }

        private void DecrementSelector()
        {
            Selector--;

            if (Selector < 0)
                Selector = Items.Count - 1;
        }

        private void IncrementSelector()
        {
            Selector++;

            if (Selector >= Items.Count)
                Selector = 0;
        }

        public IMenuItem GetItem(int index)
        {
            return Items[index];
        }

        virtual protected void AddItems()
        {
            IMenuItem testItem_1 = new BaseMenuItem("Test 1");
            Items.Add(testItem_1);

            IMenuItem testItem_2 = new BaseMenuItem("Test 2");
            Items.Add(testItem_2);
        }

    }
}
