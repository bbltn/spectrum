﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    static public class NodeValues
    {
        static public List<float> CreateForTile(ITile tile)
        {
            List<float> value = new List<float>();

            if (tile == null) return value;

            value.Add(tile.Position.X); // 0
            value.Add(tile.Position.Y); // 1
            value.Add(tile.Position.Z); // 2
            
            // 3 = blocks space
            if (tile.Feature == null) value.Add(0);
            else if (tile.Feature.BlocksSpace) value.Add(1);
            else value.Add(0);

            // 4 - blocks vision
            bool blocksVision = false;
            if (tile.Feature != null)
                if (tile.Feature.BlocksVision)
                    blocksVision = true;
            if (tile.Gas != null)
                if (tile.Gas.BlocksVision)
                    blocksVision = true;
            if (blocksVision) value.Add(1);
            else value.Add(0);

            // 5 - contains bot
            if (tile.Actor == null) value.Add(0);
            else if (tile.Actor.Control == ControlType.Bot) value.Add(1);
            else value.Add(0);
            

            return value;
        }

        static public Vector3 Position(INode node)
        {
            if (node == null) return Vector3.Zero;

            var values = node.GetNodeValues;

            return new Vector3(values[0], values[1], values[2]);
        }

        static public bool BlocksSpace(INode node)
        {
            if (node == null) return false;

            var values = node.GetNodeValues;
            return (values[3] == 1);
        }

        static public bool BlocksVision(INode node)
        {
            if (node == null) return false;

            var values = node.GetNodeValues;
            return (values[4] == 1);
        }

        static public bool ContainsBot(INode node)
        {
            if (node == null) return false;

            var values = node.GetNodeValues;
            return (values[5] == 1);
        }
    }
}
