﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public delegate float NodeCostEvaluationMethod(INode target, INode origin);

    public static class NodeCostEvaluation
    {
        public static float AlwaysFalse(/*INode node, INode origin*/)
        { return -1; }

        public static float AlwaysTrue(/*INode node, INode origin*/)
        { return 0; }

        public static float SameZLevel(INode node, INode origin)
        {
            Vector3 position = NodeValues.Position(node);
            Vector3 testPosition = NodeValues.Position(origin);

            if (position.Z == testPosition.Z)
                return 0;
            
            return -1;
        }

        public static float NoBlockedSpace(INode node/*, INode origin*/)
        {
            if (NodeValues.BlocksSpace(node))
                return -1;
            return 0;
        }

        public static float NoBots(INode node)
        {
            if (NodeValues.ContainsBot(node))
                return -1;
            return 0;
        }

        public static float SameZLevelAndNoBlockedSpace(INode node, INode origin)
        {
            bool blocked = SameZLevel(node, origin) < 0;
            if (blocked) return -1;

            blocked = NoBlockedSpace(node) < 0;
            if (blocked) return -1;

            return 0;
        }

        public static float WalkDistance(INode node, INode target)
        {
            bool blocked = SameZLevelAndNoBlockedSpace(node, target) < 0;
            if (blocked) return -1;


            return Vector3.Distance(NodeValues.Position(node), NodeValues.Position(target));
        }

        public static float BotWalkDistance(INode node, INode target)
        {
            bool blocked = SameZLevelAndNoBlockedSpace(node, target) < 0;
            if (blocked) return -1;

            blocked = NoBots(node) < 0;
            if (blocked) return -1;


            return Vector3.Distance(NodeValues.Position(node), NodeValues.Position(target));
        }
    }
}
