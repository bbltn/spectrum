﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface INode
    {
        List<INode> GetAdjacentNodes { get; }
        List<float> GetNodeValues { get; }
    }
}
