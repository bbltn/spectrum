﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spectrum
{
    public static class PlayerControl
    {
        public static void Update(IScene scene, IActor actor, BaseCommand command)
        {
            if (scene == null || actor == null || command == null) return;

            if (actor.Health.Status == HealthStatusType.Dead)
            {
                if (command != null) CreateWaitEvent(scene, actor);
                return;
            }

            if (command != null)
            {
                var vector = command as VectorCommand;
                if (vector != null)
                {
                    if (vector.Vector == Bramble.Core.Vec.Zero) 
                        CreateWaitEvent(scene, actor);
                    else
                        MoveOrStrikeInDirection(scene, actor, vector);
                }

                var letter = command as KeyCommand;
                if (letter != null)
                {
                    if (letter.Key == Keys.F)
                        if (actor.Knowledge.EnemyCount > 0)
                            CreateChooseTargetEvent(scene, actor);

                    if (letter.Key == Keys.H)
                        CreateHealEvent(scene, actor);

                    if (letter.Key == Keys.Oemplus)
                        scene.Camera.Raise();

                    if (letter.Key == Keys.OemMinus)
                        scene.Camera.Lower();
                }
            }
        }

        private static void MoveOrStrikeInDirection(IScene scene, IActor actor, VectorCommand vector)
        {
            Vector3 newPosition = new Vector3(actor.Position.X + vector.Vector.X, actor.Position.Y + vector.Vector.Y, actor.Position.Z);
            ITile tile = scene.Tiles.GetTile(newPosition);
            if (tile != null)
            {
                if (tile.Actor != null)
                    CreateStrikeEvent(scene, actor, tile.Actor);
                else
                    CreateMoveEvent(scene, actor, vector);
            }
        }

        private static void CreateHealEvent(IScene Scene, IActor Actor)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.Cost = 1;

            IEvent ev = new E_HealSelf(info);
            Scene.Events.Add(ev);
        }

        private static void CreateChooseTargetEvent(IScene Scene, IActor Actor)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.Cost = 0;

            IEvent ev = new E_ChooseTarget(info);
            Scene.Events.Add(ev);
        }

        private static void CreateShootEvent(IScene Scene, IActor Actor, IActor Target)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.TargetActor = Target;
            info.Cost = 1;

            IEvent ev = new E_Shoot(info);
            Scene.Events.Add(ev);
        }

        private static void CreateStrikeEvent(IScene Scene, IActor Actor, IActor Target)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.TargetActor = Target;
            info.Cost = 1;

            IEvent ev = new E_Strike(info);
            Scene.Events.Add(ev);
        }


        private static void CreateMoveEvent(IScene Scene, IActor Actor, VectorCommand vector)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.Vector = new Vector3(vector.Vector.X, vector.Vector.Y, 0);
            info.Cost = 1;

            IEvent moveEvent = new E_Move(info);
            Scene.Events.Add(moveEvent);
        }


        private static void CreateWaitEvent(IScene Scene, IActor Actor)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.Cost = 1;

            IEvent defEvent = new E_Wait(info);
            Scene.Events.Add(defEvent);
        }
    }
}
