﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Malison.Core;

namespace Spectrum
{
    public static class BotControl
    {

        public static void Update(IScene scene, IActor actor)
        {
            if (scene == null || actor == null) return;

            if (actor.Health.Status == HealthStatusType.Dead)
            {
                CreateWaitEvent(scene, actor);
                return;
            }

            switch (actor.Knowledge.Plan.Task)
            {
                case TaskType.MoveToTarget:
                    DoMoveToTarget(scene, actor);
                    return;

                case TaskType.Wander:
                    DoWander(scene, actor);
                    return;

                case TaskType.Wait:
                    DoWait(scene, actor);
                    return;

                default:
                    DoWait(scene, actor);
                    return;
            }
        }



        private static void DoWait(IScene scene, IActor actor)
        {
            // if we have a target, switch to approaching him
            IActor target = IdentifyTarget(scene, actor);
            if (target != null)
                actor.Knowledge.Plan.Task = TaskType.MoveToTarget;

            // otherwise wait
            else
            {
                CreateWaitEvent(scene, actor);

                // random chance to start wandering
                int WanderChance = 50;
                var RNG = scene.Resources.RNG;
                if (RNG.Next(0, 100) <= WanderChance)
                    actor.Knowledge.Plan.Task = TaskType.Wander;
            }
        }

        private static void DoWander(IScene scene, IActor actor)
        {
            // if we have a target, switch to approaching him
            IActor target = IdentifyTarget(scene, actor);
            if (target != null)
            {
                actor.Knowledge.Plan.Task = TaskType.MoveToTarget;
                return;
            }

            // if we have no path, pick a random open tile and go to it
            if (actor.Knowledge.Plan.Path == null)
            {
                CreateWanderPath(scene, actor);

                // if we still have no path, just wait
                if (actor.Knowledge.Plan.Path == null)
                {
                    actor.Knowledge.Plan.Task = TaskType.Wait;
                    return;
                }
            }


            // if we have a path
            else
            {
                var nextPosition = actor.Knowledge.Plan.Path.GetNext(actor.Position);

                // if the next position is our current one or final one, wait and get rid of the path
                if (nextPosition == actor.Position || nextPosition != actor.Knowledge.Plan.Path.GetPoint(0))
                {
                    CreateWaitEvent(scene, actor);
                    actor.Knowledge.Plan.Path = null;
                    return;
                }

                FollowPath(scene, actor);
            }
        }

        private static void ShootTarget(IScene scene, IActor actor, IActor target)
        {
            IEventInfo info = new EventInfoModel(scene);
            info.SubjectActor = actor;
            info.TargetActor = target;
            info.Cost = 1;

            IEvent ev = new E_Shoot(info);
            scene.Events.Add(ev);
        }

        private static void DoMoveToTarget(IScene scene, IActor actor)
        {
            IActor target = IdentifyTarget(scene, actor);

            // if we have no target,
            if (target == null)
            {
                // if we have no path, go to wait mode
                if (actor.Knowledge.Plan.Path == null)
                {
                    actor.Knowledge.Plan.Task = TaskType.Wait;
                    return;
                }

                // follow the path we have
                ApproachUnseenTargetOnExistingPath(scene, actor);
                return;
            }

            // if we have no path, create one
            if (actor.Knowledge.Plan.Path == null)
            {
                CreatePathToPosition(scene, actor, target.Position);
            }

            // if we have a gun and are in range, shoot
            IItem item = actor.Inventory.Equipped;
            var distance = Vector3.Distance(actor.Position, target.Position);
            if (item != null)
                if (item.Gun != null)
                    if (distance < (item.Gun.Range * 1.5))
                    {
                        ShootTarget(scene, actor, target);
                        return;
                    }


            // if we are adjacent to the target,
            if (Vector3.Distance(actor.Position, target.Position) < 2)
            {
                StrikeAdjacentTarget(scene, actor, target);
                return;
            }

            // if we still don't have a path, wait
            if (actor.Knowledge.Plan.Path == null)
            {
                CreateWaitEvent(scene, actor);
                return;
            }


            // if our path leads to ourselves, create a new one and wait
            if (actor.Knowledge.Plan.Path.GetFinal == actor.Position)
            {
                CreatePathToPosition(scene, actor, target.Position);
                CreateWaitEvent(scene, actor);
                return;
            }

            // if our path doesn't lead to the target, create a new one
            if (actor.Knowledge.Plan.Path.GetFinal != target.Position)
                CreatePathToPosition(scene, actor, target.Position);

            FollowPath(scene, actor);
            return;
        }

        private static void CreateWanderPath(IScene scene, IActor actor)
        {
            Random RNG = scene.Resources.RNG;
            Vector3 testPosition;
            ITile tile;
            int attempts = 0;

            while (attempts < 3)
            {
                testPosition = new Vector3(actor.Position.X + RNG.Next(-5, 5), actor.Position.Y + RNG.Next(-5, 5), actor.Position.Z);
                tile = scene.Tiles.GetTile(testPosition);

                if (tile != null)
                    if (!tile.BlocksMovement())
                    {
                        CreatePathToPosition(scene, actor, testPosition);

                        if (actor.Knowledge.Plan.Path != null)
                            break;
                    }

                attempts++;
            }
        }

        private static void StrikeAdjacentTarget(IScene scene, IActor actor, IActor target)
        {
            // if the target is already dead, just wait
            if (target.Health.Status == HealthStatusType.Dead)
                CreateWaitEvent(scene, actor);

            // otherwise hit them
            else CreateStrikeEvent(scene, actor, target);
        }

        private static void CreatePathToPosition(IScene Scene, IActor Actor, Vector3 targetPosition)
        {
            var originTile = Scene.Tiles.GetTile(Actor.Position);
            var endTile = Scene.Tiles.GetTile(targetPosition);
            Actor.Knowledge.Plan.Path = new Path(originTile.Node, endTile.Node, NodeCostEvaluation.BotWalkDistance);
        }

        private static void FollowPath(IScene Scene, IActor Actor)
        {
            var nextPosition = Actor.Knowledge.Plan.Path.GetNext(Actor.Position);

            // if the next position is our current one, wait
            if (nextPosition == Actor.Position)
            {
                CreateWaitEvent(Scene, Actor);
                return;
            }

            var tile = Scene.Tiles.GetTile(nextPosition);
            if (tile.BlocksMovement())
            {
                CreateWaitEvent(Scene, Actor);
                return;
            }

            var moveVector = nextPosition - Actor.Position;

            IEventInfo mInfo = new EventInfoModel(Scene);
            mInfo.SubjectActor = Actor;
            mInfo.Vector = moveVector;
            mInfo.Cost = 1;

            IEvent moveEvent = new E_Move(mInfo);
            Scene.Events.Add(moveEvent);
            return;
        }

        private static void ApproachUnseenTargetOnExistingPath(IScene Scene, IActor Actor)
        {             
            // if we are at the end of the path, get rid of it and go to Wander state
            if (Actor.Knowledge.Plan.Path.GetFinal == Actor.Position)
            {
                Actor.Knowledge.Plan.Path = null;
                Actor.Knowledge.Plan.Task = TaskType.Wait;
                return;
            }

            // otherwise keep following the path
            FollowPath(Scene, Actor);
            return;

        }


        public static IActor IdentifyTarget(IScene scene, IActor actor)
        {
            if (scene == null || actor == null) return null;

            if (actor.Knowledge.EnemyCount > 0)
                for (int iter = 0; iter < actor.Knowledge.EnemyCount; iter++)
                {
                    var enemy = actor.Knowledge.GetEnemy(iter);

                    if (enemy.Control == ControlType.Player)
                    {
                        return enemy;
                    }
                }

            return null;
        }

        private static void CreateStrikeEvent(IScene Scene, IActor Actor, IActor Target)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.TargetActor = Target;
            info.Cost = 1;

            IEvent defEvent = new E_Strike(info);
            Scene.Events.Add(defEvent);
        }


        private static void CreateWaitEvent(IScene Scene, IActor Actor)
        {
            IEventInfo info = new EventInfoModel(Scene);
            info.SubjectActor = Actor;
            info.Cost = 1;

            IEvent defEvent = new E_Wait(info);
            Scene.Events.Add(defEvent);
        }

    }
}
