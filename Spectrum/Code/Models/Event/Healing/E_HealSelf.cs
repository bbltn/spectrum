﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    class E_HealSelf : EventModel
    {
        public E_HealSelf(IEventInfo info)
            : base(info)
        {
        }

        public override void Process(BaseCommand command)
        {
            if (CheckPreconditions() == false)
            {
                State = ReturnCode.Cancel;
                return;
            }

            // get rid of a medkit
            RemoveMedkit(Info.SubjectActor.Inventory);

            // set health to full
            Info.SubjectActor.Health.Status = HealthStatusType.Good;

            // print message
            Info.Scene.Messages.Add(Info.SubjectActor.Name.Full + " uses a medkit.");

            State = ReturnCode.Done;
            return;
        }

        private static void RemoveMedkit(IInventory inventory)
        {
            int itemCount = inventory.ItemCount;

            for (int iter = 0; iter < itemCount; iter++)
                if (inventory.GetItem(iter).Name == "Medkit")
                {
                    inventory.RemoveItem(iter);
                    break;
                }
        }

        public override bool CheckPreconditions()
        {
            if (Info.SubjectActor == null) return false;

            // we are wounded
            if (Info.SubjectActor.Health.Status == HealthStatusType.Good) return false;

            // we are not dead
            if (Info.SubjectActor.Health.Status == HealthStatusType.Dead) return false;

            // we have items
            int itemCount = Info.SubjectActor.Inventory.ItemCount;
            if ( itemCount <= 0) return false;

            // we have a medkit
            bool hasMedkit = false;
            for (int iter = 0; iter < itemCount; iter++)
                if (Info.SubjectActor.Inventory.GetItem(iter).Name == "Medkit")
                {
                    hasMedkit = true;
                    break;
                }
            if (!hasMedkit) return false;

            return true;
        }
    }
}
