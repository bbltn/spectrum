﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IEventInfo
    {
        IScene Scene { get; }
        int Cost { get; set; }

        IActor SubjectActor { get; set; }
        IActor TargetActor { get; set; }

        Vector3 Vector { get; set; }

        int Counter { get; set; }

        bool Verbose { get; set; }

        bool ReadyToProcess { get; }

        ITime StartTime { get; }
    }
    
    public class EventInfoModel : IEventInfo
    {
        public IScene Scene { get; private set; }

        public IActor SubjectActor { get; set; }
        public IActor TargetActor { get; set; }

        public Vector3 Vector { get; set; }

        public int Counter { get; set; }

        public bool Verbose { get; set; }

        public ITime StartTime { get; private set; }

        public int Cost { get; set; }

        public EventInfoModel(IScene scene)
        {
            if (scene == null) return;

            Scene = scene;
            StartTime = new TimeModel(scene.Time.Absolute);
            Verbose = true;
        }

        public bool ReadyToProcess
        {
            get
            {
                if (Scene.Time.Absolute >= StartTime.Absolute + Cost)
                    return true;

                return false;
            }
        }

    }
}
