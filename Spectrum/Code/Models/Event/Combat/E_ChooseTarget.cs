﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;
using Malison.Core;

namespace Spectrum
{
    class E_ChooseTarget : EventModel
    {
        private List<IActor> Targets;
        private int Selector;
        private Line LineToTarget;
        private IGraphic MarkerGraphic;

        public E_ChooseTarget(IEventInfo info)
            : base(info)
        {
            Targets = new List<IActor>();
            Selector = 0;
            MarkerGraphic = new AsciiGraphic(new Character('#'), TermColor.Yellow, TermColor.DarkGold);
        }

        override public void Process(BaseCommand command)
        {
            if (CheckPreconditions() == false)
            {
                State = ReturnCode.Cancel;
                return;
            }

            RemoveMarkerLine();

            // get targets list
            if (Info.Counter == 0)
            {
                Info.Scene.Messages.Add(" Press Tab to cycle through targets. Press Space to fire. ", TermColor.White, TermColor.DarkGray);

                // get all enemies in view
                List<IActor> availableTargets = new List<IActor>();
                for (int iter = 0; iter < Info.SubjectActor.Knowledge.EnemyCount; iter++)
                    availableTargets.Add(Info.SubjectActor.Knowledge.GetEnemy(iter));

                // add enemies to target list, closest first
                while (availableTargets.Count > 0)
                {
                    IActor closest = availableTargets[0];

                    foreach (IActor actor in availableTargets)
                        if (Vector3.Distance(Info.SubjectActor.Position, actor.Position) < Vector3.Distance(Info.SubjectActor.Position, closest.Position))
                            closest = actor;

                    Targets.Add(closest);
                    availableTargets.Remove(closest);
                }

                //Targets.Add(Info.SubjectActor.Knowledge.ClosestEnemy);

                Info.Counter++;
                return;
            }

            // create marker path to target
            CreateMarkerLine(Info.SubjectActor.Position, Targets[Selector].Position);

            // process commands
            ProcessCommands(command);

            // State = ReturnCode.NotDone;
            return;
        }

        private void ProcessCommands(BaseCommand command)
        {
            KeyCommand key = command as KeyCommand;
            if (key != null)
            {
                if (key.Key == Keys.Space)
                {
                    CreateShootEvent();
                    State = ReturnCode.Done;
                    RemoveMarkerLine();
                    return;
                }

                if (key.Key == Keys.Tab)
                {
                    IncrementSelector();
                    return;
                }

                if (key.Key == Keys.Back)
                {
                    State = ReturnCode.Cancel;
                    RemoveMarkerLine();
                    return;
                }
            }
        }

        private void IncrementSelector()
        {
            Selector++;
            if (Selector >= Targets.Count)
                Selector = 0;
        }


        private void CreateShootEvent()
        {
            IEventInfo info = new EventInfoModel(Info.Scene);
            info.SubjectActor = Info.SubjectActor;
            info.TargetActor = Targets[Selector];
            info.Cost = 1;

            IEvent ev = new E_Shoot(info);
            Info.Scene.Events.Add(ev);
        }

        private void CreateMarkerLine(Vector3 origin, Vector3 target)
        {
            RemoveMarkerLine();

            LineToTarget = new Line(origin, target);

            foreach (Vector3 point in LineToTarget.Points)
            {
                ITile tile = Info.Scene.Tiles.GetTile(point);
                tile.Marker = MarkerGraphic;
            }
        }

        private void RemoveMarkerLine()
        {
            if (LineToTarget == null) return;

            foreach (Vector3 point in LineToTarget.Points)
            {
                ITile tile = Info.Scene.Tiles.GetTile(point);
                tile.Marker = null;
            }

            LineToTarget = null;
        }

        public override bool CheckPreconditions()
        {
            if (Info.SubjectActor == null) return false;

            if (Info.SubjectActor.Knowledge.EnemyCount <= 0) return false;

            // the attacker has a gun
            if (Info.SubjectActor.Inventory.Equipped == null) return false;
            if (Info.SubjectActor.Inventory.Equipped.Gun == null) return false;

            return true;
        }
    }
}
