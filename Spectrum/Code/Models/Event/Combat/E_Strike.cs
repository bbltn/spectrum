﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    class E_Strike : EventModel
    {
        private const TermColor BadColor = TermColor.LightRed;
        private const TermColor GoodColor = TermColor.LightGreen;
        private const TermColor StandardForeColor = TermColor.LightGray;
        private const TermColor StandardBackColor = TermColor.Black;

        private const int BaseDC = 6;

        public E_Strike(IEventInfo info)
            : base(info)
        {
        }

        public override void Process(BaseCommand command)
        {
            // roll to-hit
            bool hit = RollToHit(Info.SubjectActor, Info.TargetActor, Info.Scene.Messages);

            if (!hit)
            {
                PrintMissedMessage();                
                State = ReturnCode.Done;
                return;
            }

            // roll bonus damage
            int bonusDamage = RollBonusDamage(Info.SubjectActor, Info.TargetActor);

            DealDamage(Info.TargetActor, bonusDamage, Info.Scene.Messages);

            State = ReturnCode.Done;
            return;
        }

        private static void DealDamage(IActor target, int bonus, IMessageLog messages)
        {
            TermColor foreColor = GoodColor;
            if (target.Control == ControlType.Player) foreColor = BadColor;

            int killThreshold = 4;
            int heavyWoundThreshold = 2;

            if (bonus >= killThreshold || target.Health.Status == HealthStatusType.HeavyWounds)
            {
                messages.Add(new CharacterString("The strike kills " + target.Name.Full + ".", foreColor, StandardBackColor));
                target.Stance = StanceType.Prone;
                target.Health.Status = HealthStatusType.Dead;
                return;
            }

            if (bonus >= heavyWoundThreshold || target.Health.Status == HealthStatusType.LightWounds)
            {
                messages.Add(new CharacterString("The strike heavily wounds " + target.Name.Full + ".", foreColor, StandardBackColor));
                target.Health.Status = HealthStatusType.HeavyWounds;
                return;
            }

            messages.Add(new CharacterString("The strike wounds " + target.Name.Full + ".", foreColor, StandardBackColor));
            target.Health.Status = HealthStatusType.LightWounds;
        }

        private static int RollBonusDamage(IActor subject, IActor target)
        {
            int dc = BaseDC;

            dc += subject.Skill.Unarmed;
            dc -= target.Skill.Unarmed;

            if (subject.Inventory.Equipped != null)
                dc += subject.Inventory.Equipped.BashDamage;

            IDie first = new Die(6);
            IDie second = new Die(6);
            int roll = Roll.Sum(first, second);

            if (roll <= dc) return Math.Abs(roll - dc);

            return 0;
        }

        private static bool RollToHit(IActor subject, IActor target, IMessageLog messages)
        {
            int dc = BaseDC;

            dc += subject.Skill.Unarmed;
            dc -= target.Skill.Unarmed;

            if (subject.Inventory.Equipped != null)
                dc += subject.Inventory.Equipped.BashAptitude;

            IDie first = new Die(6);
            IDie second = new Die(6);

            //messages.Add(Roll.Sum(first, second) + " / " + dc + " ( " + BaseDC + " + " + subject.Inventory.Equipped.BashDamage + " ) ");

            return Roll.Sum(first, second) <= dc || Roll.SnakeEyes(first, second);
        }

        private void PrintMissedMessage()
        {
            TermColor foreColor = StandardForeColor;

            if (Info.TargetActor == Info.Scene.Actors.GetPlayer)
                foreColor = BadColor;
            else
                foreColor = GoodColor;

            Info.Scene.Messages.Add(Info.SubjectActor.Name.Full + " swings at " + Info.TargetActor.Name.Full + " but misses.", foreColor, StandardBackColor);
        }

        public override bool CheckPreconditions()
        {
            // we have an attacker
            if (Info.SubjectActor == null) return false;

            // we have a target
            if (Info.TargetActor == null) return false;
            
            // target isn't already dead
            if (Info.TargetActor.Health.Status == HealthStatusType.Dead) return false;
            
            return true;
        }
    }
}
