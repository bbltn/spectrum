﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Malison.Core;

namespace Spectrum
{
    public class E_Shoot : EventModel
    {
        private const TermColor BadColor = TermColor.LightRed;
        private const TermColor GoodColor = TermColor.LightGreen;
        private const TermColor StandardForeColor = TermColor.LightGray;
        private const TermColor StandardBackColor = TermColor.Black;

        private const int BaseDC = 6;

        public E_Shoot(IEventInfo info)
            : base(info)
        {
        }

        override public void Process(BaseCommand command)
        {
            if (CheckPreconditions() == false)
            {
                State = ReturnCode.Cancel;
                return;
            }

            
            // cache some useful values
            float distanceToTarget = Vector3.Distance(Info.SubjectActor.Position, Info.TargetActor.Position);
            IGun gun = Info.SubjectActor.Inventory.Equipped.Gun;

            // auto-fail if target out of range
            if (TargetOutOfRange(distanceToTarget, gun.Range))
            {
                TermColor resultColor = BadColor;
                if (Info.TargetActor.Control == ControlType.Player) resultColor = GoodColor;
                Info.Scene.Messages.Add(new CharacterString(Info.TargetActor.Name.Full + " is out of range. The shot misses.", resultColor, StandardBackColor));
                State = ReturnCode.Done;
                return;
            }

            // roll for to-hit
            bool hit = RollToHit(distanceToTarget, gun, Info.SubjectActor.Skill.Gun);

            // roll for to-damage
            if (hit)
            {
                RollDamage(distanceToTarget, gun);

                Vector3 direction = Info.TargetActor.Position - Info.SubjectActor.Position;
                SprayDecals("Blood Splatter", direction);

                if (Info.TargetActor.Health.Status == HealthStatusType.Dead)
                {
                    Info.TargetActor.Stance = StanceType.Prone;
                    DropDecal("Blood Pool");
                }
            }

            State = ReturnCode.Done;
        }

        private void DropDecal(string DecalName)
        {
            Vector3 position = Info.TargetActor.Position;
            position = new Vector3(position.X, position.Y, position.Z - 1);

            ITile tile = Info.Scene.Tiles.GetTile(position);

            if (tile.Feature != null)
                tile.Detail = Info.Scene.Resources.CreateDetailInstance(DecalName);
        }

        

        private bool RollToHit(float distanceToTarget, IGun gun, int skill)
        {
            int DC = GetToHitDC(distanceToTarget, gun, skill);
            IDie first = new Die(6);
            IDie second = new Die(6);
            if (Roll.Sum(first, second) > DC)
            {
                // shot misses - add a morale check!

                TermColor hitResultColor = BadColor;
                if (Info.TargetActor.Control == ControlType.Player) hitResultColor = GoodColor;
                Info.Scene.Messages.Add(new CharacterString("The shot misses " + Info.TargetActor.Name.Full + ".", hitResultColor, StandardBackColor));
                return false;
            }

            return true;
        }

        private void RollDamage(float distanceToTarget, IGun gun)
        {
            int DC = GetToDamageDC(distanceToTarget, gun, Info.TargetActor.Inventory.Wearing);
            IDie first = new Die(6);
            IDie second = new Die(6);
            int sum = Roll.Sum(first, second);

            if (sum <= DC)
                DealCriticalDamage(DC, sum);
            // failed the damage roll, increment wounds
            else
                IncrementDamage();
        }


        private void DealCriticalDamage(int DC, int rollSum)
        {
            TermColor damageResultColor = GoodColor;
            if (Info.TargetActor.Control == ControlType.Player) damageResultColor = BadColor;

            int spread = Math.Abs(rollSum - DC);

            // kill
            if (spread >= 2 || Info.TargetActor.Health.Status == HealthStatusType.HeavyWounds)
            {
                Info.Scene.Messages.Add(new CharacterString("The shot kills " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.Dead;
            }

            // heavily wound
            else if (spread >= 1 || Info.TargetActor.Health.Status == HealthStatusType.LightWounds)
            {
                Info.Scene.Messages.Add(new CharacterString("The shot heavily wounds " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.HeavyWounds;
            }

            // lightly wound
            else
            {
                Info.Scene.Messages.Add(new CharacterString("The shot wounds " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.LightWounds;
            }
        }

        private void IncrementDamage()
        {
            TermColor damageResultColor = GoodColor;
            if (Info.TargetActor.Control == ControlType.Player) damageResultColor = BadColor;


            // kill
            if (Info.TargetActor.Health.Status == HealthStatusType.HeavyWounds)
            {
                Info.Scene.Messages.Add(new CharacterString("The shot kills " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.Dead;
            }

            // heavily wound
            else if (Info.TargetActor.Health.Status == HealthStatusType.LightWounds)
            {
                Info.Scene.Messages.Add(new CharacterString("The shot heavily wounds " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.HeavyWounds;
            }

            // lightly wound
            else
            {
                Info.Scene.Messages.Add(new CharacterString("The shot wounds " + Info.TargetActor.Name.Full + ".", damageResultColor, StandardBackColor));
                Info.TargetActor.Health.Status = HealthStatusType.LightWounds;
            }
        }


        public override bool CheckPreconditions()
        {
            // we have an attacker
            if (Info.SubjectActor == null) return false;

            // we have a target
            if (Info.TargetActor == null) return false;
            
            // target isn't already dead
            if (Info.TargetActor.Health.Status == HealthStatusType.Dead) return false;

            // the attacker has a gun
            if (Info.SubjectActor.Inventory.Equipped == null) return false;
            if (Info.SubjectActor.Inventory.Equipped.Gun == null) return false;

            // we have line of sight to the target
            if (!Info.SubjectActor.Vision.Contains(Info.Scene, Info.TargetActor.Position)) return false;

            return true;
        }

        private static int GetToDamageDC(float distanceToTarget, IGun gun, IGarment targetGarment)
        {
            int DC = BaseDC;

            /*
            Modifiers from weapon:
            - Small caliber: +2
            - Pellets: +1
            - Hollow point, unarmored target: -1
            - Armor piercing, armored target: -1
            - Large caliber: -2
            */

            switch (gun.Caliber)
            {
                case CaliberType.Cal_308: DC += 2; break;
                case CaliberType.Cal_762: DC += 1; break;
                case CaliberType.Cal_45acp: DC -= 1; break;
                case CaliberType.Cal_9mm: DC -= 1; break;
            }

            if (targetGarment != null)
            {
                if (targetGarment.Armor == ArmorType.Kevlar) DC -= 1;
                else if (targetGarment.Armor == ArmorType.Ceramic) DC -= 2;
            }

            if (TargetAtLongRange(distanceToTarget, gun.Range)) DC -= 1;

            return DC;
        }

        private static int GetToHitDC(float distanceToTarget, IGun gun, int skill)
        {
            int dc = BaseDC;

            // DC multipliers
            if (TargetAtPointBlank(distanceToTarget)) dc *= 2;
            if (TargetAtLongRange(distanceToTarget, gun.Range)) dc = (int)(dc * 0.5f);
            // TODO:  Target concealed: 1/2x
            // TODO:  Attacker moving: 1/2x

            // skill modifiers
            dc += skill;

            // DC modifiers from weapon
            if (gun.Stock == StockType.Fixed) dc += 2;
            else if (gun.Stock == StockType.None) dc -= 2;

            if (gun.Barrel == BarrelType.Short) dc -= 1;
            else if (gun.Barrel == BarrelType.Long) dc += 1;

            if (gun.Optic == OpticType.Reflex) dc += 1;
            else if (gun.Optic == OpticType.Scope)
            {
                if (TargetAtPointBlank(distanceToTarget)) dc -= 2;
                if (TargetAtLongRange(distanceToTarget, gun.Range)) dc += 4;
            }

            /*
            TODO
            Modifiers from context:
            - Target behind cover: +2
            - Target moving: -1
            - Attacker stationary: -1
            - Last attacked adjacent tile: -1
            - Last attacked same tile: -2
            */

            return dc;
        }

        private static bool TargetOutOfRange(float distance, int range)
        {
            return distance > (range * 2);
        }

        private static bool TargetAtPointBlank(float distance)
        {
            return distance <= 3;
        }

        private static bool TargetAtLongRange(float distance, int range)
        {
            if (TargetOutOfRange(distance,range)) return false;
            if (distance > range) return true;
            return false;
        }
    }
}
