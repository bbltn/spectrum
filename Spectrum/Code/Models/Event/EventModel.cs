﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public enum ReturnCode { Done, NotDone, Cancel }

    public interface IEvent
    {
        bool CheckPreconditions();
        void Process(BaseCommand command);
        ReturnCode State { get; }
        IEventInfo Info { get; }
    }

    public class EventModel : IEvent
    {
        public ReturnCode State { get; protected set; }
        public IEventInfo Info { get; protected set; }

        public EventModel(IEventInfo info)
        {
            Info = info;
            State = ReturnCode.NotDone;
        }

        virtual public bool CheckPreconditions()
        {
            return true;
        }

        virtual public void Process(BaseCommand command)
        {
            State = ReturnCode.Done;
        }

        protected void SprayDecals(string DecalName, Vector3 direction)
        {
            int minSplatters = 2;
            int maxSplatters = 4;


            Vector3 normalizedDirection = Vector3.CastValuesToInt(Vector3.Normalize(direction));

            Random RNG = new Random();

            int splatterNumber = RNG.Next(minSplatters, maxSplatters);

            for (int iter = 0; iter < splatterNumber; iter++)
            {
                if (iter != 0)
                {
                    int jitterX = RNG.Next(-1, 1);
                    int jitterY = RNG.Next(-1, 1);

                    normalizedDirection = new Vector3(normalizedDirection.X + jitterX, normalizedDirection.Y + jitterY, normalizedDirection.Z);
                }

                ITile tile = Info.Scene.Tiles.GetTile(Info.TargetActor.Position + normalizedDirection);

                if (tile != null)
                {
                    if (tile.Feature != null)
                        tile.Detail = Info.Scene.Resources.CreateDetailInstance(DecalName);

                    else
                    {
                        Vector3 modDirection = new Vector3(normalizedDirection.X, normalizedDirection.Y, normalizedDirection.Z - 1);
                        tile = Info.Scene.Tiles.GetTile(Info.TargetActor.Position + modDirection);
                        if (tile.Feature != null)
                            tile.Detail = Info.Scene.Resources.CreateDetailInstance(DecalName);
                    }
                }
            }
        }
    }
}
