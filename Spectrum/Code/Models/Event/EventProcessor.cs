﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IEventProcessor
    {
        void Add(IEvent newEvent);
        void Remove(IEvent targetEvent);
        int Count { get; }

        void ProcessEvents(BaseCommand command);
        bool CurrentlyProcessing();
    }

    class EventProcessor : IEventProcessor
    {
        private List<IEvent> Events;
        public int Count { get { return Events.Count; } }

        public EventProcessor()
        {
            Events = new List<IEvent>();
        }

        public void Add(IEvent newEvent)
        {
            if (newEvent == null) return;

            if (newEvent.CheckPreconditions() == false)
                return;
            
            Events.Add(newEvent);

            // register event with the Subject actor
            if (newEvent.Info.SubjectActor != null)
                newEvent.Info.SubjectActor.CurrentEvent = newEvent;
        }

        public void Remove(IEvent targetEvent)
        {
            if (targetEvent == null) return;

            Events.Remove(targetEvent);

            // deregister event from Subject actor
            if (targetEvent.Info.SubjectActor != null)
                if (targetEvent.Info.SubjectActor.CurrentEvent == targetEvent)
                    targetEvent.Info.SubjectActor.CurrentEvent = null;
        }


        public void ProcessEvents(BaseCommand command)
        {
            if (Events.Count > 0)
            {
                List<IEvent> finished = new List<IEvent>();

                // see if there's a player event and do it first
                foreach (var ev in Events)
                {
                    if (ev.Info.ReadyToProcess && ev.State == ReturnCode.NotDone)
                        if (ev.Info.SubjectActor != null)
                            if (ev.Info.SubjectActor.Control == ControlType.Player)
                            {
                                ev.Process(command);
                                if (ev.State == ReturnCode.Cancel || ev.State == ReturnCode.Done)
                                    Remove(ev);
                                break;
                            }
                }

                // get out if that was the only event
                if (Events.Count <= 0)
                    return;

                // go through events and process the ones that are ready
                // mark any Done or Cancel events for removal
                for (int iter = 0; iter < Events.Count; iter++)
                {
                    IEvent curEvent = Events[iter];

                    if (curEvent.Info.ReadyToProcess && curEvent.State == ReturnCode.NotDone)
                    {
                        curEvent.Process(command);
                    }

                    if (curEvent.State == ReturnCode.Cancel || curEvent.State == ReturnCode.Done)
                        finished.Add(curEvent);

                }

                // get rid of any events that we marked as done
                foreach (IEvent ev in finished)
                {
                    Remove(ev);
                }
            }
        }

        public bool CurrentlyProcessing()
        {
            if (Events.Count > 0)
                for (int iter = 0; iter < Events.Count; iter++)
                    if (Events[iter].Info.ReadyToProcess && Events[iter].State == ReturnCode.NotDone)
                        return true;
            return false;
        }
    }
}
