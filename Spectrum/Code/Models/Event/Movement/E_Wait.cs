﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Malison.Core;

namespace Spectrum
{
    public class E_Wait : EventModel
    {
        public E_Wait(IEventInfo info)
            : base(info)
        {
        }

        override public void Process(BaseCommand command)
        {
            if (CheckPreconditions() == false)
            {
                State = ReturnCode.Cancel;
                return;
            }

            //if (Info.SubjectActor != null)
            //{
                State = ReturnCode.Done;
                return;
            //}

            //else State = ReturnCode.Cancel;
        }

    }
}
