﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Malison.Core;

namespace Spectrum
{
    public class E_Move : EventModel
    {
        public E_Move(IEventInfo info)
            : base(info)
        {
        }

        override public void Process(BaseCommand command)
        {
            if (CheckPreconditions() == false)
            {
                State = ReturnCode.Cancel;
                return;
            }

            // get current position and tile
            var currentPosition = Info.SubjectActor.Position;
            var currentTile = Info.Scene.Tiles.GetTile(currentPosition);

            // get target position and tile
            var targetPosition = currentPosition + Info.Vector;
            var targetTile = Info.Scene.Tiles.GetTile(targetPosition);

            // perform the move
            currentTile.Actor = null;
            targetTile.Actor = Info.SubjectActor;
            Info.SubjectActor.Position = targetPosition;

            State = ReturnCode.Done;
            return;
        }

        public override bool CheckPreconditions()
        {
            if (Info.SubjectActor == null) return false;

            // the position we want to move to
            var modPosition = Info.SubjectActor.Position + Info.Vector;

            // cancel out if desired position does not exist
            if (!Info.Scene.Tiles.Size.Contains(modPosition))
            {
                return false;
            }

            // get the actual tile we want to move to
            var targetTile = Info.Scene.Tiles.GetTile(modPosition);

            // cancel out if tile is occupied
            if (targetTile.Actor != null)
            {
                return false;
            }

            // cancel out if tile has a blocking feature
            if (targetTile.Feature != null)
            {
                if (targetTile.Feature.BlocksSpace)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
