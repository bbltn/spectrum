﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ITrait
    {
        string Name { get; }
        string Description { get; }
    }

    public class TraitModel : ITrait
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public TraitModel()
        {
            Name = "trait_name";
            Description = "trait_description";
        }
    }

    public class TraitInstance : ITrait
    {
        private ITrait Base;
        public string Name { get { return Base.Name; } }
        public string Description { get { return Base.Description; } }

        public TraitInstance(ITrait baseTrait)
        {
            Base = baseTrait;
        }
    }
}
