﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ITraits
    {
        void Add(ITrait trait);
        bool Contains(string name);
        int Count { get; }
        ITrait GetTrait(int index);
    }

    public class TraitSet : ITraits
    {
        private List<ITrait> Traits;

        public TraitSet()
        {
            Traits = new List<ITrait>();
        }

        public void Add(ITrait trait)
        { if (!Traits.Contains(trait)) Traits.Add(trait); }

        public bool Contains(string name)
        {
            foreach (var trait in Traits)
                if (trait.Name == name) return true;
            return false;
        }

        public int Count { get { return Traits.Count; } }

        public ITrait GetTrait(int index)
        { return Traits[index]; }
    }
}
