﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ITileSet
    {
        void Initialize(Vector3 size);

        Vector3 Size { get; }

        ITile GetTile(Vector3 position);
        
        List<INode> GetTileNeighbors(ITile tile);
    }

    public class TileSet : ITileSet
    {
        private ITile[, ,] Tiles;

        public Vector3 Size { get; private set; }

        public TileSet(Vector3 size)
        {
            Initialize(size);
        }

        public void Initialize(Vector3 size)
        {
            Size = size;

            Tiles = new TileModel[(int)Size.X, (int)Size.Y, (int)Size.Z];

            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    for (int z = 0; z < Size.Z; z++)
                    {
                        Tiles[x, y, z] = new TileModel(GetTileNeighbors, new Vector3(x, y, z));
                    }
                }
            }
        }

        public ITile GetTile(Vector3 position)
        {
            if (!Size.Contains(position))
                return null;

            if (Tiles[(int)position.X, (int)position.Y, (int)position.Z] == null)
                return null;

            return Tiles[(int)position.X, (int)position.Y, (int)position.Z];
        }

        public List<INode> GetTileNeighbors(ITile tile)
        {
            List<INode> neighbors = new List<INode>();

            if (tile == null) return neighbors;

            Vector3 offset = tile.Position;

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    for (int z = -1; z <= 1; z++)
                    {
                        Vector3 targetPosition = new Vector3(x + offset.X, y + offset.Y, z + offset.Z);
                        ITile targetTile = GetTile(targetPosition);
                        if (targetTile != null && targetTile != tile)
                            neighbors.Add(targetTile.Node);
                    }
                }
            }

            return neighbors;
        }
    }
}
