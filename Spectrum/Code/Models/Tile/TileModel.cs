﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ITile
    {
        Vector3 Position { get; set; }
        IFeature Feature { get; set; }
        IActor Actor { get; set; }
        IDetail Detail { get; set; }
        IGas Gas { get; set; }
        INode Node { get; }
        IGraphic Marker { get; set; }
        bool Explored { get; set; }

        int ItemCount { get; }
        void AddItem(IItem item);
        IItem GetItem(int index);
        void RemoveItem(int index);

        bool BlocksVision();
        bool BlocksMovement();
    }

    public delegate List<INode> GetNeighborTileFunction(ITile target);

    public class TileModel : ITile, INode
    {
        // ITile implementations
        public Vector3 Position { get; set; }
        public IFeature Feature { get; set; }
        public IActor Actor { get; set; }
        public IDetail Detail { get; set; }
        public IGas Gas { get; set; }
        public INode Node { get { return this; } }
        private List<IItem> Items;
        public IGraphic Marker { get; set; }
        public bool Explored { get; set; }

        public TileModel(GetNeighborTileFunction neighborFunction, Vector3 position)
        {
            GetNeighbors = neighborFunction;
            Position = position;
            Items = new List<IItem>();
            Explored = false;
        }


        public int ItemCount { get { return Items.Count; } }

        public void AddItem(IItem item)
        { Items.Add(item); }

        public void RemoveItem(int index)
        { Items.RemoveAt(index); }

        public IItem GetItem(int index)
        { return Items[index]; }

        public bool BlocksVision()
        {
            if (Feature != null)
                if (Feature.BlocksVision)
                    return true;
            if (Gas != null)
                if (Gas.BlocksVision)
                    return true;
            return false;
        }

        public bool BlocksMovement()
        {
            if (Feature != null)
                if (Feature.BlocksSpace)
                    return true;
            if (Actor != null)
                return true;
            return false;
        }


        // INode implementations
        private GetNeighborTileFunction GetNeighbors;
        public List<INode> GetAdjacentNodes
        {
            get
            {
                return GetNeighbors(this);
            }
        }

        public List<float> GetNodeValues
        {
            get
            {
                return NodeValues.CreateForTile(this);
            }
        }


    }
}
