﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Malison.Core;

namespace Spectrum
{
    public interface ICamera
    {
        Vector3 Position { get; set; }
        
        IGraphic[,] DrawScene(int frameBoundX, int frameBoundY, IVariables variables);
        void Follow(Vector3 targetPosition);

        void Raise();
        void Lower();

        IActor Perspective { get; set; }
    }

    public class CameraModel : ICamera
    {
        public Vector3 Position { get; set; }
        private IScene Scene { get; set; }
        private const float TrailDistance = 3;
        private const int DimHeight = 2;
        private int OffsetZ { get; set; }
        public IActor Perspective { get; set; }
        private IGraphic EmptyGraphic;

        public CameraModel(Vector3 position, IScene scene)
        {
            OffsetZ = 0;
            Position = new Vector3(position.X, position.Y, position.Z + OffsetZ);
            Scene = scene;
            //Perspective = perspective;
            EmptyGraphic = new AsciiGraphic(new Character(' '), TermColor.DarkGray, TermColor.DarkGray);
        }

        public void Raise()
        {
            if (Position.Z < Scene.Tiles.Size.Z - 1)
            {
                OffsetZ++;
                Position = new Vector3(Position.X, Position.Y, Position.Z + 1);
            }
        }

        public void Lower()
        {
            if (Position.Z > 1)
            {
                OffsetZ--;
                Position = new Vector3(Position.X, Position.Y, Position.Z - 1);
            }
        }

        public void Follow(Vector3 targetPosition)
        {
            Vector3 testPosition = new Vector3(targetPosition.X, targetPosition.Y, Position.Z);

            float x = Position.X;
            float y = Position.Y;

            if (Vector3.Distance(Position, testPosition) > TrailDistance)
            {
                if (targetPosition.X > Position.X)
                    x++;
                else if (targetPosition.X < Position.X)
                    x--;

                if (targetPosition.Y > Position.Y)
                    y++;
                else if (targetPosition.Y < Position.Y)
                    y--;
            }

            Position = new Vector3(x, y, targetPosition.Z + OffsetZ);
        }

        private static IGraphic ModifyGraphicWithDetail(IGraphic baseGraphic, IDetail detail)
        {
            TermColor fore = baseGraphic.ForeColor;
            TermColor back = baseGraphic.BackColor;
            Character icon = baseGraphic.Icon;

            if (detail.OverrideForeColor)
                fore = detail.Graphic.ForeColor;

            if (detail.OverrideBackColor)
                back = detail.Graphic.BackColor;

            if (detail.OverrideIcon)
                icon = detail.Graphic.Icon;

            return new AsciiGraphic(icon,fore,back);
        }


        public IGraphic[,] DrawScene(int frameBoundX, int frameBoundY, IVariables variables)
        {
            IGraphic[,] frame = new IGraphic[frameBoundX,frameBoundY];

            for (int frameX = 0; frameX < frameBoundX; frameX++)
            {
                for (int frameY = 0; frameY < frameBoundY; frameY++)
                {
                    int sceneX = (int)(Position.X - (frameBoundX / 2)) + frameX;
                    int sceneY = (int)(Position.Y - (frameBoundY / 2)) + frameY;
                    int sceneZ = (int)Position.Z;

                    // if the x/y is out of scene bounds we might as well not check each non-existant tile
                    if (sceneX < 0 || sceneX >= Scene.Tiles.Size.X || sceneY < 0 || sceneY >= Scene.Tiles.Size.Y)                    
                    {
                        frame[frameX, frameY] = EmptyGraphic;
                    }

                    else
                    {
                        DrawPosition(frame, frameX, frameY, sceneX, sceneY, sceneZ, variables);
                    }
                }
            }

            return frame;
        }

        private void DrawPosition(IGraphic[,] frame, int frameX, int frameY, int sceneX, int sceneY, int sceneZ, IVariables variables)
        {
            bool drillingDown = true;
            while (drillingDown)
            {
                Vector3 testPosition = new Vector3
                    (sceneX, sceneY, sceneZ);

                var targetTile = Scene.Tiles.GetTile(testPosition);

                if (targetTile != null)
                {
                    // if draw all actors is on and this tile has an actor, draw it regardless of other factors
                    if (targetTile.Actor != null && variables.Debug.DrawAllActors)
                    {
                        DrawActor(frame, targetTile, frameX, frameY, true);
                        break;
                    }
                    
                    bool visible = Perspective.Vision.Contains(Scene, targetTile.Position);

                    if (visible && !targetTile.Explored)
                        targetTile.Explored = true;

                    if (!targetTile.Explored)
                    {
                        frame[frameX, frameY] = EmptyGraphic;
                        drillingDown = false;
                    }

                    else if (targetTile.Actor != null)
                    {
                        // if we have a Perspective and can't see the tile, don't draw the actor
                        if (Perspective != null)
                        {
                            if (visible)
                            {
                                DrawActor(frame, targetTile, frameX, frameY, visible);
                                drillingDown = false;
                            }
                        }

                        // if there's no Perspective then draw the actor freely
                        else
                        {
                            DrawActor(frame, targetTile, frameX, frameY, visible);
                            drillingDown = false;
                        }
                    }


                    else if (targetTile.Marker != null)
                    {
                        DrawMarker(frame, targetTile, frameX, frameY);
                        drillingDown = false;
                    }

                    else if (targetTile.ItemCount > 0)
                    {
                        DrawItem(frame, targetTile, frameX, frameY, visible);
                        drillingDown = false;
                    }

                    else if (targetTile.Gas != null)
                    {
                        DrawGas(frame, targetTile, frameX, frameY, visible);
                        drillingDown = false;
                    }

                    else if (targetTile.Feature != null)
                    {
                        DrawFeature(frame, targetTile, frameX, frameY, visible);
                        drillingDown = false;
                    }
                }

                if (drillingDown)
                {
                    sceneZ--;

                    // draw some kind of null graphic if there's no more tiles to go down through
                    if (sceneZ < 0)
                    {
                        DrawVoid(frame, frameX, frameY);
                        drillingDown = false;
                    }
                }
            }
        }


        private static void DrawVoid(IGraphic[,] frame, int printX, int printY)
        {
            frame[printX, printY] = new AsciiGraphic(new Character('.'), TermColor.Gray, TermColor.DarkGray);

        }
        
        private void DrawMarker(IGraphic[,] frame, ITile targetTile, int printX, int printY)
        {
            frame[printX, printY] = targetTile.Marker;
        }

        private void DrawFeature(IGraphic[,] frame, ITile targetTile, int printX, int printY, bool visible)
        {
            IGraphic graphic = targetTile.Feature.Graphic;

            if (targetTile.Detail != null)
                graphic = ModifyGraphicWithDetail(graphic, targetTile.Detail);

            if (targetTile.Position.Z < Position.Z - DimHeight)
                graphic = graphic.DimColors();

            if (Perspective != null)
            {
                if (!visible)
                    graphic = graphic.DimColors();
            }

            frame[printX, printY] = graphic;
        }

        private void DrawGas(IGraphic[,] frame, ITile targetTile, int printX, int printY, bool visible)
        {
            IGraphic graphic = targetTile.Gas.Graphic;

            if (targetTile.Position.Z < Position.Z - DimHeight)
                graphic = graphic.DimColors();

            if (Perspective != null)
            {
                if (!visible)
                    graphic = graphic.DimColors();
            }

            frame[printX, printY] = graphic;
        }

        private void DrawItem(IGraphic[,] frame, ITile targetTile, int printX, int printY, bool visible)
        {
            IGraphic graphic = targetTile.GetItem(0).Graphic;

            if (targetTile.Detail != null)
                graphic = ModifyGraphicWithDetail(graphic, targetTile.Detail);

            if (targetTile.Position.Z < Position.Z - DimHeight)
                graphic = graphic.DimColors();

            if (Perspective != null)
            {
                if (!visible)
                    graphic = graphic.DimColors();
            }

            frame[printX, printY] = graphic;
        }

        private void DrawActor(IGraphic[,] frame, ITile targetTile, int printX, int printY, bool visible)
        {
            IGraphic graphic = targetTile.Actor.Graphic;

            switch (targetTile.Actor.Health.Status)
            {
                case HealthStatusType.LightWounds:
                    graphic = new AsciiGraphic(graphic.Icon, TermColor.Yellow, graphic.BackColor);
                    break;

                case HealthStatusType.HeavyWounds:
                    graphic = new AsciiGraphic(graphic.Icon, TermColor.Orange, graphic.BackColor);
                    break;

                case HealthStatusType.Dead:
                    graphic = new AsciiGraphic(new Character('%'), TermColor.Flesh, TermColor.DarkRed);
                    //graphic.Icon = new Character('%');
                    //graphic.ForeColor = TermColor.Flesh;
                    //graphic.BackColor = TermColor.DarkRed;
                    break;
            }


            if (targetTile.Detail != null)
                graphic = ModifyGraphicWithDetail(graphic, targetTile.Detail);

            if (targetTile.Position.Z < Position.Z - DimHeight)
                graphic = graphic.DimColors();

            if (Perspective != null)
            {
                if (!visible)
                    graphic = graphic.DimColors();
            }

            frame[printX, printY] = graphic;
        }





    }
}
