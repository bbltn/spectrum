﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public class DetailInstance : IDetail
    {
        private IDetail Base { get; set; }

        public string Name { get { return Base.Name; } }
        public IGraphic Graphic { get { return Base.Graphic; } }
        public bool OverrideForeColor { get { return Base.OverrideForeColor; } }
        public bool OverrideBackColor { get { return Base.OverrideBackColor; } }
        public bool OverrideIcon { get { return Base.OverrideIcon; } }

        public DetailInstance(IDetail baseDetail)
        {
            Base = baseDetail;
        }
    }
}
