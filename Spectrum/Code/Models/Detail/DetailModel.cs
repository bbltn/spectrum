﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public interface IDetail
    {
        string Name { get; }
        IGraphic Graphic { get; }
        bool OverrideForeColor { get; }
        bool OverrideBackColor { get; }
        bool OverrideIcon { get; }
    }

    public class DetailModel : IDetail
    {
        public string Name { get; set; }

        public IGraphic Graphic { get; set; }

        public bool OverrideForeColor { get; set; }
        public bool OverrideBackColor { get; set; }
        public bool OverrideIcon { get; set; }

        public DetailModel()
        {
            Name = "Detail_name";
            Graphic = new AsciiGraphic();
            OverrideForeColor = false;
            OverrideBackColor = false;
            OverrideIcon = false;
        }
    }
}
