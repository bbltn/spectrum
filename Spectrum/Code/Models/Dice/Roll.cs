﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    static public class Roll
    {
        static public int Sum(IDie first, IDie second)
        {
            if (first == null && second == null) return 0;
            else if (first == null) return second.Result;
            else if (second == null) return first.Result;

            return first.Result + second.Result;
        }

        static public bool SnakeEyes(IDie first, IDie second)
        {
            if (first == null || second == null) return false;

            return (first.Result == 1 && second.Result == 1);
        }

        static public bool Doubles(IDie first, IDie second)
        {
            if (first == null || second == null) return false;

            return (first.Result == second.Result);
        }

        static public bool HighestPossible(IDie first, IDie second)
        {
            if (first == null || second == null) return false;

            return (first.Result == first.SideCount && second.Result == second.SideCount);
        }        
    }
}
