﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IDie
    {
        int SideCount { get; }
        int Result { get; }
    }

    public class Die : IDie
    {
        public int SideCount { get; private set; }
        public int Result { get; private set; }

        public Die(int range)
        {
            SideCount = range;
            Result = new Random(Guid.NewGuid().GetHashCode()).Next(SideCount) + 1;
        }
    }
}
