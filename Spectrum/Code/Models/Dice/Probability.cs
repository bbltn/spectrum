﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class Probability
    {
        public static float SuccessPercentage(int dc, int modifier)
        {
            /*
                Get the % chance of beating the DC with the given modifier.
                Assumes beating the DC means rolling equal to it or lower.
                Assumes a roll of 2d6.
            */

            // modify the DC so we can just look at what needs to be rolled
            int modifiedDC = dc - modifier;

            // DC is lower than lowest possible roll, success impossible
            if (modifiedDC < 2)
                return 0;

            // DC is higher than greatest possible roll, success guaranteed
            if (modifiedDC > 12)
                return 100;

            // the number of combinations we could roll to beat the DC
            int winningCombinations = 0;

            // count through winning rolls to add their possible combinations
            for (int i = 2; i <= modifiedDC; i++)
            {
                winningCombinations += PossibleCombinations(i);
            }

            int totalPossibleCombinations = 36;
            return (winningCombinations / totalPossibleCombinations) * 100;
        }

        private static int PossibleCombinations(int value)
        {
            /*
                Get the number of possible combinations of two D6
                that could hit the given value.
            */

            if (value < 2 || value > 12) return 0;

            return (6 - DistanceFromSeven(value));
        }

        private static int DistanceFromSeven(int value)
        {
            return Math.Abs(7 - value);
        }
    }
}
