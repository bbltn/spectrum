﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ISound
    {
        Vector3 Origin { get; }
        IActor Creator { get; }
        ITime Time { get; }
    }

    public class Sound : ISound
    {
        public Vector3 Origin { get; private set; }
        public IActor Creator { get; private set; }
        public ITime Time { get; private set; }

        public Sound(Vector3 origin, ITime time)
        {
            Origin = origin;
            Time = time;
        }

        public Sound(Vector3 origin, ITime time, IActor creator)
        {
            Origin = origin;
            Time = time;
            Creator = creator;
        }
    }
}
