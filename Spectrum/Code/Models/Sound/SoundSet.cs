﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IHearing
    {
        int Count { get; }
        void Add(ISound value);
        ISound GetSound(int index);
        void Remove(int index);
        void Remove(ISound value);
    }

    public class SoundSet : IHearing
    {
        private List<ISound> Sounds;

        public int Count { get { return Sounds.Count; } }


        public void Add(ISound value)
        {
            Sounds.Add(value);
        }

        public ISound GetSound(int index)
        {
            return Sounds[index];
        }

        public void Remove(int index)
        {
            Sounds.RemoveAt(index);
        }

        public void Remove(ISound value)
        {
            if (Sounds.Contains(value))
                Sounds.Remove(value);
        }

        public SoundSet()
        {
            Sounds = new List<ISound>();
        }
    }
}
