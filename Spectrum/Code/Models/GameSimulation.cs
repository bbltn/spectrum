﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public class GameSimulation
    {
        public bool Active { get; set; }
        public IResources Resources { get; private set; }
        public IScene Scene { get; private set; }
        public IVariables Variables { get; private set; }
        public List<BaseCommand> Commands { get; private set; }
        
        public GameSimulation()
        {
            Active = false;

            Resources = new ResourceSet();

            Variables = new VariableSet();

            Commands = new List<BaseCommand>();

            CreateScene(dimensions: new Vector3(10, 10, 3));
        }

        private void CreateScene(Vector3 dimensions)
        {
            Scene = new SceneModel(dimensions, Resources);
        }

        public void Update()
        {
            /*
            if (Scene.Objectives.IsComplete)
            {
                Active = false;
            }
             */

            if (!Active) return;            

            if (Commands.Count > 0)
            {
                // escape back to menu
                KeyCommand letter = Commands[0] as KeyCommand;
                if (letter != null)
                {
                    //if (letter.Letter == 'q' || letter.Letter == 'Q')
                    if (letter.Key == System.Windows.Forms.Keys.Escape)
                    {
                        Active = false;
                        Commands.RemoveAt(0);
                        return;
                    }
                }

                Scene.Update(Commands[0]);
                Commands.RemoveAt(0);
            }
            else
                Scene.Update(null);
        }
        
        public void AddCommand(BaseCommand command)
        {
            Commands.Add(command);
        }  
    }
}
