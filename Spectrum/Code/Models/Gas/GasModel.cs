﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IGas
    {
        string Name { get; }
        IGraphic Graphic { get; }
        bool BlocksVision { get; }
    }

    class GasModel : IGas
    {
        public string Name { get; set; }
        public IGraphic Graphic { get; set; }
        public bool BlocksVision { get; set; }
    }

    class GasInstance : IGas
    {
        private IGas Base;

        public GasInstance(IGas baseGas)
        {
            Base = baseGas;
        }

        public string Name { get { return Base.Name; } }
        public IGraphic Graphic { get { return Base.Graphic; } }
        public bool BlocksVision { get { return Base.BlocksVision; } }
    }
}
