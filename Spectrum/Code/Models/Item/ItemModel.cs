﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public enum SizeType { Tiny, Small, Medium, Large }

    public interface IItem
    {
        string Name { get; }
        string Description { get; }
        IGraphic Graphic { get; }
        SizeType Size { get; }
        int Weight { get; }
        int BashAptitude { get; }
        int BashDamage { get; }
        List<string> Tags { get; }

        IGun Gun { get; }
        IGarment Garment { get; }
    }
    
    public class ItemModel : IItem
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IGraphic Graphic { get; set; }
        public SizeType Size { get; set; }
        public int Weight { get; set; }
        public int BashAptitude { get; set; }
        public int BashDamage { get; set; }

        virtual public IGun Gun { get { return null; } }
        virtual public IGarment Garment { get { return null; } }

        public List<string> Tags { get; private set; }

        public ItemModel()
        {
            Name = "item_name";
            Description = "item_description";
            Graphic = new AsciiGraphic();
            Size = SizeType.Medium;
            Weight = 5;
            BashAptitude = 6;
            BashDamage = 4;
            Tags = new List<string>();
        }
    }

    
}
