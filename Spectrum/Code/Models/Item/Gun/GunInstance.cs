﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class GunInstance : IGun
    {
        public IGun Base { get; private set; }
        public GunActionType CurrentAction { get; set; }

        public GunInstance(IGun baseGun)
        {
            if (baseGun == null) Base = new GunModel();

            Base = baseGun;
            CurrentAction = Base.CurrentAction;
        }

        public int ActionCount { get { return Base.ActionCount; } }
        public GunActionType GetAction(int index) { return Base.GetAction(index); }
        public bool HasAction(GunActionType type) { return Base.HasAction(type); }
        public void AddAction(GunActionType action) { Base.AddAction(action); }

        public CaliberType Caliber { get { return Base.Caliber; } }
        public StockType Stock { get { return Base.Stock; } }
        public BarrelType Barrel { get { return Base.Barrel; } }
        public OpticType Optic { get { return Base.Optic; } }
        public ForegripType Foregrip { get { return Base.Foregrip; } }
        public MuzzleType Muzzle { get { return Base.Muzzle; } }

        public int Range { get { return Base.Range; } }

        public IItem Item { get { return Base.Item; } }
    }
}
