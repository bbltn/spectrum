﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public enum CaliberType { Cal_9mm, Cal_45acp, Cal_308, Cal_556, Cal_762 };
    public enum GunActionType { Single, Semi, Burst, Full };
    public enum StockType { None, Fixed }
    public enum BarrelType { Short, Standard, Long }
    public enum OpticType { Irons, Reflex, Scope }
    public enum ForegripType { None, Vertical }
    public enum MuzzleType { None, Suppressor }

    public interface IGun
    {
        int ActionCount { get; }
        GunActionType GetAction(int index);
        bool HasAction(GunActionType type);
        void AddAction(GunActionType action);

        GunActionType CurrentAction { get; set; }

        CaliberType Caliber { get; }
        StockType Stock { get; }
        BarrelType Barrel { get; }
        OpticType Optic { get; }
        ForegripType Foregrip { get; }
        MuzzleType Muzzle { get; }

        int Range { get; }

        IItem Item { get; }
    }

    public class GunModel : ItemModel, IGun
    {
        public List<GunActionType> Actions { get; set; }
        public GunActionType CurrentAction { get; set; }
        public CaliberType Caliber { get; set; }
        public StockType Stock { get; set; }
        public BarrelType Barrel { get; set; }
        public OpticType Optic { get; set; }
        public ForegripType Foregrip { get; set; }
        public MuzzleType Muzzle { get; set; }

        public int Range { get; set; }


        public GunModel()
            : base()
        {
            Actions = new List<GunActionType>();
            CurrentAction = GunActionType.Semi;
            Caliber = CaliberType.Cal_9mm;
            Stock = StockType.Fixed;
            Barrel = BarrelType.Standard;
            Optic = OpticType.Irons;
            Foregrip = ForegripType.None;
            Muzzle = MuzzleType.None;
            Range = 8;
        }


        public int ActionCount { get { return Actions.Count; } }
        public GunActionType GetAction(int index) { return Actions[index]; }
        public bool HasAction(GunActionType type) { return Actions.Contains(type); }
        public void AddAction(GunActionType action)
        {
            if (!Actions.Contains(action))
                Actions.Add(action);
        }

        public IItem Item { get { return this; } }
        override public IGun Gun { get { return this; } }
    }
}
