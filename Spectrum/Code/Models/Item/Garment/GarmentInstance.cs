﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class GarmentInstance : IGarment
    {
        private IGarment Base { get; set; }


        public GarmentInstance(IGarment baseGarment)
        {
            Base = baseGarment;
        }


        public BodyPartType BodyPart { get { return Base.BodyPart; } }
        public ArmorType Armor { get { return Base.Armor; } }

        public IItem Item { get { return Base.Item; } }
    }
}
