﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public enum BodyPartType { Torso };
    public enum ArmorType { None, Kevlar, Ceramic }

    public interface IGarment
    {
        BodyPartType BodyPart { get; }
        ArmorType Armor { get; }

        IItem Item { get; }
    }

    public class GarmentModel : ItemModel, IGarment
    {
        public BodyPartType BodyPart { get; set; }
        public ArmorType Armor { get; set; }

        public GarmentModel()
            : base()
        {
            BodyPart = BodyPartType.Torso;
            Armor = ArmorType.None;
        }

        public IItem Item { get { return this; } }
        override public IGarment Garment { get { return this; } }
    }
}
