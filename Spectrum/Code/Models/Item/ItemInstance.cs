﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class ItemInstance : IItem
    {
        ItemModel Base;

        public Vector3 Position { get; set; }

        public ItemInstance(ItemModel baseItem)
        {
            Base = baseItem;
        }


        public string Name { get { return Base.Name; } }
        public string Description { get { return Base.Description; } }
        public IGraphic Graphic { get { return Base.Graphic; } }
        public SizeType Size { get { return Base.Size; } }
        public int Weight { get { return Base.Weight; } }
        public int BashAptitude { get { return Base.BashAptitude; } }
        public int BashDamage { get { return Base.BashDamage; } }
        public List<string> Tags { get { return Base.Tags; } }

        public IGun Gun { get { return Base.Gun; } }
        public IGarment Garment { get { return Base.Garment; } }
    }
}
