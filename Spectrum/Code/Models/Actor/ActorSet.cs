﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IActorSet
    {
        void Add(IActor newActor);
        int Count { get; }
        IActor GetActor(int index);
        IActor GetPlayer { get; }
        bool HasIdleActors { get; }
        void UpdateActorKnowledge(IScene scene);
    }

    public class ActorSet : IActorSet
    {
        private List<IActor> Actors;

        public ActorSet()
        {
            Actors = new List<IActor>();
        }

        public int Count { get { return Actors.Count; } }

        public IActor GetActor(int index)
        { return Actors[index]; }

        public void Add(IActor newActor)
        { Actors.Add(newActor); }


        public IActor GetPlayer
        {
            get
            {
                for (int iter = 0; iter < Count; iter++)
                {
                    IActor actor = GetActor(iter);
                    if (actor.Control == ControlType.Player) return actor;
                }

                return null;
            }
        }

        public bool HasIdleActors
        {
            get
            {
                for (int iter = 0; iter < Count; iter++)
                {
                    IActor actor = GetActor(iter);
                    if (actor.CurrentEvent == null)
                    {
                        return true;
                    }
                }

                return false;      
            }
        }

        public void UpdateActorKnowledge(IScene scene)
        {
            foreach (IActor actor in Actors)
                actor.Knowledge.Update(scene);
        }

    }
}
