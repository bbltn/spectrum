﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public enum StanceType { Prone, Stand, Run }
    public enum ControlType { Player, Bot }

    public interface IActor
    {
        Vector3 Position { get; set; }
        IGraphic Graphic { get; set; }
        IEvent CurrentEvent { get; set; }
        StanceType Stance { get; set; }
        ControlType Control { get; set; }
        IHearing Hearing { get; }
        IVision Vision { get; }
        IName Name { get; set; }
        IHealth Health { get; }
        IMorale Morale { get; }
        ISkills Skill { get; }
        ITraits Traits { get; }
        IInventory Inventory { get; }
        IKnowledge Knowledge { get; }
    }


    public class ActorModel : IActor
    {
        public Vector3 Position { get; set; }
        public IGraphic Graphic { get; set; }
        public IEvent CurrentEvent { get; set; }
        public IHearing Hearing { get; private set; }
        public IVision Vision { get; private set; }
        public StanceType Stance { get; set; }
        public IName Name { get; set; }
        public IHealth Health { get; private set; }
        public IMorale Morale { get; private set; }
        public ISkills Skill { get; private set; }
        public ITraits Traits { get; private set; }
        public IInventory Inventory { get; private set; }
        public ControlType Control { get; set; }
        public IKnowledge Knowledge { get; private set; }

        public ActorModel(Vector3 position)
        {
            Position = position;
            Graphic = new AsciiGraphic(new Character('@'), TermColor.White, TermColor.Black);
            Hearing = new SoundSet();
            Vision = new VisionModel(this);
            Stance = StanceType.Stand;
            Name = new FullName();
            Health = new HealthModel();
            Morale = new MoraleModel();
            Skill = new SkillModel();
            Traits = new TraitSet();
            Inventory = new InventoryModel(6);
            Control = ControlType.Bot;
            Knowledge = new KnowledgeModel(this);
        }
    }
}
