﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IInventory
    {
        IItem Equipped { get; set; }
        IGarment Wearing { get; set; }
        int ItemCount { get; }
        int ItemLimit { get; set; }
        bool CanAddItems { get; }

        void AddItem(IItem item);
        IItem GetItem(int index);
        IItem RemoveItem(int index);
    }

    public class InventoryModel : IInventory
    {
        public IItem Equipped { get; set; }
        public IGarment Wearing { get; set; }

        private List<IItem> Items { get; set; }
        public int ItemCount { get { return Items.Count; } }
        public int ItemLimit { get; set; }
        public bool CanAddItems { get { return Items.Count < ItemLimit; } }

        private const int DefaultItemLimit = 6;

        public InventoryModel()
        {
            Items = new List<IItem>();
            ItemLimit = DefaultItemLimit;
        }

        public InventoryModel(int itemLimit)
        {
            Items = new List<IItem>();
            ItemLimit = itemLimit;
        }

        public void AddItem(IItem item)
        { if (CanAddItems) Items.Add(item); }

        public IItem GetItem(int index)
        {
            if (Contains(index)) return Items[index];
            return null;
        }

        public IItem RemoveItem(int index)
        {
            if (Contains(index))
            {
                var item = Items[index];
                Items.RemoveAt(index);
                return item;
            }
            return null;
        }

        private bool Contains(int index)
        {
            return index < Items.Count && index >= 0;
        }
    }
}
