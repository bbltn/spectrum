﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IMorale
    {
        int Rating { get; set; }
        MoraleStatusType Status { get; set; }
    }

    public enum MoraleStatusType { Unbroken, Broken }

    public class MoraleModel : IMorale
    {
        public int Rating { get; set; }
        public MoraleStatusType Status { get; set; }

        public MoraleModel()
        {
            Rating = 6;
            Status = MoraleStatusType.Unbroken;
        }
    }
}
