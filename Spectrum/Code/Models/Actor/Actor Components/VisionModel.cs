﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IVision
    {
        bool Contains(IScene scene, Vector3 test);
    }
    
    public class VisionModel : IVision
    {
        const float Radius = 17;
        private IActor Parent { get; set; }

        public VisionModel(IActor parent)
        {
            Parent = parent;
        }

        public bool Contains(IScene scene, Vector3 test)
        {
            if (Vector3.Distance(Parent.Position, test) > Radius)
                return false;
            
            // if we can see directly to the tile, we can see it
            Line line = new Line(Parent.Position, test);
            if (LineIsClear(scene, line)) return true;
            
            // if we can see to the tile above the tile, we can also see it
            
            Vector3 modTest = new Vector3(test.X, test.Y, test.Z + 1);
            if (modTest.Z <= Parent.Position.Z)
            {
                Line line2 = new Line(Parent.Position, modTest);
                if (LineIsClear(scene, line2)) return true;
            }

            return false;
        }

        private static bool LineIsClear(IScene scene, Line line)
        {
            if (line.Points.Count <= 2) return true;

            for (int iter = 0; iter < line.Points.Count-1; iter++)
            {
                var tile = scene.Tiles.GetTile(line.Points[iter]);
                if (tile == null) return true;
                if (tile.BlocksVision()) return false;
            }

            return true;
        }
    }
}
