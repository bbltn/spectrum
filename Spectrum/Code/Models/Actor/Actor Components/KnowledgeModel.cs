﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IKnowledge
    {
        IPlan Plan { get; }

        int EnemyCount { get; }
        IActor GetEnemy(int index);
        IActor ClosestEnemy { get; }

        void Update(IScene scene);
    }


    public class KnowledgeModel : IKnowledge
    {
        public IPlan Plan { get; set; }
        private IActor Self { get; set; }

        private List<IActor> Enemies;
        public int EnemyCount { get { return Enemies.Count; } }
        public IActor GetEnemy(int index)
        { return Enemies[index]; }

        public IActor ClosestEnemy
        {
            get
            {
                if (Enemies.Count == 0) return null;

                IActor closest = null;
                foreach (var enemy in Enemies)
                {
                    if (closest == null) closest = enemy;
                    else if (Vector3.Distance(Self.Position, enemy.Position) < Vector3.Distance(Self.Position, closest.Position))
                        closest = enemy;
                }
                return closest;
            }
        }


        public KnowledgeModel(IActor actor)
        {
            Enemies = new List<IActor>();
            Plan = new PlanModel();
            Self = actor;
        }


        public void Update(IScene scene)
        {
            UpdateEnemies(scene);
        }

        private void UpdateEnemies(IScene scene)
        {
            Enemies.Clear();

            // loop through actors and sort them into Enemies if they can be seen
            if (scene.Actors.Count > 0)
                for (int iter = 0; iter < scene.Actors.Count; iter++)
                {
                    IActor actor = scene.Actors.GetActor(iter);
                    if (actor != Self && actor.Health.Status != HealthStatusType.Dead)
                        if (Self.Vision.Contains(scene, actor.Position))
                            Enemies.Add(actor);
                }
        }
    }
}
