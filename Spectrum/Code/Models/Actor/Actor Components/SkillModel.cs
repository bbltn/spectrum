﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ISkills
    {
        int Leadership { get; set; }
        int Unarmed { get; set; }
        int Gun { get; set; }
        int Medic { get; set; }
    }

    public class SkillModel : ISkills
    {
        public int Leadership { get; set; }
        public int Unarmed { get; set; }
        public int Gun { get; set; }
        public int Medic { get; set; }

        public SkillModel()
        {
            Leadership = 0;
            Unarmed = 0;
            Gun = 0;
            Medic = 0;
        }
    }
}
