﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IHealth
    {
        HealthStatusType Status { get; set; }
        ConsciousStatusType Consciousness { get; set; }
    }

    public enum HealthStatusType { Good, LightWounds, HeavyWounds, Dead }
    public enum ConsciousStatusType { Awake, Unconscious }

    public class HealthModel : IHealth
    {
        public HealthStatusType Status { get; set; }
        public ConsciousStatusType Consciousness { get; set; }
    }
}
