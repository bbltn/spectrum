﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface ITime
    {
        int Absolute { get; }
        int Hours { get; }
        int Minutes { get; }
        int Seconds { get; }
        int QuarterSeconds { get; }

        void Increment();
        void SetAbsolute(int time);
    }

    public class TimeModel : ITime
    {
        public int Absolute { get; private set; }

        private const int QUARTER_SECONDS = 1;
        private const int SECONDS = 4;
        private const int MINUTES = 4 * 60;
        private const int HOURS = 4 * 60 * 60;

        public TimeModel()
        {
            Absolute = 0;
        }

        public TimeModel(int absolute)
        {
            Absolute = absolute;
        }

        public void Increment()
        {
            Absolute++;
        }

        public void SetAbsolute(int time)
        {
            Absolute = time;
        }


        public int Hours 
        {
            get
            {
                int total = Absolute;
                int count = 0;

                while (total > HOURS)
                {
                    total -= HOURS;
                    count++;
                }

                return count;
            }
        }

        public int Minutes 
        {            
            get
            {
                int total = Absolute;
                int count = 0;

                while (total > HOURS)
                {
                    total -= HOURS;
                }

                while (total > MINUTES)
                {
                    total -= MINUTES;
                    count++;
                }

                return count;
            }
        }

        public int Seconds
        {
            get
            {
                int total = Absolute;
                int count = 0;

                while (total > HOURS)
                {
                    total -= HOURS;
                }

                while (total > MINUTES)
                {
                    total -= MINUTES;
                }

                while (total > SECONDS)
                {
                    total -= SECONDS;
                    count++;
                }

                return count;
            }
        }

        public int QuarterSeconds
        {
            get
            {
                int total = Absolute;
                int count = 0;

                while (total > HOURS)
                {
                    total -= HOURS;
                }

                while (total > MINUTES)
                {
                    total -= MINUTES;
                }

                while (total > SECONDS)
                {
                    total -= SECONDS;
                }

                while (total > QUARTER_SECONDS)
                {
                    total -= QUARTER_SECONDS;
                    count++;
                }

                return count;
            }
        }     
        
    }
}
