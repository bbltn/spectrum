﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class SceneGenerator
    {
        private static int SizeX = 60;
        private static int SizeY = 40;
        private static int SizeZ = 3;

        public static void Generate(IScene scene, IResources resources)
        {
            if (scene == null || resources == null) return;

            scene.Initialize(new Vector3(SizeX, SizeY, SizeZ));

            scene.Time.SetAbsolute(resources.RNG.Next(9000,300000));

            CreateFlatGround(scene, resources);
            
            CreateTreeline(scene, resources);
            CreateBuildings(scene, resources);

            CreatePlayer(scene, resources);
            for (int n = 0; n < 20; n++)
                CreateBot(scene, resources);

            CreateObjectives(scene);
        }

        private static void CreateBuildings(IScene Scene, IResources Resources)
        {
            IBuildingGenerator buildingGenerator = new BuildingGenerator();

            int numberOfBuildings = Resources.RNG.Next(2,6);

            for (int iter = 0; iter < numberOfBuildings; iter++)
            {
                int buildingX = Resources.RNG.Next(5, 12);
                int buildingY = Resources.RNG.Next(5, 12);

                int[,] building = buildingGenerator.CreateBuilding(buildingX, buildingY, Resources.RNG);

                int originX = Resources.RNG.Next(2, SizeX - buildingX - 2);
                int originY = Resources.RNG.Next(2, SizeY - buildingY - 2);

                // flatten surrounded landscape
                for (int x = originX - 2; x < originX + buildingX + 2; x++)
                    for (int y = originY - 2; y < originY + buildingY + 2; y++)
                    {
                        ITile tile = Scene.Tiles.GetTile(new Vector3(x, y, 1));

                        if (tile != null) 
                            if (tile.Feature != null)
                                if (tile.Feature.Name == "Tree")
                                    tile.Feature = null;
                    }


                // create building features
                for (int x = 0; x < buildingX; x++)
                    for (int y = 0; y < buildingY; y++)
                    {
                        IFeature feature = null;

                        if (building[x, y] == 0) feature = null;
                        if (building[x,y] == 1) feature = Resources.CreateFeatureInstance("Wall");
                        if (building[x, y] == 2) feature = Resources.CreateFeatureInstance("Door");
                        if (building[x, y] == 3) feature = Resources.CreateFeatureInstance("Window");

                        ITile tile = Scene.Tiles.GetTile(new Vector3(originX + x, originY + y, 1));
                        if (feature != null)
                            tile.Feature = feature;
                    
                        ITile roof = Scene.Tiles.GetTile(new Vector3(originX + x, originY + y, 0));
                        roof.Feature = Resources.CreateFeatureInstance("Floor");

                        ITile floor = Scene.Tiles.GetTile(new Vector3(originX + x, originY + y, 2));
                        floor.Feature = Resources.CreateFeatureInstance("Wall");
                    }
            }

        }

        private static void CreateTreeline(IScene Scene, IResources Resources)
        {
            csCaveGenerator CaveGenerator = new csCaveGenerator(SizeX, SizeY, Resources.RNG.Next(0,5000));

            bool hasClearings = false;

            int attempts = 0;

            while (!hasClearings && attempts < 50)
            {
                CaveGenerator.Build();
                CaveGenerator.ConnectCaves();


                for (int x = 0; x < SizeX; x++)
                    for (int y = 0; y < SizeY; y++)
                        if (CaveGenerator.Map[x, y] == 1)
                            hasClearings = true;

                attempts++;
            }

            for (int x = 0; x < SizeX; x++)
                for (int y = 0; y < SizeY; y++)
                    if (CaveGenerator.Map[x, y] == 0)
                        //if (Resources.RNG.Next(0,100) < 80)
                            Scene.Tiles.GetTile(new Vector3(x, y, 1)).Feature = Resources.CreateFeatureInstance("Tree");
        }


        private static void CreateObjectives(IScene Scene)
        {
            //ICondition testObjective = new Objective_ActorAtLocation(Scene.Actors.GetPlayer, new Vector3(9, 9, 1));
            ICondition testObjective = new Objective_KillAllBots(Scene);
            Scene.Objectives.Add(testObjective);
        }

        private static void CreatePlayer(IScene Scene, IResources Resources)
        {
            Vector3 position = new Vector3(Resources.RNG.Next(0, SizeX - 1), Resources.RNG.Next(0, SizeY - 1), 1);
            var targetTile = Scene.Tiles.GetTile(position);

            while (targetTile.Feature != null || targetTile.Actor != null)
            {
                position = new Vector3(Resources.RNG.Next(0, SizeX - 1), Resources.RNG.Next(0, SizeY - 1), 1);
                targetTile = Scene.Tiles.GetTile(position);
            }

            var actor = new ActorModel(position);

            actor.Skill.Unarmed = 1;

            actor.Name = Resources.CreateName();

            actor.Traits.Add(Resources.CreateTraitInstance("Marksman"));

            // place actor
            targetTile.Actor = actor;
            Scene.Actors.Add(actor);

            // attach camera
            //Scene.Camera.Perspective = Scene.Player;
            Scene.Camera.Perspective = actor;
            Scene.Camera.Position = actor.Position;

            IItem gun = Resources.CreateRandomGunInstance().Item;
            actor.Inventory.Equipped = gun;

            IGarment garment = Resources.CreateRandomGarmentInstance();
            actor.Inventory.Wearing = garment;

            for (int i = 0; i < 3; i++)
            {
                IItem disk = Resources.CreateItemInstance("Medkit");
                actor.Inventory.AddItem(disk);
            }

            // assign control
            actor.Control = ControlType.Player;
        }

        private static void CreateBot(IScene Scene, IResources Resources)
        {
            Random RNG = Resources.RNG;
            
            Vector3 position = new Vector3(RNG.Next(0, SizeX - 1), RNG.Next(0, SizeY - 1), 1);
            var targetTile = Scene.Tiles.GetTile(position);

            while (targetTile.Feature != null || targetTile.Actor != null)
            {
                position = new Vector3(RNG.Next(0, SizeX - 1), RNG.Next(0, SizeY - 1), 1);
                targetTile = Scene.Tiles.GetTile(position);
            }

            var actor = new ActorModel(position);

            actor.Name = Resources.CreateName();


            // random chance to give a gun
            bool armed = RNG.Next(0, 100) < 50;
            if (armed)
            {
                // generate gun
                int gunValue = RNG.Next(0, 100);
                IItem gun = null;
                if (gunValue < 50)
                    gun = Resources.CreateRandomGunInstance("SMG").Item;
                else if (gunValue < 80)
                    gun = Resources.CreateRandomGunInstance("Handgun").Item;
                else
                    gun = Resources.CreateRandomGunInstance("Rifle").Item;                
                actor.Inventory.Equipped = gun;
                

                actor.Graphic = new AsciiGraphic(new Malison.Core.Character('G'), Malison.Core.TermColor.White, Malison.Core.TermColor.Black);
            }

            else
            {
                actor.Graphic = new AsciiGraphic(new Malison.Core.Character('S'), Malison.Core.TermColor.White, Malison.Core.TermColor.Black);
            }

            // place actor
            targetTile.Actor = actor;
            Scene.Actors.Add(actor);

            // assign control
            actor.Control = ControlType.Bot;
        }

        private static void CreateFlatGround(IScene Scene, IResources Resources)
        {
            for (int x = 0; x < Scene.Tiles.Size.X; x++)
            {
                for (int y = 0; y < Scene.Tiles.Size.Y; y++)
                {
                    var tile = Scene.Tiles.GetTile(new Vector3(x, y, 0));
                    tile.Feature = Resources.CreateFeatureInstance("Dirt");
                }
            }
        }
    }
}
