﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public interface IScene
    {
        ITileSet Tiles { get; }
        IActorSet Actors { get; }
        ICamera Camera { get; }
        ITime Time { get; }
        IEventProcessor Events { get; }
        IMessageLog Messages { get; }
        IObjectiveSet Objectives { get; }
        IResources Resources { get; }


        void Update(BaseCommand command);
        void Initialize(Vector3 size);
    }

    public class SceneModel : IScene
    {
        public ITileSet Tiles { get; private set; }
        public ICamera Camera { get; private set; }
        public ITime Time { get; private set; }
        public IActorSet Actors { get; private set; }
        public IEventProcessor Events { get; private set; }
        public IMessageLog Messages { get; private set; }
        public IObjectiveSet Objectives { get; private set; }
        public IResources Resources { get; private set; }

        bool HasPrintedVictoryMessage;

        public SceneModel(Vector3 size, IResources resources)
        {
            Resources = resources;
            Initialize(size);
        }

        public void Initialize(Vector3 size)
        {
            Tiles = new TileSet(size);
            Actors = new ActorSet();
            Camera = new CameraModel(new Vector3(1, 1, 0), this);
            Time = new TimeModel();
            Events = new EventProcessor();
            Messages = new MessageLog(20);
            Objectives = new ObjectiveSet();

            HasPrintedVictoryMessage = false;
        }


        public void Update(BaseCommand command)
        {
            MoveCamera();

            // if the player has won, stop updating
            if (Objectives.IsComplete)
            {
                PrintVictoryMessage();
                return;
            }

            // do any events scheduled for the current scene time
            Events.ProcessEvents(command);

            // stop this update if any events are still processing
            if (Events.CurrentlyProcessing()) return;

            // update all actors' knowledge about the world
            Actors.UpdateActorKnowledge(this);

            // throw new events
            GetEventsForIdleActors(command);

            // stop this update if some actors still need to take action
            if (Actors.HasIdleActors) return;            
            
            // advance scene time
            if (!Events.CurrentlyProcessing()) Time.Increment();

            return;
        }

        private void PrintVictoryMessage()
        {
            if (!HasPrintedVictoryMessage)
            {
                CharacterString winString = new CharacterString("You win! Press ESCAPE to return.", TermColor.LightGold, TermColor.DarkGray);
                Messages.Add(winString);
                HasPrintedVictoryMessage = true;
            }
        }

        private void MoveCamera()
        {
            if (Actors.GetPlayer != null) Camera.Follow(Actors.GetPlayer.Position);
        }

        private void GetEventsForIdleActors(BaseCommand Command)
        {
            for (int iter = 0; iter < Actors.Count; iter++)
            {
                IActor actor = Actors.GetActor(iter);
                
                if (actor.CurrentEvent == null)
                {
                    if (actor.Control == ControlType.Player)
                        PlayerControl.Update(this, actor, Command);

                    else if (actor.Control == ControlType.Bot) 
                        BotControl.Update(this, actor);
                }
            }
        }
       
    }
}
