﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IBuildingGenerator
    {
        int[,] CreateBuilding(int SizeX, int SizeY, Random RNG);
    }

    public class BuildingGenerator : IBuildingGenerator
    {
        private const int Space = 0;
        private const int Wall = 1;
        private const int Door = 2;
        private const int Window = 3;

        private const int MinExteriorDoors = 2;
        private const int MaxExteriorDoors = 4;

        private const int MinExteriorWindows = 4;
        private const int MaxExteriorWindows = 8;

        public BuildingGenerator()
        {
        }

        public int[,] CreateBuilding(int SizeX, int SizeY, Random RNG)
        {
            int[,] Building = new int[SizeX, SizeY];

            InitializeBuildingArray(SizeX, SizeY, Building);
            
            PlaceExteriorWalls(SizeX, SizeY, Building);
            
            PlaceExteriorDoors(SizeX, SizeY, RNG, Building);

            PlaceExteriorWindows(SizeX, SizeY, RNG, Building);
            
            return Building;
        }

        private static void InitializeBuildingArray(int SizeX, int SizeY, int[,] Building)
        {
            for (int iterX = 0; iterX < SizeX; iterX++)
                for (int iterY = 0; iterY < SizeY; iterY++)
                    Building[iterX, iterY] = Space;
        }

        private static void PlaceExteriorWalls(int SizeX, int SizeY, int[,] Building)
        {
            for (int iterX = 0; iterX < SizeX; iterX++)
            {
                Building[iterX, 0] = Wall;
                Building[iterX, SizeY - 1] = Wall;
            }
            for (int iterY = 0; iterY < SizeY - 1; iterY++)
            {
                Building[0, iterY] = Wall;
                Building[SizeX - 1, iterY] = Wall;
            }
        }

        private static void PlaceExteriorWindows(int SizeX, int SizeY, Random RNG, int[,] Building)
        {
            int windowCount = RNG.Next(MinExteriorWindows, MaxExteriorWindows);
            for (int iter = 0; iter < windowCount; iter++)
            {
                int x = 0;
                int y = 0;

                int side = RNG.Next(1, 4);
                if (side == 1)
                {
                    x = RNG.Next(1, SizeX - 1);
                    y = 0;
                }
                if (side == 2)
                {
                    x = RNG.Next(1, SizeX - 1);
                    y = SizeY - 1;
                }
                if (side == 3)
                {
                    x = 0;
                    y = RNG.Next(1, SizeY - 1);
                }
                if (side == 4)
                {
                    x = SizeX - 1;
                    y = RNG.Next(1, SizeY - 1);
                }

                if (Building[x, y] == Door || Building[x, y] == Window)
                    iter--;
                else
                    Building[x, y] = Window;
            }
        }

        private static void PlaceExteriorDoors(int SizeX, int SizeY, Random RNG, int[,] Building)
        {
            int doorCount = RNG.Next(MinExteriorDoors, MaxExteriorDoors);
            for (int iter = 0; iter < doorCount; iter++)
            {
                int x = 0;
                int y = 0;

                int side = RNG.Next(1, 4);
                if (side == 1)
                {
                    x = RNG.Next(1, SizeX - 1);
                    y = 0;
                }
                if (side == 2)
                {
                    x = RNG.Next(1, SizeX - 1);
                    y = SizeY - 1;
                }
                if (side == 3)
                {
                    x = 0;
                    y = RNG.Next(1, SizeY - 1);
                }
                if (side == 4)
                {
                    x = SizeX - 1;
                    y = RNG.Next(1, SizeY - 1);
                }

                if (Building[x, y] == Door)
                    iter--;
                else
                    Building[x, y] = Door;
            }
        }
    }
}
