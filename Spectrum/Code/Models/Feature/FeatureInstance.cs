﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public class FeatureInstance : IFeature
    {
        private IFeature Base { get; set; }

        public string Name { get { return Base.Name; } }
        public string Description { get { return Base.Description; } }
        public IGraphic Graphic { get { return Base.Graphic; } }
        public bool BlocksVision { get { return Base.BlocksVision; } }
        public bool BlocksSound { get { return Base.BlocksSound; } }
        public bool BlocksSpace { get { return Base.BlocksSpace; } }

        public FeatureInstance(IFeature baseFeature)
        {
            Base = baseFeature;
        }
    }
}
