﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public class FeatureModel : IFeature
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IGraphic Graphic { get; set; }
        public bool BlocksVision { get; set; }
        public bool BlocksSound { get; set; }
        public bool BlocksSpace { get; set; }

        public FeatureModel()
        {
            Name = "FeatureModel";
            Description = "Description.";
            Graphic = new AsciiGraphic();
            BlocksVision = false;
            BlocksSound = false;
            BlocksSpace = false;
        }

        public FeatureModel ( string name, string description, bool blocksVision, bool blocksSound, bool blocksSpace )
        {
            Name = name;
            Description = description;
            Graphic = new AsciiGraphic();
            BlocksVision = blocksVision;
            BlocksSound = blocksSound;
            BlocksSpace = blocksSpace;
        }
    }
}
