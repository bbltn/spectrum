﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public interface IFeature
    {
        string Name { get; }
        string Description { get; }
        IGraphic Graphic { get; }
        bool BlocksVision { get; }
        bool BlocksSound { get; }
        bool BlocksSpace { get; }
    }
}
