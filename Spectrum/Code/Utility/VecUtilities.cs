﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;

namespace Spectrum
{

    public static class VecUtilities
    {
        public static float Distance(Vec first, Vec second)
        {
            var firstSet = Math.Pow(second.X - first.X, 2);
            var secondSet = Math.Pow(second.Y - first.Y, 2);
            return (float)Math.Sqrt(firstSet + secondSet);
        }

        public static bool Adjacenct(Vec first, Vec second)
        {
            var newVec = first - second;

            if (Math.Abs(newVec.X) <= 1 && Math.Abs(newVec.Y) <= 1)
                return true;

            return false;
        }

        public static double Magnitude(Vec value)
        {
            return Math.Sqrt(Math.Pow(value.X,2) + Math.Pow(value.Y,2));
        }

        public static Vec Normalize(Vec value)
        {
            Vec returnVal = value;

            if (Math.Abs(returnVal.X) > 1)
                returnVal = returnVal / Math.Abs(returnVal.X);
            if (Math.Abs(returnVal.Y) > 1)
                returnVal = returnVal / Math.Abs(returnVal.Y);

            return returnVal;
        }

        public static bool IsNormalized(Vec value)
        {
            return (Math.Abs(value.X) <= 1 && Math.Abs(value.Y) <= 1);
        }
    }
}
