﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public static class ColorUtilities
    {
        public static TermColor Tint(TermColor color)
        {
            if (color == TermColor.LightGray)
                return TermColor.White;
            if (color == TermColor.Gray)
                return TermColor.LightGray;
            if (color == TermColor.DarkGray)
                return TermColor.Gray;
            if (color == TermColor.Black)
                return TermColor.DarkGray;

            if (color == TermColor.Red)
                return TermColor.LightRed;
            if (color == TermColor.DarkRed)
                return TermColor.Red;

            if (color == TermColor.Orange)
                return TermColor.LightOrange;
            if (color == TermColor.DarkOrange)
                return TermColor.Orange;

            if (color == TermColor.Gold)
                return TermColor.LightGold;
            if (color == TermColor.DarkGold)
                return TermColor.Gold;

            if (color == TermColor.Yellow)
                return TermColor.LightYellow;
            if (color == TermColor.DarkYellow)
                return TermColor.Yellow;
            
            if (color == TermColor.Green)
                return TermColor.LightGreen;
            if (color == TermColor.DarkGreen)
                return TermColor.Green;

            if (color == TermColor.Cyan)
                return TermColor.LightCyan;
            if (color == TermColor.DarkCyan)
                return TermColor.Cyan;

            if (color == TermColor.Blue)
                return TermColor.LightBlue;
            if (color == TermColor.DarkBlue)
                return TermColor.Blue;

            if (color == TermColor.Purple)
                return TermColor.LightPurple;
            if (color == TermColor.DarkPurple)
                return TermColor.Purple;

            if (color == TermColor.Brown)
                return TermColor.LightBrown;
            if (color == TermColor.DarkBrown)
                return TermColor.Brown;

            if (color == TermColor.Pink)
                return TermColor.Flesh;

            return TermColor.LightGray;
        }

        public static TermColor Desaturate(TermColor color)
        {

            if (color == TermColor.White)
                return TermColor.White;
            else if (color == TermColor.LightGray)
                return TermColor.LightGray;
            else if (color == TermColor.Gray)
                return TermColor.Gray;
            else if (color == TermColor.DarkGray)
                return TermColor.DarkGray;

            else if (color == TermColor.LightRed)
                return TermColor.LightGray;
            else if (color == TermColor.Red)
                return TermColor.Gray;
            else if (color == TermColor.DarkRed)
                return TermColor.DarkGray;

            else if (color == TermColor.LightOrange)
                return TermColor.LightGray;
            else if (color == TermColor.Orange)
                return TermColor.Gray;
            else if (color == TermColor.DarkOrange)
                return TermColor.DarkGray;

            else if (color == TermColor.LightGold)
                return TermColor.LightGray;
            else if (color == TermColor.Gold)
                return TermColor.Gray;
            else if (color == TermColor.DarkGold)
                return TermColor.DarkGray;

            else if (color == TermColor.LightYellow)
                return TermColor.LightGray;
            else if (color == TermColor.Yellow)
                return TermColor.Gray;
            else if (color == TermColor.DarkYellow)
                return TermColor.DarkGray;

            else if (color == TermColor.LightGreen)
                return TermColor.LightGray;
            else if (color == TermColor.Green)
                return TermColor.Gray;
            else if (color == TermColor.DarkGreen)
                return TermColor.DarkGray;

            else if (color == TermColor.LightCyan)
                return TermColor.LightGray;
            else if (color == TermColor.Cyan)
                return TermColor.Gray;
            else if (color == TermColor.DarkCyan)
                return TermColor.DarkGray;

            else if (color == TermColor.LightBlue)
                return TermColor.LightGray;
            else if (color == TermColor.Blue)
                return TermColor.Gray;
            else if (color == TermColor.DarkBlue)
                return TermColor.DarkGray;

            else if (color == TermColor.LightPurple)
                return TermColor.LightGray;
            else if (color == TermColor.Purple)
                return TermColor.Gray;
            else if (color == TermColor.DarkPurple)
                return TermColor.DarkGray;

            else if (color == TermColor.LightBrown)
                return TermColor.LightGray;
            else if (color == TermColor.Brown)
                return TermColor.Gray;
            else if (color == TermColor.DarkBrown)
                return TermColor.DarkGray;

            else if (color == TermColor.Flesh)
                return TermColor.LightGray;
            else if (color == TermColor.Pink)
                return TermColor.Gray;

            return TermColor.Black;
        }

        public static TermColor Dim(TermColor color)
        {

            if (color == TermColor.White)
                return TermColor.LightGray;
            else if (color == TermColor.LightGray)
                return TermColor.Gray;
            else if (color == TermColor.Gray)
                return TermColor.DarkGray;
            else if (color == TermColor.DarkGray)
                return TermColor.Black;

            else if (color == TermColor.LightRed)
                return TermColor.Red;
            else if (color == TermColor.Red)
                return TermColor.DarkRed;
            else if (color == TermColor.DarkRed)
                return TermColor.DarkGray;

            else if (color == TermColor.LightOrange)
                return TermColor.Orange;
            else if (color == TermColor.Orange)
                return TermColor.DarkOrange;
            else if (color == TermColor.DarkOrange)
                return TermColor.DarkGray;

            else if (color == TermColor.LightGold)
                return TermColor.Gold;
            else if (color == TermColor.Gold)
                return TermColor.DarkGold;
            else if (color == TermColor.DarkGold)
                return TermColor.DarkGray;

            else if (color == TermColor.LightYellow)
                return TermColor.Yellow;
            else if (color == TermColor.Yellow)
                return TermColor.DarkYellow;
            else if (color == TermColor.DarkYellow)
                return TermColor.DarkGray;

            else if (color == TermColor.LightGreen)
                return TermColor.Green;
            else if (color == TermColor.Green)
                return TermColor.DarkGreen;
            else if (color == TermColor.DarkGreen)
                return TermColor.DarkGray;

            else if (color == TermColor.LightCyan)
                return TermColor.Cyan;
            else if (color == TermColor.Cyan)
                return TermColor.DarkCyan;
            else if (color == TermColor.DarkCyan)
                return TermColor.DarkGray;

            else if (color == TermColor.LightBlue)
                return TermColor.Blue;
            else if (color == TermColor.Blue)
                return TermColor.DarkBlue;
            else if (color == TermColor.DarkBlue)
                return TermColor.DarkGray;

            else if (color == TermColor.LightPurple)
                return TermColor.Purple;
            else if (color == TermColor.Purple)
                return TermColor.DarkPurple;
            else if (color == TermColor.DarkPurple)
                return TermColor.DarkGray;

            else if (color == TermColor.LightBrown)
                return TermColor.Brown;
            else if (color == TermColor.Brown)
                return TermColor.DarkBrown;
            else if (color == TermColor.DarkBrown)
                return TermColor.DarkGray;

            else if (color == TermColor.Flesh)
                return TermColor.Pink;
            else if (color == TermColor.Pink)
                return TermColor.Red;

            return TermColor.Black;
        }


        public static TermColor GetDarkVersion(TermColor color)
        {

            if (color == TermColor.White)
                return TermColor.Black;

            else if (color == TermColor.LightGray)
                return TermColor.DarkGray;
            else if (color == TermColor.Gray)
                return TermColor.DarkGray;
            else if (color == TermColor.DarkGray)
                return TermColor.DarkGray;

            else if (color == TermColor.LightRed)
                return TermColor.DarkRed;
            else if (color == TermColor.Red)
                return TermColor.DarkRed;
            else if (color == TermColor.DarkRed)
                return TermColor.DarkRed;

            else if (color == TermColor.LightOrange)
                return TermColor.DarkOrange;
            else if (color == TermColor.Orange)
                return TermColor.DarkOrange;
            else if (color == TermColor.DarkOrange)
                return TermColor.DarkOrange;

            else if (color == TermColor.LightGold)
                return TermColor.DarkGold;
            else if (color == TermColor.Gold)
                return TermColor.DarkGold;
            else if (color == TermColor.DarkGold)
                return TermColor.DarkGold;

            else if (color == TermColor.LightYellow)
                return TermColor.DarkYellow;
            else if (color == TermColor.Yellow)
                return TermColor.DarkYellow;
            else if (color == TermColor.DarkYellow)
                return TermColor.DarkYellow;

            else if (color == TermColor.LightGreen)
                return TermColor.DarkGreen;
            else if (color == TermColor.Green)
                return TermColor.DarkGreen;
            else if (color == TermColor.DarkGreen)
                return TermColor.DarkGreen;

            else if (color == TermColor.LightCyan)
                return TermColor.DarkCyan;
            else if (color == TermColor.Cyan)
                return TermColor.DarkCyan;
            else if (color == TermColor.DarkCyan)
                return TermColor.DarkCyan;

            else if (color == TermColor.LightBlue)
                return TermColor.DarkBlue;
            else if (color == TermColor.Blue)
                return TermColor.DarkBlue;
            else if (color == TermColor.DarkBlue)
                return TermColor.DarkBlue;

            else if (color == TermColor.LightPurple)
                return TermColor.DarkPurple;
            else if (color == TermColor.Purple)
                return TermColor.DarkPurple;
            else if (color == TermColor.DarkPurple)
                return TermColor.DarkPurple;

            else if (color == TermColor.LightBrown)
                return TermColor.DarkBrown;
            else if (color == TermColor.Brown)
                return TermColor.DarkBrown;
            else if (color == TermColor.DarkBrown)
                return TermColor.DarkBrown;

            else if (color == TermColor.Flesh)
                return TermColor.DarkRed;
            else if (color == TermColor.Pink)
                return TermColor.DarkRed;

            return TermColor.Black;
        }

        public static TermColor GetLightVersion(TermColor color)
        {

            if (color == TermColor.Black)
                return TermColor.White;

            else if (color == TermColor.LightGray)
                return TermColor.LightGray;
            else if (color == TermColor.Gray)
                return TermColor.LightGray;
            else if (color == TermColor.DarkGray)
                return TermColor.LightGray;

            else if (color == TermColor.LightRed)
                return TermColor.LightRed;
            else if (color == TermColor.Red)
                return TermColor.LightRed;
            else if (color == TermColor.DarkRed)
                return TermColor.LightRed;

            else if (color == TermColor.LightOrange)
                return TermColor.LightOrange;
            else if (color == TermColor.Orange)
                return TermColor.LightOrange;
            else if (color == TermColor.DarkOrange)
                return TermColor.LightOrange;

            else if (color == TermColor.LightGold)
                return TermColor.LightGold;
            else if (color == TermColor.Gold)
                return TermColor.LightGold;
            else if (color == TermColor.DarkGold)
                return TermColor.LightGold;

            else if (color == TermColor.LightYellow)
                return TermColor.LightYellow;
            else if (color == TermColor.Yellow)
                return TermColor.LightYellow;
            else if (color == TermColor.DarkYellow)
                return TermColor.LightYellow;

            else if (color == TermColor.LightGreen)
                return TermColor.LightGreen;
            else if (color == TermColor.Green)
                return TermColor.LightGreen;
            else if (color == TermColor.DarkGreen)
                return TermColor.LightGreen;

            else if (color == TermColor.LightCyan)
                return TermColor.LightCyan;
            else if (color == TermColor.Cyan)
                return TermColor.LightCyan;
            else if (color == TermColor.DarkCyan)
                return TermColor.LightCyan;

            else if (color == TermColor.LightBlue)
                return TermColor.LightBlue;
            else if (color == TermColor.Blue)
                return TermColor.LightBlue;
            else if (color == TermColor.DarkBlue)
                return TermColor.LightBlue;

            else if (color == TermColor.LightPurple)
                return TermColor.LightPurple;
            else if (color == TermColor.Purple)
                return TermColor.LightPurple;
            else if (color == TermColor.DarkPurple)
                return TermColor.LightPurple;

            else if (color == TermColor.LightBrown)
                return TermColor.LightBrown;
            else if (color == TermColor.Brown)
                return TermColor.LightBrown;
            else if (color == TermColor.DarkBrown)
                return TermColor.LightBrown;

            else if (color == TermColor.Flesh)
                return TermColor.Flesh;
            else if (color == TermColor.Pink)
                return TermColor.Flesh;

            return TermColor.White;
        }

    }
}
