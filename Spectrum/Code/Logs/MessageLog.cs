﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public interface IMessageLog
    {
        int Max { get; }
        int Count { get; }
        CharacterString GetMessage(int index);
        void Add(CharacterString newMessage);
        void Add(string newMessage);
        void Add(string newMessage, TermColor foreColor);
        void Add(string newMessage, TermColor foreColor, TermColor backColor);
        void Clear();
        void Resize(int size);
    }

    public class MessageLog : IMessageLog
    {
        private List<CharacterString> Messages;
        public int Max { get; private set; }
        public int Count { get { return Messages.Count; } }
        private const TermColor FORE_COLOR = TermColor.LightGray;
        private const TermColor BACK_COLOR = TermColor.Black;

        public MessageLog(int max)
        {
            Messages = new List<CharacterString>();
            Max = max;
        }

        public CharacterString GetMessage(int index)
        {
            if (index < 0) 
                return new CharacterString("");

            if (index >= Messages.Count)
                return new CharacterString("");

            return Messages[index];
        }

        public void Add(CharacterString newMessage)
        {
            Messages.Add(newMessage);
            PruneMessages();
        }

        public void Add(string newMessage)
        {
            Messages.Add(new CharacterString(newMessage, FORE_COLOR, BACK_COLOR));
            PruneMessages();
        }

        public void Add(string newMessage, TermColor foreColor)
        {
            Messages.Add(new CharacterString(newMessage, foreColor, BACK_COLOR));
            PruneMessages();
        }

        public void Add(string newMessage, TermColor foreColor, TermColor backColor)
        {
            Messages.Add(new CharacterString(newMessage, foreColor, backColor));
            PruneMessages();
        }

        public void Clear()
        {
            Messages = new List<CharacterString>();
        }

        public void Resize(int size)
        {
            Max = size;
            PruneMessages();
        }

        private void PruneMessages()
        {
            if (Max <= 0) return;

            while (Messages.Count > Max)
                Messages.RemoveAt(0);
        }
    }
}
