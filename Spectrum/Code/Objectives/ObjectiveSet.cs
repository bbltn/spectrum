﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IObjectiveSet
    {
        bool IsComplete { get; }
        int Count { get; }
        ICondition GetObjective(int index);
        void Add(ICondition objective);
    }

    class ObjectiveSet : IObjectiveSet
    {
        private List<ICondition> Objectives;

        public ObjectiveSet()
        {
            Objectives = new List<ICondition>();
        }

        public bool IsComplete
        {
            get
            {
                foreach (var objective in Objectives) 
                    if (objective.Evaluate) 
                        return true;
                return false;
            }
        }

        public int Count { get { return Objectives.Count; } }

        public ICondition GetObjective(int index)
        {
            if (index < 0 || index >= Objectives.Count)
                return null;

            return Objectives[index];
        }

        public void Add(ICondition objective)
        {
            Objectives.Add(objective);
        }
    }
}
