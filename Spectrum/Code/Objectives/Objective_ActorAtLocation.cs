﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    class Objective_ActorAtLocation : ICondition
    {
        private IActor Actor { get; set; }
        private Vector3 TargetPosition { get; set; }
        public ConditionResult Result { get; private set; }

        public Objective_ActorAtLocation(IActor actor, Vector3 targetPosition, ConditionResult result = ConditionResult.Win)
        {
            Actor = actor;
            TargetPosition = targetPosition;
            Result = result;
        }

        public bool Evaluate 
        {
            get
            {
                if (Actor.Position.Equals(TargetPosition)) return true;
                return false;
            }
        }
        
    }
}
