﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    class Objective_KillAllBots : ICondition
    {
        IScene Scene;
        public ConditionResult Result { get; private set; }

        public Objective_KillAllBots(IScene scene)
        {
            Scene = scene;

            Result = ConditionResult.Win;
        }

        public bool Evaluate
        {
            get
            {
                bool noBotsRemaining = true;

                for (int i = 0; i < Scene.Actors.Count; i++)
                {
                    var actor = Scene.Actors.GetActor(i);
                    if (actor.Control == ControlType.Bot && actor.Health.Status != HealthStatusType.Dead)
                    {
                        noBotsRemaining = false;
                        break;
                    }
                }

                return noBotsRemaining;
            }
        }
    }
}
