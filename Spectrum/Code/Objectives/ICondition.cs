﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public enum ConditionResult { Win, Lose }

    public interface ICondition
    {
        bool Evaluate { get; }
        ConditionResult Result { get; }
    }
}
