﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IPath
    {
        int Count { get; }
        Vector3 GetPoint(int index);
        int Cost { get; }
        Vector3 GetFinal { get; }
        Vector3 GetNext(Vector3 current);
    }

    public class Path : IPath
    {
        private static int MAX_ATTEMPTS = 100;

        private List<Vector3> Points;

        private Vector3 Start { get; set; }
        
        public int Count { get { return Points.Count; } }
        public Vector3 GetPoint(int index) { return Points[index]; }
        public int Cost { get; private set; }

        public Vector3 GetFinal
        {
            get
            {
                if (Count == 0)
                    return Start;

                return Points[Points.Count - 1];
            }
        }

        public Vector3 GetNext(Vector3 current)
        {
            if (Count == 0)
                return current;

            if (Count == 1)
                return Points[0];
            
            for (int iter = 0; iter < Points.Count - 1; iter++)
            {
                if (current == Points[iter]) return Points[iter+1];
            }

            return Points[0];
            //return Points[Points.Count-1];
        }


        public Path(INode origin, INode end, NodeCostEvaluationMethod evaluationMethod)
        {
            Points = new List<Vector3>();
            Cost = 0;

            Start = NodeValues.Position(origin);

            FindPath(origin, end, evaluationMethod, AddPoint, PlotCost);

        }

        private void AddPoint(INode node)
        {
            Points.Add(NodeValues.Position(node));
        }
        private void PlotCost(int cost)
        {
            Cost = cost;
        }


        public static void FindPath(INode origin, INode end, NodeCostEvaluationMethod evaluationMethod, Action<INode> plotMethod, Action<int> plotCost)
        {
            // validate that inputs are not null
            if (origin == null || end == null || evaluationMethod == null || plotMethod == null || plotCost == null) return;


            // List<INode> returnSet = new List<INode>();
            List<PathNode> closedSet = new List<PathNode>();
            List<PathNode> openSet = new List<PathNode>();

            // add the origin point
            closedSet.Add(new PathNode(origin,null));

            var initialOpenNodes = origin.GetAdjacentNodes;
            foreach (var node in initialOpenNodes)
                openSet.Add(new PathNode(node, new PathNode(origin,null)));

            int attempts = 0;
            while (openSet.Count > 0 && attempts < MAX_ATTEMPTS)
            {
                // find the lowest cost node in the open set
                PathNode best = FindLowestCostOpenNode(openSet, end, evaluationMethod);

                // if no best node, we got nothing...
                if (best == null)
                {
                    plotMethod(origin);
                    return;
                }

                // add the best node to the closed set
                closedSet.Add(best);

                // remove best node from open set
                RemoveBestNodeFromOpenSet(openSet, best);

                // break loop if best node is the target
                if (best.Node == end)
                    break;

                // get the adjacent nodes of the best nodes
                var adjacents = best.Node.GetAdjacentNodes;

                // add them to the open set if they aren't already open or closed
                AddAdjacentNodesToOpenSet(closedSet, openSet, best, adjacents);

                // increment attempt count
                attempts++;
            }

            // walk back through nodes to origin, plotting each one
            PlotFinalClosedSetToPath(origin, end, plotMethod, closedSet);

            plotCost(attempts);
        }

        private static void PlotFinalClosedSetToPath(INode origin, INode end, Action<INode> plotMethod, List<PathNode> closedSet)
        {
            PathNode pair = closedSet[closedSet.Count - 1];
            if (pair.Node == end)
            {
                List<INode> path = new List<INode>();

                while (pair.Node != origin)
                {
                    path.Insert(0, pair.Node);
                    pair = pair.Previous;
                }

                foreach (var node in path)
                    plotMethod(node);
            }
        }

        private static void AddAdjacentNodesToOpenSet(List<PathNode> closedSet, List<PathNode> openSet, PathNode best, List<INode> adjacents)
        {
            foreach (var node in adjacents)
            {
                bool isNew = true;
                foreach (var nodePair in openSet)
                    if (nodePair.Node == node) { isNew = false; break; }
                if (isNew) foreach (var nodePair in closedSet)
                        if (nodePair.Node == node) { isNew = false; break; }

                if (isNew)
                    openSet.Add(new PathNode(node, best));
            }
        }

        private static void RemoveBestNodeFromOpenSet(List<PathNode> openSet, PathNode best)
        {
            foreach (var thing in openSet)
                if (thing.Node == best.Node)
                {
                    openSet.Remove(thing);
                    break;
                }
        }


        private static PathNode FindLowestCostOpenNode(List<PathNode> openSet, INode end, NodeCostEvaluationMethod evaluationMethod)
        {
            PathNode best = null;
            foreach (PathNode curPair in openSet)
            {
                float cost = evaluationMethod(curPair.Node, end);

                if (cost >= 0)
                {
                    if (best == null)
                        best = curPair;
                    else if (cost < evaluationMethod(best.Node, end))
                        best = curPair;
                }
            }

            return best;
        }


    }
}
