﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class PathNode
    {
        public INode Node { get; set; }
        public PathNode Previous { get; set; }

        public PathNode(INode node, PathNode previous)
        {
            Node = node;
            Previous = previous;
        }
    }
}
