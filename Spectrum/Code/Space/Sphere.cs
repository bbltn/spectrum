﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class Sphere
    {
        public List<Vector3> Selection { get; private set; }

        public Sphere(Vector3 origin, float radius)
        {
            Selection = new List<Vector3>();

            int absRadius = (int)Math.Abs(radius);

            if (absRadius == 0)
                Selection.Add(origin);
            else
                AddPointsToSelection(origin, absRadius);
        }

        private void AddPointsToSelection(Vector3 origin, int radius)
        {
            for (int x = -radius; x <= radius; x++)
            {
                for (int y = -radius; y <= radius; y++)
                {
                    for (int z = -radius; z <= radius; z++)
                    {
                        Vector3 testPoint = new Vector3(x + origin.X, y + origin.Y, z + origin.Z);

                        if (Vector3.Distance(origin, testPoint) <= radius)
                            Selection.Add(testPoint);
                    }
                }
            }
        }

        public static bool Contains(Vector3 origin, float radius, Vector3 testPoint)
        {
            var distance = Vector3.Distance(origin, testPoint);

            if (distance <= radius)
                return true;
            
            return false;
        }
    }
}
