﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class Line
    {
        public List<Vector3> Points { get; private set; }

        public Line(Vector3 origin, Vector3 end)
        {
            Points = new List<Vector3>();

            DoLine((int)origin.X, (int)origin.Y, (int)origin.Z, (int)end.X, (int)end.Y, (int)end.Z, AddPoint);
        }

        public void AddPoint(int pointX, int pointY, int pointZ)
        {
            Points.Add(new Vector3(pointX, pointY, pointZ));
        }


        public static void DoLine(int startX, int startY, int startZ, int endX, int endY, int endZ, Action<int, int, int> callback)
        {
            if (callback == null) return;

            int dx, dy, dz;
            int sx, sy, sz;
            int accum, accum2;//accumilator

            dx = endX - startX;//Start X subtracted from End X
            dy = endY - startY;
            dz = endZ - startZ;

            sx = ((dx) < 0 ? -1 : ((dx) > 0 ? 1 : 0));//if dx is less than 0, sx = -1; otherwise if dx is greater than 0, sx = 1; otherwise sx = 0
            sy = ((dy) < 0 ? -1 : ((dy) > 0 ? 1 : 0));
            sz = ((dz) < 0 ? -1 : ((dz) > 0 ? 1 : 0));

            //dx = (dx < 0 ? -dx : dx);//if dx is less than 0, dx = -dx (becomes positive), otherwise nothing changes
            dx = Math.Abs(dx);//Absolute value
            //dy = (dy < 0 ? -dy : dy);
            dy = Math.Abs(dy);

            dz = Math.Abs(dz);

            endX += sx;//Add sx to End X
            endY += sy;
            endZ += sz;

            if (dx > dy)//if dx is greater than dy
            {
                if (dx > dz)
                {
                    accum = dx >> 1;
                    accum2 = accum;
                    do
                    {
                        callback(startX, startY, startZ);

                        accum -= dy;
                        accum2 -= dz;
                        if (accum < 0)
                        {
                            accum += dx;
                            startY += sy;
                        }
                        if (accum2 < 0)
                        {
                            accum2 += dx;
                            startZ += sz;
                        }
                        startX += sx;
                    }
                    while (startX != endX);
                }
                else
                {
                    accum = dz >> 1;
                    accum2 = accum;
                    do
                    {
                        callback(startX, startY, startZ);

                        accum -= dy;
                        accum2 -= dx;
                        if (accum < 0)
                        {
                            accum += dz;
                            startY += sy;
                        }
                        if (accum2 < 0)
                        {
                            accum2 += dz;
                            startX += sx;
                        }
                        startZ += sz;
                    }
                    while (startZ != endZ);
                }
            }
            else // dy is greater than dx
            {
                if (dy > dz)
                {
                    accum = dy >> 1;
                    accum2 = accum;
                    do
                    {
                        callback(startX, startY, startZ);

                        accum -= dx;
                        accum2 -= dz;
                        if (accum < 0)
                        {
                            accum += dy;
                            startX += sx;
                        }
                        if (accum2 < 0)
                        {
                            accum2 += dy;
                            startZ += sz;
                        }
                        startY += sy;
                    }
                    while (startY != endY);
                }
                else
                {
                    accum = dz >> 1;
                    accum2 = accum;
                    do
                    {
                        callback(startX, startY, startZ);

                        accum -= dx;
                        accum2 -= dy;
                        if (accum < 0)
                        {
                            accum += dx;
                            startX += sx;
                        }
                        if (accum2 < 0)
                        {
                            accum2 += dx;
                            startY += sy;
                        }
                        startZ += sz;
                    }
                    while (startZ != endZ);
                }
            }
        }


        /*
        public static List<Vector3> GetPoints(Vector3 origin, Vector3 end)
        {
            List<Vector3> selection = new List<Vector3>();

            // early out 
            if (origin.Equals(end))
            {
                selection.Add(origin);
                return selection;
            }


            bool done = false;

            selection.Add(origin);

            while (!done)
            {
                List<Vector3> openSet = new List<Vector3>();

                for (int x = -1; x <= 1; x++)
                {
                    for (int y = -1; y <= 1;y++)
                    {
                        for (int z = -1; z <= 1; z++)
                        {
                            openSet.Add(new Vector3(selection[selection.Count - 1].X + x, selection[selection.Count - 1].Y + y, selection[selection.Count - 1].Z + z));
                        }
                    }
                }

                Vector3 lowest = origin;

                foreach (var test in openSet)
                {
                    if (Vector3.Distance(end, test) < Vector3.Distance(end, lowest))
                        lowest = test;
                }

                selection.Add(lowest);

                if (lowest.Equals(end))
                {
                    done = true;
                }
            }



            return selection;
        }
        */

        /*
        public static List<Vector3> GetPoints(Vector3 origin, Vector3 end)
        {
            List<Vector3> selection = new List<Vector3>();

            // early out 
            if (origin.Equals(end))
            {
                selection.Add(origin);
                return selection;
            }

            
            int distance = (int)Vector3.Distance(origin, end);
            //distance++;
            for (int iter = 0; iter < distance; iter++)
            {
                int x = (int)((origin.X + (end.X - origin.X) * (1.0f / distance) * iter));
                int y = (int)((origin.Y + (end.Y - origin.Y) * (1.0f / distance) * iter));
                int z = (int)((origin.Z + (end.Z - origin.Z) * (1.0f / distance) * iter));

                Vector3 candidate = new Vector3(x,y,z);

                if (selection.Count == 0)
                    selection.Add(candidate);

                if (!selection[selection.Count-1].Equals(candidate))
                    selection.Add(new Vector3(x, y, z));
            }

            if (!selection.Contains(end)) selection.Add(end);
            
             

            return selection;
        }
         */
        
    }
}
