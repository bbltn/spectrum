﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public struct Vector3
    {
        public float X;
        public float Y;
        public float Z;

        public bool Contains(Vector3 test)
        {
            if (X < 0 || Y < 0 || Z < 0)
                return false;

            if (test.X >= X || test.X < 0)
                    return false;
            if (test.Y >= Y || test.Y < 0)
                    return false;
            if (test.Z >= Z || test.Z < 0)
                    return false;
            
            return true;
        }

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public static Vector3 Zero { get { return new Vector3(0, 0, 0); } }
        public static Vector3 One { get { return new Vector3(1, 1, 1); } }

        public static Vector3 FromString(string value)
        {
            if (value == null) return Vector3.Zero;

            string first = "";
            bool firstValue = false;

            string second = "";
            bool secondValue = false;

            string third = "";

            for (int iter = 0; iter < value.Count(); iter++)
            {
                char divider = ',';
                
                bool isDivider = value[iter] == divider;
                
                if (isDivider)
                {
                    if (!firstValue) firstValue = true;
                    else if (!secondValue) secondValue = true;
                }

                else if (!firstValue)
                    first += value[iter];
                else if (!secondValue)
                    second += value[iter];
                else 
                    third += value[iter];
            }

            var x = float.Parse(first);
            var y = float.Parse(second);
            var z = float.Parse(third);

            return new Vector3(x, y, z);
        }

        public override string ToString()
        {
            return (X.ToString() + "," + Y.ToString() + "," + Z.ToString());
        }

        public float GetDistance(Vector3 other)
        {
            return Vector3.Distance(this, other);
        }

        public static float Distance(Vector3 first, Vector3 second)
        {
            var firstPair = Math.Pow(second.X - first.X, 2);
            var secondPair = Math.Pow(second.Y - first.Y, 2);
            var thirdPair = Math.Pow(second.Z - first.Z, 2);

            return (float)Math.Sqrt(firstPair + secondPair + thirdPair);
        }

        public static Vector3 Normalize(Vector3 value)
        {
            if (value == Vector3.Zero) return value;

            float highestValue = Math.Abs(value.X);
            if (Math.Abs(value.Y) > highestValue) highestValue = Math.Abs(value.Y);
            if (Math.Abs(value.Z) > highestValue) highestValue = Math.Abs(value.Z);

            float newX = value.X / highestValue;
            float newY = value.Y / highestValue;
            float newZ = value.Z / highestValue;

            return new Vector3(newX, newY, newZ);
        }

        public static Vector3 CastValuesToInt(Vector3 value)
        {
            return new Vector3((int)value.X, (int)value.Y, (int)value.Z);
        }


        public static Vector3 operator +(Vector3 first, Vector3 second)
        {
            return new Vector3(first.X + second.X, first.Y + second.Y, first.Z + second.Z);
        }
        public static Vector3 operator +(Vector3 first, float second)
        {
            return new Vector3(first.X + second, first.Y + second, first.Z + second);
        }
        public static Vector3 operator +(Vector3 first, int second)
        {
            return new Vector3(first.X + second, first.Y + second, first.Z + second);
        }

        public static Vector3 operator -(Vector3 first, Vector3 second)
        {
            return new Vector3(first.X - second.X, first.Y - second.Y, first.Z - second.Z);
        }
        public static Vector3 operator -(Vector3 first, float second)
        {
            return new Vector3(first.X - second, first.Y - second, first.Z - second);
        }
        public static Vector3 operator -(Vector3 first, int second)
        {
            return new Vector3(first.X - second, first.Y - second, first.Z - second);
        }
        public static Vector3 operator *(Vector3 first, int second)
        {
            return new Vector3(first.X * second, first.Y * second, first.Z * second);
        }

        public static bool operator ==(Vector3 first, Vector3 second)
        {
            if (first == null && second == null) return true;
            if (first == null || second == null) return false;

            // If one is null, but not both, return false.
            if (((object)first == null) || ((object)second == null))
                return false;

            // Return true if the fields match:
            return first.X == second.X && first.Y == second.Y && first.Z == second.Z;
        }

        public static bool operator !=(Vector3 first, Vector3 second)
        {
            return !(first == second);
        }


        public override int GetHashCode()
        {
            return (int)Math.Pow((X * Y), Z);
        }
    }
}
