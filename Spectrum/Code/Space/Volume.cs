﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class Volume
    {
        public static List<Vector3> Box(Vector3 origin, Vector3 bounds)
        {
            List<Vector3> selection = new List<Vector3>();

            // early out for bad input
            if (bounds.X < 0 || bounds.Y < 0 || bounds.Z < 0)
                return selection;
             
            // loop through box and add points
            for (int x = 0; x < bounds.X; x++)
            {
                for (int y = 0; y < bounds.Y; y++)
                {
                    for (int z = 0; z < bounds.Z; z++)
                    {
                        selection.Add(new Vector3(x + origin.X, y + origin.Y, z + origin.Z));
                    }
                }
            }            

            return selection;
        }      
    }
}
