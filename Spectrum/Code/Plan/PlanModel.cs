﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public enum TaskType { MoveToTarget, Wait, Wander }

    public interface IPlan
    {
        TaskType Task { get; set; }
        IPath Path { get; set; }
    }

    public class PlanModel : IPlan
    {
        public TaskType Task { get; set; }
        public IPath Path { get; set; }

        public PlanModel()
        {
            Task = TaskType.MoveToTarget;
        }
    }
}
