﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IVariables
    {
        IDebugVariables Debug { get; }
        IGameVariables Game { get; }
    }

    public class VariableSet : IVariables
    {
        public IDebugVariables Debug { get; private set; }
        public IGameVariables Game { get; private set; }

        public VariableSet()
        {
            Debug = new DebugVariables();
            Game = new GameVariables();

            VariableOperations.Load(this);
        }
    }
}
