﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IDebugVariables
    {
        bool DebugDijkstra { get; set; }
        bool DebugLine { get; set; }
        bool DebugSphere { get; set; }
        bool DebugPath { get; set; }
        bool DrawAllActors { get; set; }
    }
    
    public class DebugVariables : IDebugVariables
    {
        // space / volume stuff
        public bool DebugLine { get; set; }
        public bool DebugDijkstra { get; set; }
        public bool DebugSphere { get; set; }
        public bool DebugPath { get; set; }
        public bool DrawAllActors { get; set; }

        public DebugVariables()
        {
            DebugDijkstra = false;
            DebugLine = false;
            DebugSphere = false;
            DebugPath = false;
            DrawAllActors = false;
        }
    }
}
