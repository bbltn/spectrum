﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IResources
    {
        Random RNG { get; }

        void AddItem(ItemModel item);
        IItem CreateItemInstance(string name);
        IItem CreateItemInstance(int index);
        int ItemCount { get; }

        void AddGun(GunModel gun);
        IGun CreateRandomGunInstance();
        IGun CreateRandomGunInstance(string tag);
        IGun CreateGunInstance(string name);
        IGun CreateGunInstance(int index);
        int GunCount { get; }

        void AddGarment(GarmentModel garment);
        IGarment CreateRandomGarmentInstance();
        IGarment CreateRandomGarmentInstance(string tag);
        IGarment CreateGarmentInstance(string name);
        IGarment CreateGarmentInstance(int index);
        int GarmentCount { get; }

        void AddTrait(ITrait trait);
        ITrait CreateTraitInstance(string name);

        void AddFirstName(BaseName name);
        void AddLastName(BaseName name);
        IName CreateName();
        IName CreateName(string tag);

        void AddFeature(IFeature newFeature);
        IFeature CreateFeatureInstance(string name);
        IFeature CreateFeatureInstance(int index);
        int FeatureCount { get; }

        void AddDetail(IDetail newDetail);
        IDetail CreateDetailInstance(string name);
        IDetail CreateDetailInstance(int index);
        int DetailCount { get; }

        void AddGas(IGas newGas);
        IGas CreateGasInstance(string name);
        IGas CreateGasInstance(int index);
        int GasCount { get; }
    }

    public class ResourceSet : IResources
    {
        public Random RNG { get; private set; }

        public ResourceSet()
        {
            RNG = new Random();

            Items = new List<ItemModel>();
            ItemOperations.LoadAllItems(this);

            Guns = new List<GunModel>();
            GunOperations.LoadAllGuns(this);

            Garments = new List<GarmentModel>();
            GarmentOperations.LoadAllGarments(this);

            Traits = new List<ITrait>();
            TraitOperations.LoadAllTraits(this);

            FirstNames = new List<BaseName>();
            LastNames = new List<BaseName>();
            NameOperations.LoadFirstNames(this);
            NameOperations.LoadLastNames(this);

            Features = new List<IFeature>();
            FeatureOperations.LoadAllFeatures(this);

            Details = new List<IDetail>();
            DetailOperations.LoadAllDetails(this);

            Gases = new List<IGas>();
            GasOperations.LoadAllGas(this);
        }

        // ================================

        private List<GarmentModel> Garments;
        public int GarmentCount { get { return Garments.Count; } }

        public void AddGarment(GarmentModel garment)
        { Garments.Add(garment); }

        public IGarment CreateRandomGarmentInstance()
        {
            return new GarmentInstance(Garments[RNG.Next(0, Garments.Count)]);
        }
        
        public IGarment CreateRandomGarmentInstance(string tag)
        {
            List<IGarment> openSet = new List<IGarment>();

            foreach (var garment in Garments)
                if (garment.Tags.Contains(tag))
                    openSet.Add(garment);

            //Random rng = new Random();
            
            return new GarmentInstance(openSet[RNG.Next(0,openSet.Count)]);
        }

        public IGarment CreateGarmentInstance(string name)
        {
            foreach (var garment in Garments)
                if (garment.Name == name)
                    return new GarmentInstance(garment);
            return null;
        }

        public IGarment CreateGarmentInstance(int index)
        {
            if (index >= 0 && index < Garments.Count)
                return new GarmentInstance(Garments[index]);
            return null;
        }
        



        // ================================

        private List<GunModel> Guns;
        public int GunCount { get { return Guns.Count; } }

        public void AddGun(GunModel gun)
        { Guns.Add(gun); }

        public IGun CreateRandomGunInstance()
        {
            return new GunInstance(Guns[RNG.Next(0, Guns.Count)]);
        }

        public IGun CreateRandomGunInstance(string tag)
        {
            List<IGun> openSet = new List<IGun>();

            foreach (var gun in Guns)
                if (gun.Tags.Contains(tag))
                    openSet.Add(gun);

            return new GunInstance(openSet[RNG.Next(0, openSet.Count)]);
        }

        public IGun CreateGunInstance(string name)
        {
            foreach (var gun in Guns)
                if (gun.Name == name)
                    return new GunInstance(gun);
            return null;
        }

        public IGun CreateGunInstance(int index)
        {
            if (index >= 0 && index < Guns.Count)
                return new GunInstance(Guns[index]);
            return null;
        }

        // ================================

        private List<ItemModel> Items;
        public int ItemCount { get { return Items.Count; } }

        public void AddItem(ItemModel item)
        {
            Items.Add(item);
        }

        public IItem CreateItemInstance(string name)
        {
            foreach (var item in Items)
                if (item.Name == name)
                    return new ItemInstance(item);
            return null;
        }

        public IItem CreateItemInstance(int index)
        {
            if (index >= 0 && index < Items.Count)
                return new ItemInstance(Items[index]);
            return null;
        }

        // ================================

        private List<ITrait> Traits;

        public void AddTrait(ITrait trait)
        {
            if (!Traits.Contains(trait))
                Traits.Add(trait);
        }

        public ITrait CreateTraitInstance(string name)
        {
            foreach (var trait in Traits)
                if (trait.Name == name)
                    return new TraitInstance(trait);
            return null;
        }


        // ================================

        private List<BaseName> FirstNames;
        private List<BaseName> LastNames;

        public void AddFirstName(BaseName name)
        { FirstNames.Add(name); }
        public void AddLastName(BaseName name)
        { LastNames.Add(name); }

        public IName CreateName()
        {
            IName value = new FullName();
            value.First = FirstNames[RNG.Next(0, FirstNames.Count)].Name;
            value.Last = LastNames[RNG.Next(0, LastNames.Count)].Name;
            return value;
        }

        public IName CreateName(string tag)
        {
            if (tag == null) return CreateName();

            List<BaseName> openFirstNames = new List<BaseName>();
            foreach (var name in FirstNames)
                if (tag.Length == 0 || name.Tags.Contains(tag)) openFirstNames.Add(name);

            List<BaseName> openLastNames = new List<BaseName>();
            foreach (var name in LastNames)
                if (tag.Length == 0 || name.Tags.Contains(tag)) openLastNames.Add(name);

            //Random RNG = new Random();

            IName value = new FullName();
            value.First = openFirstNames[RNG.Next(0, openFirstNames.Count)].Name;
            value.Last = openLastNames[RNG.Next(0, openLastNames.Count)].Name;

            return value;
        }

        // ================================

        private List<IFeature> Features;
        public int FeatureCount { get { return Features.Count; } }
        
        public void AddFeature(IFeature newFeature)
        { Features.Add(newFeature); }

        public IFeature CreateFeatureInstance(string name)
        {
            foreach (IFeature feature in Features)
                if (feature.Name == name)
                    return new FeatureInstance(feature);
            return null;
        }

        public IFeature CreateFeatureInstance(int index)
        {
            if (index >= 0 && index < Features.Count)
                return new FeatureInstance(Features[index]);
            return null;
        }

        // ================================

        private List<IDetail> Details;
        public int DetailCount { get { return Details.Count; } }


        public void AddDetail(IDetail newDetail)
        {  Details.Add(newDetail); }

        public IDetail CreateDetailInstance(string name)
        {
            foreach (IDetail detail in Details)
                if (detail.Name == name)
                    return new DetailInstance(detail);
            return null;
        }

        public IDetail CreateDetailInstance(int index)
        {
            if (index >= 0 && index < Features.Count)
                return new DetailInstance(Details[index]);
            return null;
        }

        // ================================

        private List<IGas> Gases;
        public int GasCount { get { return Gases.Count; } }


        public void AddGas(IGas newGas)
        { Gases.Add(newGas); }

        public IGas CreateGasInstance(string name)
        {
            foreach (IGas gas in Gases)
                if (gas.Name == name)
                    return new GasInstance(gas);
            return null;
        }

        public IGas CreateGasInstance(int index)
        {
            if (index >= 0 && index < Gases.Count)
                return new GasInstance(Gases[index]);
            return null;
        }
    }
}
