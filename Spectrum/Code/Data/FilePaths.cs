﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class FilePaths
    {
        public const string Feature = "Resources/Tiles/Features.txt";
        public const string Detail = "Resources/Tiles/Details.txt";
        public const string Gas = "Resources/Tiles/Gases.txt";

        public const string Save = "Resources/Save.txt";

        public const string Variables = "Resources/Game/Vars.txt";

        public const string FirstNames = "Resources/Strings/FirstNames.txt";
        public const string LastNames = "Resources/Strings/LastNames.txt";

        public const string Traits = "Resources/Actor/Traits.txt";

        public const string Items = "Resources/Items/Basic.txt";
        public const string Guns = "Resources/Items/Guns.txt";
        public const string Garments = "Resources/Items/Garments.txt";
    }
}
