﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class ItemOperations
    {
        public static void WriteItemToText(IItem item, List<string> lines)
        {
            if (item == null || lines == null) return;

            lines.Add("ITEM");
            WriteItemPropertiesToText(item, lines);
            lines.Add("END ITEM");
        }

        public static void WriteItemPropertiesToText(IItem item, List<string> lines)
        {
            if (item == null || lines == null) return;

            lines.Add("Name:" + item.Name);
            if (item.Tags.Count > 0)
                foreach (var tag in item.Tags)
                    lines.Add("Tag:" + tag);
            lines.Add("Description:" + item.Description);
            GraphicOperations.WriteGraphicToText(item.Graphic, lines);
            lines.Add("Size:" + item.Size.ToString());
            lines.Add("Weight:" + item.Weight);
            lines.Add("BashAptitude:" + item.BashAptitude);
            lines.Add("BashDamage:" + item.BashDamage);
        }

        public static void SaveItems(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.ItemCount; iter++)
            {
                WriteItemToText(resources.CreateItemInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Items, lines);
        }


        public static void LoadAllItems(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Items, "ITEM", index);
                if (lines.Length <= 0)
                    return;

                ItemModel item = new ItemModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            item.Name = value;
                            break;

                        case "Tag":
                            item.Tags.Add(value);
                            break;

                        case "Description":
                            item.Description = value;
                            break;

                        case "Graphic":
                            item.Graphic = GraphicOperations.ReadGraphicFromText(value);
                            break;

                        case "Size":
                            item.Size = (SizeType)Enum.Parse(typeof(SizeType), value);
                            break;

                        case "Weight":
                            item.Weight = int.Parse(value);
                            break;

                        case "BashAptitude":
                            item.Weight = int.Parse(value);
                            break;

                        case "BashDamage":
                            item.Weight = int.Parse(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddItem(item);
                index++;
            }
        }
    }
}
