﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class TileOperations
    {
        public static void LoadTilesIntoScene(IScene scene, IResources resources)
        {
            if (scene == null || resources == null) return;

            int index = 0;
            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Save, "TILE", index);
                if (lines.Length <= 0)
                    break;

                Vector3 position = Vector3.Zero;
                IFeature feature = null;
                IDetail detail = null;
                IGas gas = null;
                List<IItem> items = new List<IItem>();
                bool explored = false;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    string value = "";
                    string key = "";
                    bool isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "POSITION":
                            position = FileOperations.ReadVector3FromText(value);
                            break;

                        case "FEATURE":
                            feature = resources.CreateFeatureInstance(value);
                            break;

                        case "DETAIL":
                            detail = resources.CreateDetailInstance(value);
                            break;

                        case "GAS":
                            gas = resources.CreateGasInstance(value);
                            break;

                        case "ITEM":
                            items.Add(resources.CreateItemInstance(value));
                            break;

                        case "GUN":
                            items.Add(resources.CreateGunInstance(value).Item);
                            break;

                        case "GARMENT":
                            items.Add(resources.CreateGarmentInstance(value).Item);
                            break;

                            case "EXPLORED":
                            explored = Strings.StringToBool(value);
                            break;

                        default:
                            break;
                    }
                }

                ITile tile = scene.Tiles.GetTile(position);
                if (feature != null)
                    tile.Feature = feature;
                if (detail != null)
                    tile.Detail = detail;
                if (gas != null)
                    tile.Gas = gas;

                if (items.Count > 0)
                    for (int iter = 0; iter < items.Count; iter++)
                        tile.AddItem(items[iter]);

                tile.Explored = explored;

                index++;
            }
        }

        public static void WriteTileToText(List<string> lines, ITile tile)
        {
            if (lines == null || tile == null) return;

            lines.Add("TILE");

            lines.Add("POSITION:" + tile.Position.X + "," + tile.Position.Y + "," + tile.Position.Z);

            lines.Add("EXPLORED:" + Strings.BoolToString(tile.Explored));
            
            if (tile.Feature != null)
                lines.Add("FEATURE:" + tile.Feature.Name);

            if (tile.Detail != null)
                lines.Add("DETAIL:" + tile.Detail.Name);

            if (tile.Gas != null)
                lines.Add("GAS:" + tile.Gas.Name);

            if (tile.ItemCount > 0)
            {
                for (int iter = 0; iter < tile.ItemCount; iter++)
                {
                    IItem item = tile.GetItem(iter);

                    if (item.Gun != null)
                        lines.Add("GUN:" + item.Name);
                    else if (item.Garment != null)
                        lines.Add("GARMENT:" + item.Name);
                    else
                        lines.Add("ITEM:" + tile.GetItem(iter).Name);
                }
            }

            lines.Add("END TILE");
            lines.Add("");
        }
    }
}
