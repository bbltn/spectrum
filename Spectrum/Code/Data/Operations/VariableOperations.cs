﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class VariableOperations
    {
        public static void Save(IVariables variables)
        {
            if (variables == null) return;

            List<string> lines = new List<string>();
            SaveDebugVariables(variables.Debug, lines);
            lines.Add("");
            SaveGameVariables(variables.Game, lines);
            System.IO.File.WriteAllLines(FilePaths.Variables, lines);
        }

        public static void Load(IVariables variables)
        {
            if (variables == null) return;

            LoadDebugVariables(variables.Debug);
            LoadGameVariables(variables.Game);
        }

        // ==============================================================

        public static void SaveDebugVariables(IDebugVariables variables, List<string> lines)
        {
            if (variables == null || lines == null) return;

            lines.Add("DEBUG");
            lines.Add("DebugDijkstra:" + Strings.BoolToString(variables.DebugDijkstra));
            lines.Add("DebugLine:" + Strings.BoolToString(variables.DebugLine));
            lines.Add("DebugSphere:" + Strings.BoolToString(variables.DebugSphere));
            lines.Add("DebugPath:" + Strings.BoolToString(variables.DebugPath));
            lines.Add("DrawAllActors:" + Strings.BoolToString(variables.DrawAllActors));
            lines.Add("END DEBUG");
        }

        public static void LoadDebugVariables(IDebugVariables variables)
        {
            if (variables == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Variables, "DEBUG", index);
                if (lines.Length <= 0)
                    return;

                //FeatureModel feature = new FeatureModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "DebugDijkstra":
                            variables.DebugDijkstra = Strings.StringToBool(value);
                            break;

                        case "DebugLine":
                            variables.DebugLine = Strings.StringToBool(value);
                            break;
                            
                        case "DebugSphere":
                            variables.DebugSphere = Strings.StringToBool(value);
                            break;

                        case "DebugPath":
                            variables.DebugPath = Strings.StringToBool(value);
                            break;

                        case "DrawAllActors":
                            variables.DrawAllActors = Strings.StringToBool(value);
                            break;

                        default:
                            break;
                    }
                }

                index++;
            }
        }   


        // ==============================================================


        public static void SaveGameVariables(IGameVariables variables, List<string> lines)
        {
            if (variables == null || lines == null) return;

            lines.Add("GAME");
            lines.Add("END GAME");
        }

        public static void LoadGameVariables(IGameVariables variables)
        {
            if (variables == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Variables, "GAME", index);
                if (lines.Length <= 0)
                    return;

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        /*
                        case "DebugLine":
                            vars.DebugLine = Strings.StringToBool(value);
                            break;
                        */
                        default:
                            break;
                    }
                }

                index++;
            }
        }  
    }
}
