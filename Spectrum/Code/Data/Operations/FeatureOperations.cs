﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class FeatureOperations
    {
        private static void WriteFeatureToText(IFeature feature, List<string> lines)
        {
            if (feature == null || lines == null) return;

            lines.Add("FEATURE");
            lines.Add("Name:" + feature.Name);
            lines.Add("Description:" + feature.Description);
            GraphicOperations.WriteGraphicToText(feature.Graphic, lines);
            lines.Add("BlocksVision:" + Strings.BoolToString(feature.BlocksVision));
            lines.Add("BlocksSound:" + Strings.BoolToString(feature.BlocksSound));
            lines.Add("BlocksSpace:" + Strings.BoolToString(feature.BlocksSpace));
            lines.Add("END FEATURE");
        }

        public static void SaveFeatures(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.FeatureCount; iter++)
            {
                WriteFeatureToText(resources.CreateFeatureInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Feature, lines);
        }

        public static void LoadAllFeatures(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Feature, "FEATURE", index);
                if (lines.Length <= 0)
                    return;

                FeatureModel feature = new FeatureModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            feature.Name = value;
                            break;

                        case "Description":
                            feature.Description = value;
                            break;

                        case "Graphic":
                            feature.Graphic = GraphicOperations.ReadGraphicFromText(value);
                            break;

                        case "BlocksVision":
                            feature.BlocksVision = Strings.StringToBool(value);
                            break;

                        case "BlocksSound":
                            feature.BlocksSound = Strings.StringToBool(value);
                            break;

                        case "BlocksSpace":
                            feature.BlocksSpace = Strings.StringToBool(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddFeature(feature);
                index++;
            }
        }      
    }
}
