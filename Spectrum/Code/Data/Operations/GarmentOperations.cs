﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class GarmentOperations
    {
        public static void WriteGarmentToText(IGarment garment, List<string> lines)
        {
            if (garment == null || lines == null) return;

            lines.Add("GARMENT");
            ItemOperations.WriteItemPropertiesToText(garment.Item, lines);
            WriteGarmentPropertiesToText(garment, lines);
            lines.Add("END GARMENT");
        }

        private static void WriteGarmentPropertiesToText(IGarment garment, List<string> lines)
        {
            if (garment == null || lines == null) return;

            lines.Add("BodyPart:" + garment.BodyPart.ToString());
            lines.Add("Armor:" + garment.Armor.ToString());
        }


        public static void SaveGarments(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.GarmentCount; iter++)
            {
                WriteGarmentToText(resources.CreateGarmentInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Garments, lines);
        }

        public static void LoadAllGarments(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Garments, "GARMENT", index);
                if (lines.Length <= 0)
                    return;

                GarmentModel garment = new GarmentModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            garment.Name = value;
                            break;

                        case "Tag":
                            garment.Tags.Add(value);
                            break;

                        case "Description":
                            garment.Description = value;
                            break;

                        case "Graphic":
                            garment.Graphic = GraphicOperations.ReadGraphicFromText(value);
                            break;

                        case "Size":
                            garment.Size = (SizeType)Enum.Parse(typeof(SizeType), value);
                            break;

                        case "Weight":
                            garment.Weight = int.Parse(value);
                            break;

                        case "BashAptitude":
                            garment.Weight = int.Parse(value);
                            break;

                        case "BashDamage":
                            garment.Weight = int.Parse(value);
                            break;

                        case "BodyPart":
                            garment.BodyPart = (BodyPartType)Enum.Parse(typeof(BodyPartType), value);
                            break;

                        case "Armor":
                            garment.Armor = (ArmorType)Enum.Parse(typeof(ArmorType), value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddGarment(garment);
                index++;
            }
        }



    }
}
