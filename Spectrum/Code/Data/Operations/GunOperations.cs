﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class GunOperations
    {
        public static void WriteGunToText(IGun gun, List<string> lines)
        {
            if (gun == null || lines == null) return;

            lines.Add("GUN");
            ItemOperations.WriteItemPropertiesToText(gun.Item, lines);
            WriteGunPropertiesToText(gun, lines);
            lines.Add("END GUN");
        }

        private static void WriteGunPropertiesToText(IGun gun, List<string> lines)
        {
            if (gun == null || lines == null) return;

            lines.Add("Range:" + gun.Range);
            lines.Add("Caliber:" + gun.Caliber.ToString());
            lines.Add("Stock:" + gun.Stock.ToString());
            lines.Add("Barrel:" + gun.Barrel.ToString());
            lines.Add("Optic:" + gun.Optic.ToString());
            lines.Add("Foregrip:" + gun.Foregrip.ToString());
            lines.Add("Muzzle:" + gun.Muzzle.ToString());
            if (gun.ActionCount > 0)
                for (int iter = 0; iter < gun.ActionCount; iter++)
                    lines.Add("Action:" + gun.GetAction(iter).ToString());
            lines.Add("CurrentAction:" + gun.CurrentAction.ToString());
        }


        public static void SaveGuns(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.GunCount; iter++)
            {
                WriteGunToText(resources.CreateGunInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Guns, lines);
        }


        public static void LoadAllGuns(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Guns, "GUN", index);
                if (lines.Length <= 0)
                    return;

                GunModel gun = new GunModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    ConfigureGunProperty(gun, key, value);
                }

                resources.AddGun(gun);
                index++;
            }
        }

        private static void ConfigureGunProperty(GunModel gun, string key, string value)
        {
            switch (key)
            {
                case "Name":
                    gun.Name = value;
                    break;

                case "Tag":
                    gun.Tags.Add(value);
                    break;

                case "Description":
                    gun.Description = value;
                    break;

                case "Graphic":
                    gun.Graphic = GraphicOperations.ReadGraphicFromText(value);
                    break;

                case "Size":
                    gun.Size = (SizeType)Enum.Parse(typeof(SizeType), value);
                    break;

                case "Weight":
                    gun.Weight = int.Parse(value);
                    break;

                case "BashAptitude":
                    gun.BashAptitude = int.Parse(value);
                    break;

                case "BashDamage":
                    gun.BashDamage = int.Parse(value);
                    break;

                case "Caliber":
                    gun.Caliber = (CaliberType)Enum.Parse(typeof(CaliberType), value);
                    break;

                case "Range":
                    gun.Range = int.Parse(value);
                    break;

                case "Stock":
                    gun.Stock = (StockType)Enum.Parse(typeof(StockType), value);
                    break;

                case "Barrel":
                    gun.Barrel = (BarrelType)Enum.Parse(typeof(BarrelType), value);
                    break;

                case "Optics":
                    gun.Optic = (OpticType)Enum.Parse(typeof(OpticType), value);
                    break;

                case "Foregrip":
                    gun.Foregrip = (ForegripType)Enum.Parse(typeof(ForegripType), value);
                    break;

                case "Muzzle":
                    gun.Muzzle = (MuzzleType)Enum.Parse(typeof(MuzzleType), value);
                    break;

                case "Action":
                    gun.AddAction((GunActionType)Enum.Parse(typeof(GunActionType), value));
                    break;

                case "CurrentAction":
                    gun.CurrentAction = (GunActionType)Enum.Parse(typeof(GunActionType), value);
                    break;

                default:
                    break;
            }
        }
    }
}
