﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class NameOperations
    {
        private static void WriteNameToText(BaseName name, List<string> lines)
        {
            lines.Add("NAME");
            lines.Add("Name:" + name.Name);
            foreach (var tag in name.Tags)
                lines.Add("Tag:" + tag);
            lines.Add("END NAME");
        }

        public static void LoadFirstNames(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.FirstNames, "NAME", index);
                if (lines.Length <= 0)
                    return;

                BaseName name = new BaseName();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            name.Name = value;
                            break;

                        case "Tag":
                            name.Tags.Add(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddFirstName(name);
                index++;
            }
        }


        public static void LoadLastNames(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.LastNames, "NAME", index);
                if (lines.Length <= 0)
                    return;

                BaseName name = new BaseName();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            name.Name = value;
                            break;

                        case "Tag":
                            name.Tags.Add(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddLastName(name);
                index++;
            }
        }

    }
}
