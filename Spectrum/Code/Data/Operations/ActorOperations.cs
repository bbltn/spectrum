﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class ActorOperations
    {
        public static void WriteActorToText(List<string> lines, IActor actor)
        {
            if (lines == null || actor == null) return;

            lines.Add("ACTOR");
            lines.Add("POSITION:" + actor.Position.X + "," + actor.Position.Y + "," + actor.Position.Z);
            lines.Add("CONTROL:" + actor.Control.ToString());
            lines.Add("STANCE:" + actor.Stance.ToString());
            WriteNameToText(lines, actor.Name);
            WriteHealthToText(lines, actor.Health);
            WriteMoraleToText(lines, actor.Morale);
            WriteSkillsToText(lines, actor.Skill);
            WriteTraitsToText(lines, actor.Traits);
            WriteInventoryToText(lines, actor.Inventory);
            lines.Add("END ACTOR");
            lines.Add("");
        }

        private static void WriteNameToText(List<string> lines, IName name)
        {
            lines.Add("FIRST_NAME:" + name.First);
            lines.Add("LAST_NAME:" + name.Last);
        }

        private static void WriteHealthToText(List<string> lines, IHealth health)
        {
            lines.Add("HEALTH_STATUS:" + health.Status.ToString());
            lines.Add("CONSCIOUSNESS_STATUS:" + health.Consciousness.ToString());
        }

        private static void WriteMoraleToText(List<string> lines, IMorale morale)
        {
            lines.Add("MORALE_RATING:" + morale.Rating);
            lines.Add("MORALE_STATUS:" + morale.Status.ToString());
        }

        private static void WriteSkillsToText(List<string> lines, ISkills skills)
        {
            lines.Add("SKILL_LEADERSHIP:" + skills.Leadership);
            lines.Add("SKILL_UNARMED:" + skills.Unarmed);
            lines.Add("SKILL_GUN:" + skills.Gun);
            lines.Add("SKILL_MEDIC:" + skills.Medic);
        }

        private static void WriteInventoryToText(List<string> lines, IInventory inventory)
        {
            WriteEquippedItemToText(lines, inventory);
            WriteWornItemToText(lines, inventory);
            WriteInventoryItemsToText(lines, inventory);
        }

        private static void WriteInventoryItemsToText(List<string> lines, IInventory inventory)
        {
            if (inventory.ItemCount > 0)
                for (int iter = 0; iter < inventory.ItemCount; iter++)
                {
                    IItem item = inventory.GetItem(iter);
                    string prefix = "INVENTORY_ITEM:";

                    if (item.Gun != null)
                        prefix = "INVENTORY_GUN:";
                    else if (item.Garment != null)
                        prefix = "INVENTORY_GARMENT:";

                    lines.Add(prefix + item.Name);
                }
        }

        private static void WriteWornItemToText(List<string> lines, IInventory inventory)
        {
            if (inventory.Wearing != null)
            {
                lines.Add("WEARING:" + inventory.Wearing.Item.Name);
            }
        }

        private static void WriteEquippedItemToText(List<string> lines, IInventory inventory)
        {
            if (inventory.Equipped != null)
            {
                var item = inventory.Equipped;
                string prefix = "EQUIPPED_ITEM:";

                if (item.Gun != null)
                    prefix = "EQUIPPED_GUN:";

                else if (item.Garment != null)
                    prefix = "EQUIPPED_GARMENT:";

                lines.Add(prefix + item.Name);
            }
        }

        private static void WriteTraitsToText(List<string> lines, ITraits traits)
        {
            if (traits.Count > 0)
            {
                for (int iter = 0; iter < traits.Count; iter++)
                    lines.Add("TRAIT:" + traits.GetTrait(iter).Name);
            }
        }

        public static void LoadActorsIntoScene(IScene scene, IResources resources)
        {
            if (scene == null || resources == null) return;

            int index = 0;
            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Save, "ACTOR", index);
                if (lines.Length <= 0)
                    break;

                //Vector3 position = Vector3.Zero;
                IActor newActor = new ActorModel(Vector3.Zero);

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    string value = "";
                    string key = "";
                    bool isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    ConfigureActor(scene, resources, newActor, value, key);
                }

                index++;
            }
        }

        private static void ConfigureActor(IScene scene, IResources resources, IActor newActor, string value, string key)
        {
            switch (key)
            {
                case "POSITION":
                    Vector3 position = FileOperations.ReadVector3FromText(value);
                    newActor.Position = position;
                    ITile tile = scene.Tiles.GetTile(position);
                    tile.Actor = newActor;
                    scene.Actors.Add(newActor);
                    break;

                case "CONTROL":
                    newActor.Control = (ControlType)Enum.Parse(typeof(ControlType), value);
                    break;

                case "STANCE":
                    newActor.Stance = (StanceType)Enum.Parse(typeof(StanceType), value);
                    break;

                case "FIRST_NAME":
                    newActor.Name.First = value;
                    break;

                case "LAST_NAME":
                    newActor.Name.Last = value;
                    break;

                case "HEALTH_STATUS":
                    newActor.Health.Status = (HealthStatusType)Enum.Parse(typeof(HealthStatusType), value);
                    break;

                case "CONSCIOUSNESS_STATUS":
                    newActor.Health.Consciousness = (ConsciousStatusType)Enum.Parse(typeof(ConsciousStatusType), value);
                    break;

                case "MORALE_RATING":
                    newActor.Morale.Rating = int.Parse(value);
                    break;

                case "MORALE_STATUS":
                    newActor.Morale.Status = (MoraleStatusType)Enum.Parse(typeof(MoraleStatusType), value);
                    break;

                case "SKILL_LEADERSHIP":
                    newActor.Skill.Leadership = int.Parse(value);
                    break;

                case "SKILL_UNARMED":
                    newActor.Skill.Unarmed = int.Parse(value);
                    break;

                case "SKILL_GUN":
                    newActor.Skill.Gun = int.Parse(value);
                    break;

                case "SKILL_MEDIC":
                    newActor.Skill.Medic = int.Parse(value);
                    break;

                case "TRAIT":
                    newActor.Traits.Add(resources.CreateTraitInstance(value));
                    break;

                case "EQUIPPED_GUN":
                    newActor.Inventory.Equipped = resources.CreateGunInstance(value).Item;
                    break;

                case "EQUIPPED_GARMENT":
                    newActor.Inventory.Equipped = resources.CreateGarmentInstance(value).Item;
                    break;

                case "EQUIPPED_ITEM":
                    newActor.Inventory.Equipped = resources.CreateItemInstance(value);
                    break;

                case "WEARING":
                    newActor.Inventory.Wearing = resources.CreateGarmentInstance(value);
                    break;

                case "INVENTORY_GUN":
                    newActor.Inventory.AddItem(resources.CreateGunInstance(value).Item);
                    break;

                case "INVENTORY_GARMENT":
                    newActor.Inventory.AddItem(resources.CreateGarmentInstance(value).Item);
                    break;

                case "INVENTORY_ITEM":
                    newActor.Inventory.AddItem(resources.CreateItemInstance(value));
                    break;

                default:
                    break;
            }
        }
    }
}
