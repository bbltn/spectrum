﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class GasOperations
    {
        private static void WriteGasToText(IGas gas, List<string> lines)
        {
            lines.Add("GAS");
            lines.Add("Name:" + gas.Name);
            GraphicOperations.WriteGraphicToText(gas.Graphic, lines);
            lines.Add("BlocksVision:" + Strings.BoolToString(gas.BlocksVision));
            lines.Add("END GAS");
        }

        public static void SaveGas(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.DetailCount; iter++)
            {
                WriteGasToText(resources.CreateGasInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Detail, lines);
        }

        public static void LoadAllGas(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Gas, "GAS", index);
                if (lines.Length <= 0)
                    return;

                GasModel gas = new GasModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            gas.Name = value;
                            break;

                        case "Graphic":
                            gas.Graphic = GraphicOperations.ReadGraphicFromText(value);
                            break;

                        case "BlocksVision":
                            gas.BlocksVision = Strings.StringToBool(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddGas(gas);
                index++;
            }
        }

    }
}
