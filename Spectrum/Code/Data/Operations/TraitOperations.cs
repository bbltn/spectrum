﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class TraitOperations
    {
        private static void WriteTraitToText(ITrait trait, List<string> lines)
        {
            lines.Add("TRAIT");
            lines.Add("Name:" + trait.Name);
            lines.Add("Description:" + trait.Description);
            lines.Add("END TRAIT");
        }

        public static void LoadAllTraits(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Traits, "TRAIT", index);
                if (lines.Length <= 0)
                    return;

                TraitModel trait = new TraitModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            trait.Name = value;
                            break;

                        case "Description":
                            trait.Description = value;
                            break;

                        default:
                            break;
                    }
                }

                resources.AddTrait(trait);
                index++;
            }
        }
    }
}
