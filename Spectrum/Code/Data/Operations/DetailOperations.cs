﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class DetailOperations
    {
        private static void WriteDetailToText(IDetail detail, List<string> lines)
        {
            
            lines.Add("DETAIL");
            lines.Add("Name:" + detail.Name);
            GraphicOperations.WriteGraphicToText(detail.Graphic, lines);
            lines.Add("OverrideForeColor:" + Strings.BoolToString(detail.OverrideForeColor));
            lines.Add("OverrideBackColor:" + Strings.BoolToString(detail.OverrideBackColor));
            lines.Add("OverrideIcon:" + Strings.BoolToString(detail.OverrideIcon));
            lines.Add("END DETAIL");
             
        }

        public static void SaveDetails(IResources resources)
        {
            if (resources == null) return;

            List<string> lines = new List<string>();

            for (int iter = 0; iter < resources.DetailCount; iter++)
            {
                WriteDetailToText(resources.CreateDetailInstance(iter), lines);
                lines.Add("");
            }

            System.IO.File.WriteAllLines(FilePaths.Detail, lines);
        }

        public static void LoadAllDetails(IResources resources)
        {
            if (resources == null) return;

            int index = 0;

            while (true)
            {
                string[] lines = FileOperations.LoadElementByIndex(FilePaths.Detail, "DETAIL", index);
                if (lines.Length <= 0)
                    return;

                DetailModel detail = new DetailModel();

                string key;
                string value;
                bool isSettingKey;

                for (int iter = 0; iter < lines.Length; iter++)
                {
                    value = "";
                    key = "";
                    isSettingKey = true;

                    for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                    {
                        if (lines[iter][letterIndex] == ':')
                            isSettingKey = false;

                        else if (isSettingKey)
                            key += lines[iter][letterIndex];

                        else
                            value += lines[iter][letterIndex];
                    }

                    switch (key)
                    {
                        case "Name":
                            detail.Name = value;
                            break;

                        case "Graphic":
                            detail.Graphic = GraphicOperations.ReadGraphicFromText(value);
                            break;

                        case "OverrideForeColor":
                            detail.OverrideForeColor = Strings.StringToBool(value);
                            break;

                        case "OverrideBackColor":
                            detail.OverrideBackColor = Strings.StringToBool(value);
                            break;

                        case "OverrideIcon":
                            detail.OverrideIcon = Strings.StringToBool(value);
                            break;

                        default:
                            break;
                    }
                }

                resources.AddDetail(detail);
                index++;
            }
        }

    }
}
