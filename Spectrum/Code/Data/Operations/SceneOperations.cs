﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public static class SceneOperations
    {
        public static void WriteSceneToText(IScene scene, List<string> lines)
        {
            if (scene == null || lines == null) return;

            lines.Add("SCENE");
            lines.Add("SIZE:" + scene.Tiles.Size.X + "," + scene.Tiles.Size.Y + "," + scene.Tiles.Size.Z);
            lines.Add("TIME:" + scene.Time.Absolute);
            lines.Add("END SCENE");
            lines.Add("");

            // loop through each tile to save actors
            for (int x = 0; x < scene.Tiles.Size.X; x++)
            {
                for (int y = 0; y < scene.Tiles.Size.Y; y++)
                {
                    for (int z = 0; z < scene.Tiles.Size.Z; z++)
                    {
                        Vector3 tilePosition = new Vector3(x, y, z);
                        ITile tile = scene.Tiles.GetTile(tilePosition);
                        if (tile.Actor != null)
                            ActorOperations.WriteActorToText(lines, tile.Actor);
                    }
                }
            }

            // loop through each tile to save features, details
            for (int x = 0; x < scene.Tiles.Size.X; x++)
            {
                for (int y = 0; y < scene.Tiles.Size.Y; y++)
                {
                    for (int z = 0; z < scene.Tiles.Size.Z; z++)
                    {
                        Vector3 tilePosition = new Vector3(x, y, z);
                        ITile tile = scene.Tiles.GetTile(tilePosition);
                        if (tile.Feature != null || tile.Detail != null || tile.Gas != null || tile.ItemCount > 0)
                            TileOperations.WriteTileToText(lines, tile);
                    }
                }
            }
        }

        public static void LoadScene(IScene scene, IResources resources)
        {
            if (scene == null || resources == null) return;

            // load the Scene element
            LoadSceneElement(scene);

            // load each tile element   
            TileOperations.LoadTilesIntoScene(scene, resources);
            

            // load each actor             
            ActorOperations.LoadActorsIntoScene(scene, resources);

            scene.Camera.Perspective = scene.Actors.GetPlayer;

            // add default objective
            ICondition testObjective = new Objective_KillAllBots(scene);
            scene.Objectives.Add(testObjective);
        }

        private static void LoadSceneElement(IScene scene)
        {
            string[] lines = FileOperations.LoadElementByIndex(FilePaths.Save, "SCENE", 0);
            if (lines.Length <= 0)
                return;

            // parse the element for info
            for (int iter = 0; iter < lines.Length; iter++)
            {
                string value = "";
                string key = "";
                bool isSettingKey = true;

                for (int letterIndex = 0; letterIndex < lines[iter].Length; letterIndex++)
                {
                    if (lines[iter][letterIndex] == ':')
                        isSettingKey = false;

                    else if (isSettingKey)
                        key += lines[iter][letterIndex];

                    else
                        value += lines[iter][letterIndex];
                }

                switch (key)
                {
                    case "SIZE":
                        Vector3 size = FileOperations.ReadVector3FromText(value);
                        scene.Initialize(size);
                        break;

                    case "TIME":
                        scene.Time.SetAbsolute(int.Parse(value));
                        break;

                    default:
                        break;
                }
            }
        }
    }
}
