﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public static class FileOperations
    {        
        public static void Save(string path, IScene scene)
        {
            List<string> saveLines = new List<string>();

            SceneOperations.WriteSceneToText(scene, saveLines);

            System.IO.File.WriteAllLines(path, saveLines);
        }
         


        public static Vector3 ReadVector3FromText(string line)
        {
            if (line == null) return Vector3.Zero;

            int transcribeIndex = 0;
            string x = "";
            string y = "";
            string z = "";

            for (int letterIndex = 0; letterIndex < line.Length; letterIndex++)
            {
                if (line[letterIndex] == ',')
                    transcribeIndex++;

                else if (transcribeIndex == 0)
                    x += line[letterIndex];
                else if (transcribeIndex == 1)
                    y += line[letterIndex];
                else if (transcribeIndex == 2)
                    z += line[letterIndex];
            }

            return new Vector3(float.Parse(x), float.Parse(y), float.Parse(z));
        }

        

        
        public static string[] LoadElementByIndex(string path, string elementType, int instance)
        {

            var file = System.IO.File.ReadAllLines(path);

            List<string> lines = new List<string>();
            bool isTargetLine = false;
            int instanceCounter = 0;
            for (int currentLine = 0; currentLine < file.Length; currentLine++)
            {
                if (file[currentLine] == "END " + elementType)
                    isTargetLine = false;

                if (isTargetLine) lines.Add(file[currentLine]);

                if (file[currentLine].Contains(elementType) && file[currentLine] != "END " + elementType)
                {
                    if (instanceCounter == instance)
                        isTargetLine = true;

                    instanceCounter++;
                }
            }

            return lines.ToArray();
        }

        public static string[] LoadElementByName(string path, string elementType, string name)
        {
            var file = System.IO.File.ReadAllLines(path);

            List<string> lines = new List<string>();
            bool isTargetLine = false;

            for (int iter = 0; iter < file.Length; iter++)
            {
                if (file[iter] == "END " + elementType)
                    isTargetLine = false;

                if (isTargetLine) lines.Add(file[iter]);

                if (file[iter] == elementType + " " + name)
                    isTargetLine = true;
            }

            return lines.ToArray();
        }

        public static Glyph StringToGlyph(string value)
        {
            var target = Enum.Parse(typeof(Glyph), value);
            return (Glyph)target;
        }

        public static TermColor StringToTermColor(string value)
        {
            
            var target = Enum.Parse(typeof(TermColor), value);
            return (TermColor)target;
             
        }        

        public static Vec StringToVec(string value)
        {
            Vec answer = Vec.Zero;

            if (value == null) return answer;

            string first = "";
            string second = "";
            bool hasPassedComma = false;

            for (int x = 0; x < value.Length; x++)
            {
                if (value[x] == ',')
                    hasPassedComma = true;

                else if (!hasPassedComma)
                    first += value[x];

                else
                    second += value[x];
            }

            answer.X = int.Parse(first);
            answer.Y = int.Parse(second);

            return answer;
        }
    }
}
