﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public static class GraphicOperations
    {
        public static void WriteGraphicToText(IGraphic graphic, List<string> lines)
        {
            if (graphic == null || lines == null) return;

            lines.Add("Graphic:" + graphic.Icon.ToString() + "," + graphic.ForeColor.ToString() + "," + graphic.BackColor.ToString());
        }

        public static IGraphic ReadGraphicFromText(string line)
        {

            IGraphic graphic = new AsciiGraphic();

            if (line == null) return graphic;

            int transcribeIndex = 0;
            string icon = "";
            string foreColor = "";
            string backColor = "";


            for (int letterIndex = 0; letterIndex < line.Length; letterIndex++)
            {
                if (line[letterIndex] == ',')
                    transcribeIndex++;

                else if (transcribeIndex == 0)
                    icon += line[letterIndex];
                else if (transcribeIndex == 1)
                    foreColor += line[letterIndex];
                else if (transcribeIndex == 2)
                    backColor += line[letterIndex];
            }

            graphic.Icon = new Character(FileOperations.StringToGlyph(icon));
            graphic.ForeColor = FileOperations.StringToTermColor(foreColor);
            graphic.BackColor = FileOperations.StringToTermColor(backColor);

            return graphic;
        }
    }
}
