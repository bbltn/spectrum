﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Bramble.Core;

namespace Spectrum
{
    public interface IInputProcessor
    {
        void Register(KeyEventArgs key);
        bool HasInput { get; }
        BaseCommand PullCommand();
    }
    
    public class InputProcessor : IInputProcessor
    {
        private List<KeyEventArgs> keysPressed;
        public bool HasInput
        {
            get { return keysPressed.Count > 0; }
        }

        public InputProcessor()
        {
            keysPressed = new List<KeyEventArgs>();
        }

        public BaseCommand PullCommand()
        {
            if (HasInput)
            {
                var command = KeyToCommand(keysPressed[0]);
                keysPressed.RemoveAt(0);
                return command;
            }

            else
                return new BaseCommand();
        }

        static private BaseCommand KeyToCommand(KeyEventArgs key)
        {
            
            if (key.KeyCode == Keys.D1 || key.KeyCode == Keys.NumPad1)
                    return new VectorCommand(new Vec(-1, 1));
            
            if (key.KeyCode == Keys.D2 || key.KeyCode == Keys.NumPad2 || key.KeyCode == Keys.Down)
                return new VectorCommand(new Vec(0, 1));
            
            if (key.KeyCode == Keys.D3 || key.KeyCode == Keys.NumPad3)
                    return new VectorCommand(new Vec(1, 1));

            if (key.KeyCode == Keys.D4 || key.KeyCode == Keys.NumPad4 || key.KeyCode == Keys.Left)
                    return new VectorCommand(new Vec(-1, 0));
            
            if (key.KeyCode == Keys.D5 || key.KeyCode == Keys.NumPad5)
                    return new VectorCommand(new Vec(0, 0));

            if (key.KeyCode == Keys.D6 || key.KeyCode == Keys.NumPad6 || key.KeyCode == Keys.Right)
                    return new VectorCommand(new Vec(1, 0));
            
            if (key.KeyCode == Keys.D7 || key.KeyCode == Keys.NumPad7)
                    return new VectorCommand(new Vec(-1, -1));

            if (key.KeyCode == Keys.D8 || key.KeyCode == Keys.NumPad8 || key.KeyCode == Keys.Up)
                    return new VectorCommand(new Vec(0, -1));
            
            if (key.KeyCode == Keys.D9 || key.KeyCode == Keys.NumPad9)
                    return new VectorCommand(new Vec(1, -1));

            return new KeyCommand(key.KeyCode);
                         
        }

        public void Register(KeyEventArgs key)
        {
            keysPressed.Add(key);
        }        
      
    }
}
