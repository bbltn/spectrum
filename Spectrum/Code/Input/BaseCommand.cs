﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Bramble.Core;

namespace Spectrum
{
    public class BaseCommand
    {
        public BaseCommand()
        {
        }
    }

    public class VectorCommand : BaseCommand
    {
        public Vec Vector { get; private set; }

        public VectorCommand(Vec newVector) : base()
        {
            Vector = newVector;
        }
    }

    public class KeyCommand : BaseCommand
    {
        public Keys Key { get; private set; }

        public KeyCommand(Keys newLetter)
            : base()
        {
            Key = newLetter;
        }
    }

    public class NumberCommand : BaseCommand
    {
        public int Number { get; private set; }

        public NumberCommand(int newNumber)
            : base()
        {
            Number = newNumber;
        }
    }
}
