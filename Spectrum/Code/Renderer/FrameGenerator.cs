﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public interface IFrameGenerator
    {
        void AddWindow(IWindow window);
        List<Vec> ChangedPoints { get; }
        IGraphic GraphicAtPosition(Vec position);
        void Update(GameSimulation model, IMenu menu);

        Vec Dimensions { get; }
    }
    
    public class FrameGenerator : IFrameGenerator
    {
        private IFrame LastFrame;
        private IFrame NextFrame;

        public Vec Dimensions { get; private set; }

        public List<Vec> ChangedPoints { get; private set; }
        
        //public List<Vec> GetChangedPoints() { return ChangedPoints; }

        private List<IWindow> Windows;
        public void AddWindow(IWindow window)
        {
            Windows.Add(window);
        }
        

        //private static Character DEFAULT_CHARACTER = new Character(' ', TermColor.White, TermColor.Black);

        public FrameGenerator(Vec size)
        {
            ChangedPoints = new List<Vec>();

            Dimensions = size;

            SetRenderSize(size);
            Windows = new List<IWindow>();
        }

        public void Update(GameSimulation model, IMenu menu)
        {
            CreateNextFrame(model, menu);
            UpdateChangedPoints();
            ApplyChangedPoints();
        }

        private void ApplyChangedPoints()
        {
            foreach (Vec changedPoint in ChangedPoints)
            {
                LastFrame.SetSingle(changedPoint, NextFrame.GraphicAtPosition(changedPoint));
            }
        }

        private void CreateNextFrame(GameSimulation model, IMenu menu)
        {
            NextFrame.Fill(new AsciiGraphic(new Character(' ')));

            foreach (IWindow window in Windows)
            {
                window.Update(model, menu);
                if (window.Visible)
                {
                    DrawWindowToNextFrame(window);
                    DrawWindowBorder(window);
                }
            }
        }

        private void DrawWindowBorder(IWindow window)
        {
            Vec position = new Vec(window.Origin.X-2, window.Origin.Y-2);
            Vec size = new Vec(window.Size.X+3, window.Size.Y+3);

            TermColor foreColor = TermColor.DarkGray;
            TermColor backColor = TermColor.Black;

            IGraphic borderVertical = new AsciiGraphic(new Character(Glyph.VerticalBarsFill), foreColor, backColor);
            IGraphic borderHorizontal = new AsciiGraphic(new Character(Glyph.HorizontalBarsFill), foreColor, backColor);
            
            IGraphic borderUpperLeft = new AsciiGraphic(new Character(Glyph.BarDoubleDownRight), foreColor, backColor);
            IGraphic borderUpperRight = new AsciiGraphic(new Character(Glyph.BarDoubleDownLeft), foreColor, backColor);
            IGraphic borderLowerLeft = new AsciiGraphic(new Character(Glyph.BarDoubleUpRight), foreColor, backColor);
            IGraphic borderLowerRight = new AsciiGraphic(new Character(Glyph.BarDoubleUpLeft), foreColor, backColor);

            NextFrame.SetSingle(new Vec(position.X, position.Y), borderUpperLeft);
            NextFrame.SetSingle(new Vec(position.X + size.X, position.Y), borderUpperRight);
            NextFrame.SetSingle(new Vec(position.X, position.Y + size.Y), borderLowerLeft);
            NextFrame.SetSingle(new Vec(position.X + size.X, position.Y + size.Y), borderLowerRight);

            for (int X = position.X + 1; X < position.X + size.X; X++)
            {
                NextFrame.SetSingle(new Vec(X, position.Y), borderHorizontal);
                NextFrame.SetSingle(new Vec(X, position.Y + size.Y), borderHorizontal);
            }

            for (int Y = position.Y + 1; Y < position.Y + size.Y; Y++)
            {
                NextFrame.SetSingle(new Vec(position.X, Y), borderVertical);
                NextFrame.SetSingle(new Vec(position.X + size.X, Y), borderVertical);
            }
        }

        private void DrawWindowToNextFrame(IWindow window)
        {
            Vec position = Vec.Zero;

            for (int x = 0; x < window.Size.X; x++)
            {
                for (int y = 0; y < window.Size.Y; y++)
                {
                    position.X = x;
                    position.Y = y;

                    var thing =  window.LocalFrame.GraphicAtPosition(position);
                    
                    NextFrame.SetSingle(window.Origin + position, thing);
                }
            }
        }


        
        public IGraphic GraphicAtPosition(Vec position) 
        {
            return LastFrame.GraphicAtPosition(position);
        }
         
         
        
        private void UpdateChangedPoints()
        {
            ChangedPoints.Clear();
            Vec Size = NextFrame.Size;
            Vec point = Vec.Zero;
            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    point.X = x;
                    point.Y = y;

                    if (LastFrame.GraphicAtPosition(point) != NextFrame.GraphicAtPosition(point))
                    {
                        ChangedPoints.Add(point);
                    }
                }
            }
        }
        

        private void SetRenderSize(Vec Size)
        {
            LastFrame = new Frame(Size);
            NextFrame = new Frame(Size);
        }
    }
}
