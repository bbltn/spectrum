﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;
using Malison.WinForms;

namespace Spectrum
{
    public class Renderer
    {
        private enum LastRendered { GameView, MenuView }
        private LastRendered last { get; set; }

        private IFrameGenerator GameView;
        private IFrameGenerator MenuView;

        private ITerminal Terminal;

        public Renderer(ITerminal terminal, Vec size)
        {
            Terminal = terminal;
            
            GameView = new FrameGenerator(size);
            ConfigureGameView();

            MenuView = new FrameGenerator(size);
            ConfigureMenuView();

            last = LastRendered.MenuView;
        }

        private void ConfigureMenuView()
        {
            MenuView.AddWindow(new MenuWindow(origin: new Vec(42, 4), size: new Vec(80, 50)));
        }

        private void ConfigureGameView()
        {
            GameView.AddWindow(new CharacterWindow(origin: new Vec(3, 3), size: new Vec(34, 16)));
            GameView.AddWindow(new WeaponWindow(origin: new Vec(3, 24), size: new Vec(34, 14)));
            GameView.AddWindow(new InventoryWindow(origin: new Vec(3, 43), size: new Vec(34, 12)));
            GameView.AddWindow(new SceneWindow(origin: new Vec(42, 3), size: new Vec(80, 40)));
            GameView.AddWindow(new MessageWindow(origin: new Vec(42, 48), size: new Vec(80, 7)));
            GameView.AddWindow(new TimeWindow(origin: new Vec(127, 3), size: new Vec(30, 1)));
            GameView.AddWindow(new TileWindow(origin: new Vec(127, 9), size: new Vec(30, 10)));
            GameView.AddWindow(new HelpWindow(origin: new Vec(127, 24), size: new Vec(30, 20)));
        }


        public void RenderGameSimulation(GameSimulation model, IMenu menu)
        {
            GameView.Update(model, menu);

            if (last == LastRendered.GameView)
                RenderChanges(GameView);
            else
                RenderAll(GameView);

            last = LastRendered.GameView;
        }

        public void RenderMenu(GameSimulation model, IMenu menu)
        {
            MenuView.Update(model, menu);

            if (last == LastRendered.MenuView)
                RenderChanges(MenuView);
            else
                RenderAll(MenuView);

            last = LastRendered.MenuView;
        }


        private void RenderChanges(IFrameGenerator view)
        {
            List<Vec> changedPoints = view.ChangedPoints;

            foreach (Vec changedPoint in changedPoints)
            {
                var target = view.GraphicAtPosition(changedPoint);
                Terminal[changedPoint.X, changedPoint.Y][target.ForeColor,target.BackColor].Write(target.Icon.Glyph);
            }            
        }

        private void RenderAll(IFrameGenerator view)
        {
            for (int x = 0; x < view.Dimensions.X; x++)
            {
                for (int y = 0; y < view.Dimensions.Y; y++)
                {
                    var target = view.GraphicAtPosition(new Vec(x, y));
                    Terminal[x, y][target.ForeColor, target.BackColor].Write(target.Icon.Glyph);
                }
            }
        }
    }
}
