﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public interface IGraphic
    {
        Character Icon { get; set; }
        TermColor ForeColor { get; set; }
        TermColor BackColor { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Desaturate")]
        IGraphic DesaturateColors();
        IGraphic DimColors();
        IGraphic TintColors();
    }

    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ascii")]
    public class AsciiGraphic : IGraphic
    {
        public Character Icon { get; set; }
        public TermColor ForeColor { get; set; }
        public TermColor BackColor { get; set; }


        public AsciiGraphic()
        {
            Icon = new Character('?');
            ForeColor = TermColor.Pink;
            BackColor = TermColor.DarkBlue;
        }

        
        public AsciiGraphic(TermColor foreColor, TermColor backColor)
        {
            Icon = new Character('?');
            ForeColor = foreColor;
            BackColor = backColor;
        }


        public AsciiGraphic(Character icon)
        {
            Icon = icon;
            ForeColor = TermColor.LightGray;
            BackColor = TermColor.Black;
        }


        public AsciiGraphic(Character icon, TermColor foreColor)
        {
            Icon = icon;
            ForeColor = foreColor;
            BackColor = TermColor.Black;
        }
         

        public AsciiGraphic(Character icon, TermColor foreColor, TermColor backColor)
        {
            Icon = icon;
            ForeColor = foreColor;
            BackColor = backColor;
        }

        public IGraphic DesaturateColors()
        {
            return new AsciiGraphic(Icon, ColorUtilities.Desaturate(ForeColor), ColorUtilities.Desaturate(BackColor));
        }

        public IGraphic DimColors()
        {
            return new AsciiGraphic(Icon, ColorUtilities.Dim(ForeColor), ColorUtilities.Dim(BackColor));
        }

        public IGraphic TintColors()
        {
            return new AsciiGraphic(Icon, ColorUtilities.Tint(ForeColor), ColorUtilities.Tint(BackColor));
        }



        public static bool operator ==(AsciiGraphic first, AsciiGraphic second)
        {
            if (first == null && second == null) return true;
            if (first == null || second == null) return false;

            // If one is null, but not both, return false.
            if (((object)first == null) || ((object)second == null))
                return false;

            // Return true if the fields match:
            //return first.X == second.X && first.Y == second.Y && first.Z == second.Z;

            return first.Icon.Glyph == second.Icon.Glyph && first.ForeColor == second.ForeColor && first.BackColor == second.BackColor;
        }

        public static bool operator !=(AsciiGraphic first, AsciiGraphic second)
        {
            return !(first == second);
        }

    }
}
