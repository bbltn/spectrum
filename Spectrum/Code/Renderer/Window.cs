﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    public interface IWindow
    {
        Vec Origin { get; }
        Vec Size { get; }
        Frame LocalFrame { get; }
        void Update(GameSimulation model, IMenu menu);
        bool Visible { get; set; }
        bool Transparent { get; set; }
    }

    public class Window : IWindow
    {
        public bool Visible { get; set; }
        public bool Transparent { get; set; }
        public Vec Origin { get; private set; }
        public Frame LocalFrame { get; private set; }
        public Vec Size 
        { 
            get 
            { 
                return LocalFrame.Size; 
            } 
        }

        public Window(Vec origin, Vec size/*, bool visible = true, bool transparent = false*/)
        {
            Origin = origin;
            LocalFrame = new Frame(size);
            Visible = true;
            Transparent = false;
        }

        public virtual void Update(GameSimulation model, IMenu menu)
        {
            LocalFrame.Fill(new AsciiGraphic(new Character(' ')));
        }

        protected int GetCenterOffset(int length)
        {
            //if (str == null) return (Size.X / 2);
            return (Size.X / 2) - (length / 2); 
        }

        protected int GetRightAlignOffset(int length)
        {
            return Size.X - length; 
        }
    }
}
