﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class SceneWindow : Window
    {
        public SceneWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            var render = model.Scene.Camera.DrawScene(Size.X, Size.Y, model.Variables);

            for (int x = 0; x < Size.X; x++)
            {
                for (int y = 0; y < Size.Y; y++)
                {
                    LocalFrame.SetSingle(new Vec(x, y), render[x, y]);
                }
            }

            if (model.Variables.Debug.DebugLine)
                DebugLine(model);

            if (model.Variables.Debug.DebugDijkstra)
                DebugFloodFill(model, 5, NodeCostEvaluation.SameZLevelAndNoBlockedSpace);

            if (model.Variables.Debug.DebugSphere)
                DebugSphere(model, 4);

            if (model.Variables.Debug.DebugPath)
                DebugPath(model);
        }

        private void DebugPath(GameSimulation model)
        {
            Vec offset = GetOffset(Size, model.Scene.Camera.Position);

            INode origin = model.Scene.Tiles.GetTile(new Vector3(5, 5, 1)).Node;
            INode target = model.Scene.Tiles.GetTile(model.Scene.Actors.GetPlayer.Position).Node;

            IPath path = new Path(origin, target, NodeCostEvaluation.WalkDistance);

            TermColor foreColor = TermColor.Pink;
            TermColor backColor = TermColor.DarkGray;
            Character character = new Character('#');
            IGraphic graphic = new AsciiGraphic(character, foreColor, backColor);

            for (int iter = 0; iter < path.Count; iter++)
            {
                Vec position = new Vec((int)path.GetPoint(iter).X + offset.X, (int)path.GetPoint(iter).Y + offset.Y);
                LocalFrame.SetSingle(position, graphic);
            }

            LocalFrame.Write(new Vec(1, 1), new CharacterString("PATH: " + path.Count, TermColor.Flesh, TermColor.Black));
            LocalFrame.Write(new Vec(1, 2), new CharacterString("CYCLES: " + path.Cost, TermColor.Flesh, TermColor.Black));
        }

        private void DebugSphere(GameSimulation model, int radius)
        {
            Sphere sphere = new Sphere(model.Scene.Actors.GetPlayer.Position, radius);
            Vec offset = GetOffset(Size, model.Scene.Camera.Position);

            TermColor foreColor = TermColor.Pink;
            TermColor backColor = TermColor.DarkGray;
            Character character = new Character('#');

            foreach (var point in sphere.Selection)
            {
                Vec position = new Vec((int)point.X + offset.X, (int)point.Y + offset.Y);
                IGraphic graphic = new AsciiGraphic(character, foreColor, backColor);
                LocalFrame.SetSingle(position, graphic);
            }
        }

        private void DebugFloodFill(GameSimulation model, int floodDistance, NodeCostEvaluationMethod evaluation)
        {
            INode origin = model.Scene.Tiles.GetTile(model.Scene.Actors.GetPlayer.Position).Node;

            var djikstra = FloodFill.Create(origin, floodDistance, evaluation);

            Vec offset = GetOffset(Size, model.Scene.Camera.Position);

            foreach (var point in djikstra)
            {
                Vec position = new Vec((int)point.Position.X + offset.X, (int)point.Position.Y + offset.Y);

                TermColor foreColor = TermColor.White;
                TermColor backColor = TermColor.Black;

                if (point.Value >= 5)
                    backColor = TermColor.Yellow;
                else if (point.Value == 4)
                    backColor = TermColor.Red;
                else if (point.Value == 3)
                    backColor = TermColor.Orange;
                else if (point.Value == 2)
                    backColor = TermColor.Green;
                else if (point.Value == 1)
                    backColor = TermColor.Blue;

                IGraphic graphic = new AsciiGraphic(new Character(point.Value.ToString()[0]), foreColor, backColor);

                LocalFrame.SetSingle(position, graphic);
            }
        }

        private static Vec GetOffset(Vec Size, Vector3 CameraPosition)
        {
            return new Vec((int)Size.X / 2 - (int)CameraPosition.X, (int)Size.Y / 2 - (int)CameraPosition.Y);
        }

        private void DebugLine(GameSimulation model)
        {
            var player = model.Scene.Actors.GetPlayer;
            if (player != null)
            {
                Line line = new Line(model.Scene.Camera.Position, player.Position);

                Vec offset = GetOffset(Size, model.Scene.Camera.Position);

                var graphic = new AsciiGraphic(new Character('X'), TermColor.Pink, TermColor.DarkBlue);

                model.Scene.Messages.Clear();

                for (int iter = 0; iter < line.Points.Count; iter++)
                {
                    Vec position = new Vec((int)line.Points[iter].X + offset.X, (int)line.Points[iter].Y + offset.Y);

                    LocalFrame.SetSingle(position, graphic);

                    if (iter == line.Points.Count - 1)
                    {
                        model.Scene.Messages.Add("TARG DIST: " + line.Points[iter].X + "," + line.Points[iter].Y, TermColor.Green, TermColor.Black);
                        model.Scene.Messages.Add("PLAY POS:  " + player.Position.X + "," + player.Position.Y, TermColor.Red, TermColor.Black);

                        string tiles = "";

                        foreach (var thing in line.Points)
                            tiles += "{" + thing.X + "," + thing.Y + "," + thing.Z + "} ";
                        
                        model.Scene.Messages.Add("TILES: " + line.Points.Count + " " + tiles);

                    }
                }
            }
            
        }
    }
}
