﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class CharacterWindow : Window
    {
        public CharacterWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            base.Update(model, menu);

            WriteName(model, new Vec(0, 0));
            WriteHealth(model, new Vec(0, 2));
            WriteStance(model, new Vec(0, 4));
            WriteSkills(model, new Vec(0, 6));
        }

        private void WriteName(GameSimulation model, Vec offset)
        {
            TermColor foreColor = TermColor.LightGold;
            TermColor backColor = TermColor.Black;
            string playerName = " -[ " + model.Scene.Actors.GetPlayer.Name.First + " " + model.Scene.Actors.GetPlayer.Name.Last + " ]- ";
            int center = (Size.X / 2) - (playerName.Length / 2);
            LocalFrame.Write(new Vec(center + offset.X, 0 + offset.Y), new CharacterString(playerName, foreColor, backColor));
        }

        private void WriteHealth(GameSimulation model, Vec offset)
        {
            string health = "HEALTHY";            
            TermColor foreColor = TermColor.LightGreen;
            TermColor backColor = TermColor.DarkGreen;

            switch (model.Scene.Actors.GetPlayer.Health.Status)
            {
                case HealthStatusType.LightWounds:
                    foreColor = TermColor.LightYellow;
                    backColor = TermColor.DarkYellow;
                    health = "LIGHT WOUNDS";
                    break;
                case HealthStatusType.HeavyWounds:
                    foreColor = TermColor.LightOrange;
                    backColor = TermColor.DarkOrange;
                    health = "HEAVY WOUNDS";
                    break;
                case HealthStatusType.Dead:
                    foreColor = TermColor.LightRed;
                    backColor = TermColor.DarkRed;
                    health = "DEAD";
                    break;
            }

            CharacterString healthStr = new CharacterString("[ " + health + " ]", foreColor, backColor);


            string conscious = "AWAKE";
            foreColor = TermColor.LightGreen;
            backColor = TermColor.DarkGreen;

            switch (model.Scene.Actors.GetPlayer.Health.Consciousness)
            {
                case ConsciousStatusType.Unconscious:
                    foreColor = TermColor.LightRed;
                    backColor = TermColor.DarkRed;
                    conscious = "UNCONSCIOUS";
                    break;
            }

            CharacterString consciousStr = new CharacterString("[ " + conscious + " ]", foreColor, backColor);


            CharacterString combination = new CharacterString();
            for (int iter = 0; iter < healthStr.Count; iter++)
                combination.Add(healthStr[iter]);

            combination.Add(new Character(' '));

            for (int iter = 0; iter < consciousStr.Count; iter++)
                combination.Add(consciousStr[iter]);

            int center = GetCenterOffset(combination.Count);

            LocalFrame.Write(new Vec(offset.X + center, offset.Y), combination);
        }

        private void WriteStance(GameSimulation model, Vec offset)
        {
            string stance = " STANDING ";
            TermColor backColor = TermColor.DarkYellow;

            switch (model.Scene.Actors.GetPlayer.Stance)
            {
                case StanceType.Prone:
                    stance = " PRONE ";
                    backColor = TermColor.DarkBlue;
                    break;
                
                case StanceType.Run:
                    stance = " RUNNING ";
                    backColor = TermColor.DarkGreen;
                    break;
            }

            CharacterString printStr = new CharacterString(stance, TermColor.White, backColor);
            int center = GetCenterOffset(printStr.Count);
            LocalFrame.Write(new Vec(center + offset.X, offset.Y), printStr);
        }

        private void WriteSkills(GameSimulation model, Vec offset)
        {
            var skills = model.Scene.Actors.GetPlayer.Skill;

            TermColor foreColor = TermColor.White;
            TermColor backColor = TermColor.DarkGray;

            CharacterString leaderStr = new CharacterString(" Leadership: " + skills.Leadership + " ", foreColor, backColor);
            int center = GetCenterOffset(leaderStr.Count);
            LocalFrame.Write(new Vec(center + offset.X, 0 + offset.Y), leaderStr);

            CharacterString skillStr = new CharacterString(" Guns: " + skills.Gun + "  Unarmed: " + skills.Unarmed + "  Medic: " + skills.Medic + " ", foreColor, backColor);
            center = GetCenterOffset(skillStr.Count);
            LocalFrame.Write(new Vec(center + offset.X, 2 + offset.Y), skillStr);
        }
    }
}
