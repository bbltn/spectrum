﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class TimeWindow : Window
    {
        public TimeWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            string hours;
            int hourCount = model.Scene.Time.Hours;
            if (hourCount >= 10) hours = hourCount.ToString();
            else hours = "0" + hourCount;

            string minutes;
            int minuteCount = model.Scene.Time.Minutes;
            if (minuteCount >= 10) minutes = minuteCount.ToString();
            else minutes = "0" + hourCount;

            string str = " " + hours + ":" + minutes + " " + model.Scene.Time.Seconds.ToString() + "' ";
            CharacterString second = new CharacterString(str, TermColor.LightGold, TermColor.DarkBlue);

            CharacterString first = new CharacterString("Time: ", TermColor.LightGray, TermColor.Black);

            CharacterString toPrint = Strings.Concatenate(first, second);

            LocalFrame.Write(new Vec(GetCenterOffset(toPrint.Count), 0), toPrint);

            /*
            LocalFrame.Write(new Vec(0, 0), new CharacterString("H: " + model.Scene.Time.Hours.ToString()));
            LocalFrame.Write(new Vec(0, 1), new CharacterString("M: " + model.Scene.Time.Minutes.ToString()));
            LocalFrame.Write(new Vec(0, 2), new CharacterString("S: " + model.Scene.Time.Seconds.ToString()));
            LocalFrame.Write(new Vec(0, 3), new CharacterString("Q: " + model.Scene.Time.QuarterSeconds.ToString()));
            LocalFrame.Write(new Vec(0, 4), new CharacterString("Absolute: " + model.Scene.Time.Absolute.ToString()));
             */
        }
    }
}
