﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class MessageWindow : Window
    {
        public MessageWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            for (int iter = 0; iter < Size.Y; iter++)
                LocalFrame.Write(new Vec(0, iter), model.Scene.Messages.GetMessage(model.Scene.Messages.Count - iter));
        }
    }
}
