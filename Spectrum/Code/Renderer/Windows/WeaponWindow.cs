﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class WeaponWindow : Window
    {
        //private const int INDENT = 2;

        public WeaponWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            var equipped = model.Scene.Actors.GetPlayer.Inventory.Equipped;

            if (equipped != null)
                if (equipped.Gun != null)
                {
                    WriteGun(equipped.Gun, new Vec(0, 0));
                    //WriteGunType(equipped.Gun, new Vec(0, 1));
                    WriteGunDescription(equipped.Gun, new Vec(0, 2));
                    WriteGunStats(equipped.Gun, new Vec(0, 5));
                    WriteAttachments(equipped.Gun, new Vec(0, 8));
                    WriteFireMode(equipped.Gun, new Vec(0, 13));
                }
        }


        private void WriteFireMode(IGun gun, Vec offset)
        {
            CharacterString spacer = new CharacterString("  ");
            string str = "Mode: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString combined = new CharacterString(str, fore, back);

            for (int iter = 0; iter < gun.ActionCount; iter++)
            {
                var action = gun.GetAction(iter);

                str = " " + action.ToString() + " ";

                if (gun.CurrentAction == action)
                {
                    fore = TermColor.White;
                    back = TermColor.DarkGold;
                }
                else
                {
                    fore = TermColor.LightGray;
                    back = TermColor.Black;
                }

                CharacterString value = new CharacterString(str, fore, back);
                combined = Strings.Concatenate(combined, spacer);
                combined = Strings.Concatenate(combined, value);
            }

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(combined.Count), offset.Y), combined);
        }

        private void WriteAttachments(IGun gun, Vec offset)
        {
            CharacterString spacer = new CharacterString(" ");
            var stock = GetStockAsCharacterString(gun, TermColor.DarkGray);
            var barrel = GetBarrelAsCharacterString(gun, TermColor.DarkGray);
            var muzzle = GetMuzzleAsCharacterString(gun, TermColor.DarkGray);
            var optic = GetOpticAsCharacterString(gun, TermColor.DarkGray);
            var grip = GetForegripAsCharacterString(gun, TermColor.DarkGray);

            var combined = Strings.Concatenate(stock, spacer);
            combined = Strings.Concatenate(combined, barrel);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(combined.Count), offset.Y), combined);

            combined = Strings.Concatenate(optic, spacer);
            combined = Strings.Concatenate(combined, grip);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(combined.Count), offset.Y + 1), combined);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(muzzle.Count), offset.Y + 2), muzzle);
        }

        private void WriteGunStats(IGun gun, Vec offset)
        {
            CharacterString spacer = new CharacterString(" ");
            var caliber = GetCaliberAsCharacterString(gun);
            var range = GetRangeAsCharacterString(gun);

            var combined = Strings.Concatenate(caliber, spacer);
            combined = Strings.Concatenate(combined, range);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(combined.Count), offset.Y), combined);
        }

        private void WriteGunDescription(IGun gun, Vec offset)
        {
            string str = gun.Item.Description;
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;

            CharacterString toPrint = new CharacterString(str, fore, back);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(toPrint.Count), offset.Y), toPrint);
        }

        private void WriteGun(IGun gun, Vec offset)
        {
            string str = gun.Item.Name;
            TermColor fore = TermColor.White;
            //TermColor back = ColorUtils.GetDarkVersion(gun.Item.Graphic.BackColor);
            TermColor back = TermColor.DarkRed;
            CharacterString toPrint = new CharacterString(str, fore, back);

            LocalFrame.Write(new Vec(offset.X + GetCenterOffset(toPrint.Count), offset.Y), toPrint);
        }



        private static CharacterString GetCaliberAsCharacterString(IGun gun)
        {
            string str = "Cal: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + Strings.CaliberToString(gun.Caliber) + " ";
            fore = TermColor.White;
            back = TermColor.DarkBlue;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetRangeAsCharacterString(IGun gun)
        {
            string str = " Range: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);


            str = " " + gun.Range + " ";
            fore = TermColor.White;
            back = TermColor.DarkBlue;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetStockAsCharacterString(IGun gun, TermColor valueColor = TermColor.DarkGreen)
        {
            string str = "Stock: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + gun.Stock + " ";
            fore = TermColor.White;
            back = valueColor;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetBarrelAsCharacterString(IGun gun, TermColor valueColor = TermColor.DarkGreen)
        {
            string str = "Barrel: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + gun.Barrel + " ";
            fore = TermColor.White;
            back = valueColor;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetOpticAsCharacterString(IGun gun, TermColor valueColor = TermColor.DarkGreen)
        {
            string str = "Optic: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + gun.Optic + " ";
            fore = TermColor.White;
            back = valueColor;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetForegripAsCharacterString(IGun gun, TermColor valueColor = TermColor.DarkGreen)
        {
            string str = "Grip: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + gun.Foregrip + " ";
            fore = TermColor.White;
            back = valueColor;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }

        private static CharacterString GetMuzzleAsCharacterString(IGun gun, TermColor valueColor = TermColor.DarkGreen)
        {
            string str = "Muzzle: ";
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            CharacterString first = new CharacterString(str, fore, back);

            str = " " + gun.Muzzle + " ";
            fore = TermColor.White;
            back = valueColor;
            CharacterString second = new CharacterString(str, fore, back);

            return Strings.Concatenate(first, second);
        }
    }
}
