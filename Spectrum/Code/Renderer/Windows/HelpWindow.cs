﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class HelpWindow : Window
    {
        public HelpWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            WriteString(0, "HELP");

            WriteControl(2, "Move", "NUMPAD");
            WriteControl(4, "Strike", "Move into enemy");
            WriteControl(6, "Shoot", "F");
            WriteControl(8, "Medkit", "H");
            WriteControl(10, "Menu", "ESCAPE");

            WriteString(14, "You look like '@'");
            WriteString(15, "Enemies look like 'S'");
            WriteString(16, "Kill 'em all to win!");

            WriteEnemyCount(18, model.Scene.Actors);
        }

        private void WriteEnemyCount(int offset, IActorSet actors)
        {
            CharacterString prompt = new CharacterString("Enemies: ", TermColor.LightGray, TermColor.Black);

            int max = actors.Count - 1;

            TermColor color = TermColor.White;

            int living = 0;
            for (int i = 0; i < actors.Count; i++)
            {
                IActor actor = actors.GetActor(i);

                if (actor.Control == ControlType.Bot && actor.Health.Status != HealthStatusType.Dead)
                    living++;
            }

            if (living <= 5) color = TermColor.Green;
            else if (living <= 10) color = TermColor.Yellow;
            

            CharacterString value = new CharacterString(living.ToString() + "/" + max.ToString(), color, TermColor.Black);

            CharacterString combined = Strings.Concatenate(prompt, value);

            LocalFrame.Write(new Vec(GetCenterOffset(combined.Count), offset), combined);
        }

        private void WriteString(int offset, string str)
        {
            CharacterString toPrint = new CharacterString(str, TermColor.White, TermColor.Black);

            LocalFrame.Write(new Vec(GetCenterOffset(toPrint.Count), offset), toPrint);
        }

        private void WriteControl(int offset, string prompt, string value)
        {
            CharacterString first = CreatePrompt(prompt + ": ");
            CharacterString second = CreateValue(" " + value + " ");

            CharacterString combined = Strings.Concatenate(first, second);

            LocalFrame.Write(new Vec(0, offset), combined);
        }

        private static CharacterString CreatePrompt(string str)
        {
            TermColor fore = TermColor.LightGray;
            TermColor back = TermColor.Black;
            return new CharacterString(str, fore, back);
        }

        private static CharacterString CreateValue(string str)
        {
            TermColor fore = TermColor.Flesh;
            TermColor back = TermColor.DarkRed;
            return new CharacterString(str, fore, back);
        }
    }
}
