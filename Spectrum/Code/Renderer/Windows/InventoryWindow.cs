﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class InventoryWindow : Window
    {
        private const int INDENT = 2;

        public InventoryWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            base.Update(model, menu);

            WriteTitle(new Vec(0, 0));
            //WriteEquippedItem(model, new Vec(0, 2));
            WriteEquippedGarment(model, new Vec(0, 2));
            WriteInventoryItems(model, new Vec(0, 5));
        }

        private void WriteTitle(Vec offset)
        {
            string str = " INVENTORY ";
            TermColor foreColor = TermColor.LightCyan;
            TermColor backColor = TermColor.DarkBlue;
            CharacterString titleStr = new CharacterString(str, foreColor, backColor);

            int center = GetCenterOffset(titleStr.Count);

            LocalFrame.Write(new Vec(offset.X + center, offset.Y), titleStr);
        }

        private void WriteEquippedGarment(GameSimulation model, Vec offset)
        {
            TermColor foreColor = TermColor.White;
            TermColor backColor = TermColor.Black;
            CharacterString prefix = new CharacterString("Wearing: ", foreColor, backColor);
            CharacterString suffix;
            CharacterString combined = new CharacterString();

            IGarment garment = model.Scene.Actors.GetPlayer.Inventory.Wearing;

            if (garment == null)
                suffix = new CharacterString("nothing", foreColor, backColor);
            else
            {
                foreColor = TermColor.Flesh;
                backColor = TermColor.DarkRed;
                suffix = new CharacterString(" " + garment.Item.Name + " ", foreColor, backColor);
            }

            combined = Strings.Concatenate(prefix, suffix);

            LocalFrame.Write(offset, combined);

            // write description if we have an item
            if (garment != null)
            {
                foreColor = TermColor.LightGray;
                backColor = TermColor.Black;
                CharacterString description = new CharacterString(garment.Item.Description, foreColor, backColor);
                LocalFrame.Write(new Vec(offset.X + INDENT, offset.Y + 1), description);
            }
        }

        private void WriteInventoryItems(GameSimulation model, Vec offset)
        {
            IInventory inventory = model.Scene.Actors.GetPlayer.Inventory;

            if (inventory.ItemCount <= 0) return;

            TermColor foreColor = TermColor.White;
            TermColor backColor = TermColor.Black;
            string str = "In pack:";
            CharacterString printStr = new CharacterString(str, foreColor, backColor);

            LocalFrame.Write(offset, printStr);

            for (int iter = 0; iter < inventory.ItemCount; iter++)
            {
                foreColor = TermColor.LightGray;
                backColor = TermColor.Black;
                str = inventory.GetItem(iter).Name;
                printStr = new CharacterString(str, foreColor, backColor);

                LocalFrame.Write(new Vec(offset.X + INDENT, offset.Y + 1 + iter), printStr);
            }
        }
    }
}
