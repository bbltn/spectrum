﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class TileWindow : Window
    {
        private const int INDENT = 2;

        public TileWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            base.Update(model, menu);

            WriteLowerFeature(model, new Vec(0, 0));

            WriteDecal(model, new Vec(0, 3));
            WriteGas(model, new Vec(0, 5));
            WriteItems(model, new Vec(0, 7));
        }

        private void WriteLowerFeature(GameSimulation model, Vec offset)
        {
            var basePos = model.Scene.Actors.GetPlayer.Position;
            var modPos = new Vector3(basePos.X, basePos.Y, basePos.Z - 1);
            ITile tile = model.Scene.Tiles.GetTile(modPos);

            TermColor fore = TermColor.White;
            TermColor back = TermColor.Black;
            string str = "Tile: ";
            CharacterString toPrint = new CharacterString(str, fore, back);


            if (tile.Feature == null)
            {
                fore = TermColor.LightGray;
                back = TermColor.Black;
                str = " empty ";
            }
            else
            {
                fore = TermColor.White;
                back = ColorUtilities.GetDarkVersion(tile.Feature.Graphic.BackColor);
                str = " " + tile.Feature.Name + " ";
            }
            CharacterString value = new CharacterString(str, fore, back);

            LocalFrame.Write(offset, Strings.Concatenate(toPrint, value));

            if (tile.Feature != null)
            {
                fore = TermColor.LightGray;
                back = TermColor.Black;
                str = tile.Feature.Description;
                value = new CharacterString(str, fore, back);
                LocalFrame.Write(new Vec(offset.X + INDENT, offset.Y + 1), value);
            }
        }

        private void WriteDecal(GameSimulation model, Vec offset)
        {
            var basePos = model.Scene.Actors.GetPlayer.Position;
            var modPos = new Vector3(basePos.X, basePos.Y, basePos.Z - 1);
            ITile tile = model.Scene.Tiles.GetTile(modPos);

            if (tile.Detail != null)
            {
                TermColor fore = TermColor.White;
                TermColor back = TermColor.Black;
                //string str = "Decal: ";
                string str = "";
                CharacterString first = new CharacterString(str, fore, back);

                fore = TermColor.White;
                back = ColorUtilities.GetDarkVersion(tile.Detail.Graphic.BackColor);
                str = " " + tile.Detail.Name + " ";
                CharacterString second = new CharacterString(str, fore, back);

                LocalFrame.Write(offset, Strings.Concatenate(first, second));
            }
        }

        private void WriteGas(GameSimulation model, Vec offset)
        {
            var pos = model.Scene.Actors.GetPlayer.Position;
            IGas gas = model.Scene.Tiles.GetTile(pos).Gas;
            if (gas != null)
            {
                TermColor fore = TermColor.White;
                TermColor back = TermColor.Black;
                string str = "Gas: ";
                CharacterString first = new CharacterString(str, fore, back);

                fore = TermColor.LightGreen;
                back = TermColor.DarkGreen;
                str = " " + gas.Name + " ";
                CharacterString second = new CharacterString(str, fore, back);

                LocalFrame.Write(offset, Strings.Concatenate(first, second));
            }
        }

        private void WriteItems(GameSimulation model, Vec offset)
        {
            var pos = model.Scene.Actors.GetPlayer.Position;
            var tile = model.Scene.Tiles.GetTile(pos);

            if (tile.ItemCount > 0)
            {
                TermColor fore = TermColor.White;
                TermColor back = TermColor.Black;
                string str = "Items: ";
                CharacterString value = new CharacterString(str, fore, back);

                LocalFrame.Write(offset, value);

                for (int iter = 0; iter < tile.ItemCount; iter++)
                {
                    var item = tile.GetItem(iter);

                    fore = TermColor.White;
                    back = ColorUtilities.GetDarkVersion(item.Graphic.BackColor);
                    str = item.Name;
                    value = new CharacterString(str, fore, back);

                    LocalFrame.Write(new Vec(offset.X + INDENT, offset.Y + iter + 1), value);
                }
            }
        }
    }
}
