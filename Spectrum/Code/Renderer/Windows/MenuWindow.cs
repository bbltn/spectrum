﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Bramble.Core;
using Malison.Core;

namespace Spectrum
{
    class MenuWindow : Window
    {
        public MenuWindow(Vec origin, Vec size)
            : base(origin, size)
        {
        }

        public override void Update(GameSimulation model, IMenu menu)
        {
            if (model == null || menu == null) return;

            base.Update(model, menu);

            for (int iter = 0; iter < menu.ItemCount; iter++)
            {
                LocalFrame.Write(new Vec(2, 0 + (iter*2)), menu.GetItem(iter).Name);
            }

            //frame.Set(new Vec(0, 0 + (menu.Selector * 2)), new CharacterString(">", TermColor.Green, TermColor.Black));
            LocalFrame.SetSingle(new Vec(0, 0 + (menu.Selector * 2)), new AsciiGraphic(new Character('>'), TermColor.Green, TermColor.Black));
        }
    }
}
