﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class PositionValue
    {
        public Vector3 Position { get; set; }
        public int Value { get; set; }
        
        public PositionValue(Vector3 position, int value)
        {
            Position = position;
            Value = value;
        }
    }
}
