﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    static public class FloodFill
    {
        static public PositionValue NodeToPositionValue(INode node, int value)
        {
            return new PositionValue(NodeValues.Position(node), value);
        }

        static public List<PositionValue> Create(INode origin, int value, NodeCostEvaluationMethod evaluationMethod)
        {
            List<PositionValue> returnSet = new List<PositionValue>();

            if (origin == null || evaluationMethod == null) return returnSet;

            List<INode> closedSet = new List<INode>();
            List<INode> openSet = new List<INode>();

            // add the origin point
            returnSet.Add(NodeToPositionValue(origin, value));
            closedSet.Add(origin);

            var initialOpenSet = origin.GetAdjacentNodes;
            foreach (var thing in initialOpenSet) openSet.Add(thing);
            int curValue = value - 1;

            while (openSet.Count > 0 && curValue > 0)
            {
                List<INode> newAdjacents = new List<INode>();

                foreach (var node in openSet)
                {
                    // see if this node is already closed or has invalid cost
                    int cost = (int)evaluationMethod(node, origin);
                    if (closedSet.Contains(node) == false && cost >= 0 && curValue - cost > 0)
                    {
                        // if not, add its neighbors to 'new adjecents'
                        var neighbors = node.GetAdjacentNodes;
                        foreach (var thing in neighbors) newAdjacents.Add(thing);

                        // and add it to return and closed sets
                        closedSet.Add(node);
                        returnSet.Add(NodeToPositionValue(node, curValue - cost));
                    }
                }

                openSet.Clear();
                foreach (var thing in newAdjacents) openSet.Add(thing);
                
                curValue--;
            }

            

            return returnSet;
        }
    }
}
