﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Malison.Core;

namespace Spectrum
{
    public static class Strings
    {
        public static string CaliberToString(CaliberType value)
        {
            switch (value)
            {
                case CaliberType.Cal_308:
                    return ".308";
                case CaliberType.Cal_45acp:
                    return ".45ACP";
                case CaliberType.Cal_556:
                    return "5.56";
                case CaliberType.Cal_762:
                    return "7.62";
                case CaliberType.Cal_9mm:
                    return "9mm";
            }

            return value.ToString();
        }

        public static CharacterString Concatenate(CharacterString first, CharacterString second)
        {
            CharacterString combined = new CharacterString();
            
            if (first != null)
                foreach (var ch in first)
                    combined.Add(ch);
            if (second != null)
                foreach (var ch in second)
                    combined.Add(ch);

            return combined;
        }

        public static bool StringToBool(string value)
        {
            if (value == "True" || value == "true" || value == "TRUE")
                return true;
            else if (value == "1" || value == "One" || value == "one" || value == "ONE")
                return true;
            else if (value == "Yes" || value == "yes" || value == "YES")
                return true;
            else if (value == "On" || value == "on" || value == "ON")
                return true;

            return false;
        }

        public static string BoolToString(bool value)
        {
            if (value == true) return "TRUE";
            else return "FALSE";
        }
    }
}
