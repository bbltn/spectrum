﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class BaseName
    {
        public string Name { get; set; }

        public List<string> Tags { get; set; }

        public BaseName()
        {
            Name = "base_name";
            Tags = new List<string>();
        }

        public BaseName(string name)
        {
            Name = name;
            Tags = new List<string>();
        }
    }
}
