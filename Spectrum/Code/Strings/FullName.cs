﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public interface IName
    {
        string First { get; set; }
        string Last { get; set; }
        string Full { get; }
    }

    public class FullName : IName
    {
        public string First { get; set; }
        public string Last { get; set; }

        public string Full { get { return First + " " + Last; } }

        public FullName()
        {
            First = "first_name";
            Last = "last_name";
        }
    }
}
