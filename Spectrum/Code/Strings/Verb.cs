﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spectrum
{
    public class Verb
    {
        public string Abstract { get; private set; }
        public string Past { get; private set; }
        public string Present { get; private set; }
        public string Future { get; private set; }

        public Verb
            (string abstractTense, string pastTense, string presentTense, string futureTense)
        {
            Abstract = abstractTense;
            Past = pastTense;
            Present = presentTense;
            Future = futureTense;
        }
    }
}
