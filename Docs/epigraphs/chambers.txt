Born under a south Kentucky sky, he�d come west to Mexico to fight.
1842 at Mier, the gutters filled with blood and fear,
barely made it back to Texas alive.

-----

With Missouri Volunteers when the war began.
In �46 they crossed the Rio Grande.
There he met his dark-eyed love, 
said goodbye when the war was done.
He swore that he�d come back for her again.

-----

Oh, oh, novia. Oh, oh, your man is gone.
Maybe he�s in Texas, but we�ll take what God has left us.
We�ll leave for California with the dawn.

-----

Worked his way back to Old Mexico,
reclaim the love he�d left two years ago.
Back to those same city walls where he�d watched copper cannonballs
like wayward suns roll down the cobblestones.

----

They put him in a prison, left alone,
with other Yankee fools so far from home.
Parade them through the square in chains
�til in rode Captain Glanton�s gang.
Apache scalps for bounties paid in gold.

-----

Glanton�s men were killers all by trade.
Through the prison bars a deal was made.
Glanton needed three new men, he hired this lovelorn veteran.
The killers rode out through the Governor�s gates.

-----

The veteran left camp for the rising sun.
No killer, he�d left other work undone.
He was not yet two days out when Glanton�s naked native scouts
brought back his empty horse and brand new gun.