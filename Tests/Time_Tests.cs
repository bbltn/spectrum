﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spectrum;

namespace Tests
{
    [TestClass]
    public class Time_Tests
    {
        [TestMethod]
        public void Absolute_Time()
        {
            TimeModel Time = new TimeModel(absolute: 17);

            Assert.AreEqual(Time.Absolute, 17);
        }
    }
}
