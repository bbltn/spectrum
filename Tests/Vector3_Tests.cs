﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spectrum;

namespace Tests
{
    [TestClass]
    public class Vector3_Tests
    {
        [TestMethod]
        public void Vector3_Contains()
        {
            Vector3 container = new Vector3(2, 2, 2);
            Vector3 test = new Vector3(1, 1, 1);

            Assert.AreEqual(container.Contains(test), true);
        }

        [TestMethod]
        public void Vector3_ContainFail()
        {
            Vector3 container = new Vector3(2, 2, 2);
            Vector3 test = new Vector3(3, 3, 3);

            Assert.AreEqual(container.Contains(test), false);
        }

        [TestMethod]
        public void Vector3_ContainsEdgeCase()
        {
            Vector3 container = new Vector3(2, 2, 2);
            Vector3 test = new Vector3(0, 0, 1);

            Assert.AreEqual(container.Contains(test), true);
        }   

        [TestMethod]
        public void Vector3_Equality()
        {
            Vector3 first = new Vector3(1, 1, 1);
            Vector3 second = new Vector3(1, 1, 1);

            Assert.AreEqual(first, second);
        }

        [TestMethod]
        public void Vector3_Addition()
        {
            Vector3 first = new Vector3(2, 1, -1);
            Vector3 second = new Vector3(-3, 5, 3);

            Vector3 addTest = first + second;
            Vector3 expectedResult = new Vector3(-1, 6, 2);
            Assert.AreEqual(addTest, expectedResult);

            float floatTest = 3f;
            addTest = first + floatTest;
            expectedResult = new Vector3(5, 4, 2);
            Assert.AreEqual(addTest, expectedResult);

            int intTest = 2;
            addTest = first + intTest;
            expectedResult = new Vector3(4, 3, 1);
            Assert.AreEqual(addTest, expectedResult);
        }

        [TestMethod]
        public void Vector3_Subtraction()
        {
            Vector3 first = new Vector3(2, 1, -1);
            Vector3 second = new Vector3(-3, 5, 3);

            Vector3 subTest = first - second;
            Vector3 expectedResult = new Vector3(5, -4, -4);
            Assert.AreEqual(subTest, expectedResult);

            float floatTest = 3f;
            subTest = first - floatTest;
            expectedResult = new Vector3(-1, -2, -4);
            Assert.AreEqual(subTest, expectedResult);

            int intTest = 2;
            subTest = first - intTest;
            expectedResult = new Vector3(0, -1, -3);
            Assert.AreEqual(subTest, expectedResult);
        }

        [TestMethod]
        public void Vector3_Distance()
        {
            Vector3 first = Vector3.Zero;
            Vector3 second = new Vector3(5, 0, 0);
            Vector3 third = new Vector3(2, -3, 1);

            float result = Vector3.Distance(first,second);
            float expected = 5f;
            Assert.AreEqual(result, expected);

            result = Vector3.Distance(first, third);
            expected = (float)Math.Sqrt(14);
            Assert.AreEqual(result, expected);

            result = first.GetDistance(third);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Vector3_FromString()
        {
            string input = "1,2,3";

            Vector3 result = Vector3.FromString(input);
            Vector3 expectedResult = new Vector3(1, 2, 3);
            Assert.AreEqual(result, expectedResult);

            input = "-2,3,-1";
            result = Vector3.FromString(input);
            expectedResult = new Vector3(-2, 3, -1);
            Assert.AreEqual(result, expectedResult);
        }

        [TestMethod]
        public void Vector3_ToString()
        {
            Vector3 input = new Vector3(1, -2, 3);

            string result = input.ToString();
            string expectedResult = "1,-2,3";

            Assert.AreEqual(result, expectedResult);
        }

    }
}
