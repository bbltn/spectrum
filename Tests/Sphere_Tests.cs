﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Spectrum;

namespace Tests
{
    [TestClass]
    public class Sphere_Tests
    {
        [TestMethod]
        public void Sphere_Contains()
        {
            Vector3 origin = new Vector3(3, 3, 3);
            Vector3 testPoint = new Vector3(6, 3, 3);
            float radius = 3f;

            Assert.AreEqual(Sphere.Contains(origin, radius, testPoint), true);
        }

        [TestMethod]
        public void Sphere_ContainFail()
        {
            Vector3 origin = new Vector3(3, 3, 3);
            Vector3 testPoint = new Vector3(6, 3, 3);
            float radius = 2f;

            Assert.AreEqual(Sphere.Contains(origin, radius, testPoint), false);
        }

        [TestMethod]
        public void Sphere_VolumePoints()
        {
            Vector3 origin = new Vector3(5, 0, 0);
            float radius = 5f;
            Sphere sphere = new Sphere(origin, radius);
            Vector3 testPoint = new Vector3(3, 1, -1);

            Assert.AreEqual(sphere.Selection.Contains(testPoint), true);
        }
    }
}
